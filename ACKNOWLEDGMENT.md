Acknowledgements     
================

The DYNASAUR software makes use of the following external libraries and tools :


- [matplotlib](https://matplotlib.org/)
- [colorama](https://github.com/tartley/colorama)
- [numpy](http://www.numpy.org/)
- [qd-cae-python](https://github.com/qd-cae/qd-cae-python)
- [Pillow](https://pillow.readthedocs.io/en/5.1.x/)
- [termcolor](https://github.com/ikalnytskyi/termcolor)

Risk functions in the exemplary dynasaur.def file for the CSDM plugin refer to 
Takhounts, E. G., Craig, M. J., Moorhouse, K., McFadden, J., & Hasija, V. (2013). Development of brain injury criteria (BrIC). Stapp Car Crash Journal, 57, 243â€“266.

The formulas in the rib fracture plugin and the step function in the exemplary dynasaur.def file refers to 

Forman, J. L., Kent, R. W., Mroz, K., Pipkorn, B., Bostrom, O., & Segui-Gomez, M. (2012). Predicting Rib Fracture Risk With Whole-Body Finite Element Models: Development and Preliminary Evaluation of a Probabilistic Analytical Framework. Annals of Advances in Automotive Medicine, 56, 109â€“124.

Parts of the software were developed within the EC funded project [OSCCAR](http://osccarproject.eu/). OSCCAR has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 768947.
