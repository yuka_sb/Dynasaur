
from Data.secforc import Secforc
from Data.nodout import Nodout
from Data.elout import Elout
from Data.rcforc import RCForc
from Data.deforc import DEForc
from Data.volume import Volume
from Data.sbtout import SBTout
from Data.matsum import Matsum
from Data.glstat import Glstat
from constants import ObjectConstantsForData, CodeType


class DataContainer:
    _elout = None
    _secforc = None
    _rcforc = None
    _deforc = None
    _nodout = None
    _volume = None
    _sbtout = None
    _matsum = None
    _glstat = None

    @staticmethod
    def get_data(name):
        if name == ObjectConstantsForData.ELEMENT:
            return DataContainer._elout
        elif name == ObjectConstantsForData.CROSS_SECTION:
            return DataContainer._secforc
        elif name == ObjectConstantsForData.VOLUME:
            return DataContainer._volume
        elif name == ObjectConstantsForData.DISCRETE:
            return DataContainer._deforc
        elif name == ObjectConstantsForData.NODE:
            return DataContainer._nodout
        elif name == ObjectConstantsForData.SEAT_BELT:
            return DataContainer._sbtout
        elif name == ObjectConstantsForData.CONTACT:
            return DataContainer._rcforc
        elif name == ObjectConstantsForData.ENERGY_PART:
            return DataContainer._matsum
        elif name == ObjectConstantsForData.ENERGY_GLOBAL:
            return DataContainer._glstat
        return

    @staticmethod
    def init_all_data_sources(binout, logger, dynasaur_def, binout_dir):
        DataContainer._secforc = Secforc(binout, logger, dynasaur_def)
        DataContainer._elout = Elout(binout, logger, dynasaur_def)
        DataContainer._nodout = Nodout(binout, logger, dynasaur_def, code_type=CodeType.BINOUT)
        DataContainer._deforc = DEForc(binout, logger, dynasaur_def)
        DataContainer._rcforc = RCForc(binout, logger, dynasaur_def)
        DataContainer._sbtout = SBTout(binout, logger, dynasaur_def)
        DataContainer._volume = Volume(binout_dir, logger, dynasaur_def)
        DataContainer._matsum = Matsum(binout, logger, dynasaur_def)
        DataContainer._glstat = Glstat(binout, logger, dynasaur_def)

    @staticmethod
    def init_data_source_nodout(madymo, logger, dynasaur_def):
        DataContainer._nodout = Nodout(madymo, logger, dynasaur_def, code_type=CodeType.MADYMO)

