import abc, time
import numpy as np

from constants import LOGConstants, ObjectConstants, TestConstants


class BinoutData(object):
    def __init__(self, binout, logger, dynasaur_definitions, name):
        self._binout = binout
        self._logger = logger
        self._dynasaur_definitions = dynasaur_definitions
        self._name = name
        self.__init_data__()
        self._defined_types = []
        self._set_defined_types(self._dynasaur_definitions)
        self._clean_defined_types()

    def get_time(self):
        return self._time

    def get_channels_ids_object_name(self, object_name, plugin_name):
        return self._dynasaur_definitions.get_ids_from_name(object_name, self._name, plugin_name)

    def get_ids(self):
        return [int(i) for i in self._ids]

    def __init_data__(self, ids=None, names=None, time=None, data=None):
        self._ids = ids
        self._names = names
        self._time = time
        self._data = data

    def _set_defined_types(self, dynasaur_definitions):
        for def_key in dynasaur_definitions.__dict__:
            if def_key == "_criteria":
                for key in dynasaur_definitions.__dict__[def_key].keys():
                    self._get_data_from_json(dynasaur_definitions.__dict__[def_key][key])
            if def_key == "_data_vis":
                for key in dynasaur_definitions.__dict__[def_key].keys():
                    self._get_data_from_json(dynasaur_definitions.__dict__[def_key][key]["x"])
                    self._get_data_from_json(dynasaur_definitions.__dict__[def_key][key]["y"])

    def _get_data_from_json(self, json_object):
        if 'function' in json_object.keys():  # expected name and params
            parameter_def = json_object['function']['param']

            param_dict = {}
            for key in parameter_def.keys():
                if type(parameter_def[key]) is dict:  # step into recursion
                    param_dict[key] = self._get_data_from_json(parameter_def[key])
                else:
                    param_dict[key] = parameter_def[key]
        else:  # data to obtain
            if "value" in json_object.keys():
                return json_object["value"]

            self._defined_types.append(json_object['array'])
            return json_object['array']

    def _clean_defined_types(self):
        _list_temp = []
        for array_temp in self._defined_types:
            for type in array_temp:
                type = type.replace("(", "")
                type = type.replace(")", "")
                first_type = type.split(",")[0]
                second_type = type.split(",")[1]
                second_type = second_type.replace(" ", "")
                if first_type == "0" and second_type != "0":
                    if second_type in self.TYPES:
                        _list_temp.append(second_type)
                elif first_type != "0" and second_type == '0':
                    if first_type in self.TYPES:
                        _list_temp.append(first_type)
                else:
                    if first_type in self.TYPES:
                        _list_temp.append(first_type)
                    if second_type in self.TYPES:
                        _list_temp.append(second_type)
        self._defined_types = set(_list_temp)

    ##################################################
    # function will be called before a plugin controller is instantiated which needs rcforc data
    #
    # return True when data is read in
    # return False when no data available
    def read_binout_data(self):
        # data already read
        if self._ids is not None:
            return True

        # no rcforc in binout
        if self._name not in self._binout.read():
            self._logger.emit(LOGConstants.ERROR[0], "no " + self._name + " data in binout")
            return False

        # actual nodout read
        self._logger.emit(LOGConstants.READ_BINOUT[0], 'read ' + self._name + ' data ...')
        elements = self._binout.read(self._name)

        #
        # check if required data is available (defined in SBTout.__TYPES__)
        #
        if not self._check_negative_intersection(elements):
            return False

        legend_names = self._read_legend(elements)
        time_interp = self._read_time()
        time = self._binout.read(self._name, 'time')
        data = self._read_data_types(time_interp, time)

        # interpolate data to have a constant sampling rate!
        # TODO
        # split up functionality here!
        id_name = 'belt_ids' if self._name == 'sbtout' else 'ids'
        #TODO: Check parameters maybe time insted of time_interp
        self.__init_data__(self._binout.read(self._name, id_name), legend_names, time_interp, data)
        self._logger.emit(LOGConstants.READ_BINOUT[0], "done reading " + self._name + "!")

        return True

    def _read_legend(self, elements):
        # read legend names
        try:
            # strange error coming from chr() arg not in range(0x110000) in _binout.read()
            legend_length = len(self._binout.read(self._name, 'legend')) // len(self._binout.read(self._name, 'legend_ids'))
            legend = ''.join(i for i in self._binout.read(self._name, 'legend'))
            legend_names = [legend[legend_index:(legend_index + legend_length)].rstrip()
                            for legend_index in range(0, len(legend), legend_length)]
            temp_return_value = self._check_for_id_in_binout()
            if not temp_return_value[0]:
                self._logger.emit(LOGConstants.ERROR[0], "Not valid ID: " + str(temp_return_value[1]) + ". Plese check your def file!")
                time.sleep(.5)
                exit("Exiting")

            return legend_names

        except ValueError:
            self._logger.emit(LOGConstants.WARNING[0], "Something went wrong for reading the legend data in " + self._name + "!")
            return []

        except ZeroDivisionError:
            self._logger.emit(LOGConstants.WARNING[0], "Legend data for " + self._name + " is empty!")
            return []

    def _check_for_id_in_binout(self):
        if self._name == "secforc":
            return self._check_id_in_binout("_cross_sections")
        elif self._name == "nodout":
            return self._check_id_in_binout("_node")
        elif self._name == "rcforc":
            return self._check_id_in_binout("_contact")
        elif self._name == "deforc":
            return self._check_id_in_binout("_discrete")
        elif self._name == "elout":
            return self._check_id_in_binout("_element")
        elif self._name == "sbtout":
            return self._check_id_in_binout("_seatbelt")
        elif self._name == "matsum":
            return self._check_id_in_binout("_energy_part")


    def _check_id_in_binout(self, binout_name):
        ids_list = []
        dyn_def_key = getattr(self._dynasaur_definitions, binout_name)
        for id_temp in self._binout.read(self._name, "ids"):
            ids_list.append(id_temp)

        if len(dyn_def_key) == 0:
            self._logger.emit(LOGConstants.ERROR[0], "The ID \"" + self._name + "\" has no definition in def file")
            time.sleep(.5)
            raise SystemExit("Exiting, check your def file")
        for sec in dyn_def_key:
            for id_for_loop in dyn_def_key[sec]:
                if binout_name == TestConstants.CONTACT:
                    id_for_loop = int(id_for_loop[:-1])


                if id_for_loop in ids_list:
                    continue
                else:
                    return False, id_for_loop
            return True, id_for_loop

    def _check_negative_intersection(self, elements):
        # only necessary functions
        negative_intersection = [val for val in self._defined_types if val not in elements]
        if len(negative_intersection) != 0:
            self._logger.emit(LOGConstants.ERROR[0], "binout keys: " + " ".join(elements))
            self._logger.emit(LOGConstants.ERROR[0], "missing keys: " + " ".join(negative_intersection))
            self._logger.emit(LOGConstants.ERROR[0], 'failed to read ' + self._name + ' data!')
            #TODO: Maybe exit?
            return False
        return True

    def _read_time(self):
        # init nodout data
        time = self._binout.read(self._name, 'time')


        if self._dynasaur_definitions.get_units().second() == 1000: # means ms
            end_time = np.round(time[-1] - time[0])   # assumed ms             
        elif self._dynasaur_definitions.get_units().second() == 1: # means s
            end_time = np.round(time[-1] - time[0], 3)
        else:
            assert False

        time_interp = np.linspace(time[0], end_time, len(time))

        return time_interp

    def _read_data_types(self, time_interp, time, elem_name=None):
        data = {}
        for key in self.__class__.TYPES:
            typ_data = self._binout.read(self._name, key) if elem_name is None else self._binout.read(self._name, elem_name, key)
            # case for elout beam element

            if typ_data.shape[0] != 0:
                if len(typ_data.shape) == 1: # case if only one datapoint available
                    typ_data = typ_data[:time_interp.shape[0]]
                else:
                    typ_data = typ_data[:time_interp.shape[0], :]

            typ_data = np.reshape(typ_data, (time_interp.shape[0], -1))
            data_interp = np.zeros(shape=(time_interp.shape[0], typ_data.shape[1]))
            for i in range(typ_data.shape[1]):
                data_interp[:, i] = np.interp(time_interp, time[:time_interp.shape[0]], typ_data[:, i])
            data[key] = data_interp
        return data

    ''' 
    returns the interpolated channel, 
    Aim: Constant time interval
    '''
    def get_measurement_channel(self, id, channel_name):
        if channel_name == 'time':
            return self._time.reshape(-1, 1)
        data_index = np.where(int(id) == self._ids)[0]

        self._logger.emit(LOGConstants.DATA_PLUGIN[0], 'read id ' + str(id) + ' from channel name: ' + channel_name)
        assert(len(data_index) >= 1)

        if channel_name not in self._data.keys():
            self._logger.emit(LOGConstants.ERROR[0], str(id) + ' has no data with the identifier : ' + channel_name)
            return []

        d = self._data[channel_name][:, data_index]
        return d.reshape(-1, len(data_index))


class BinoutDataInterface(metaclass=abc.ABCMeta):

    # @abc.abstractmethod
    # def __initData__(self):
    #     """Save the data object to the output."""
    #
    # # @abc.abstractmethod
    # # def initData(self):
    # #     """"""

    def _round(self, time_diff):
        shifts = 0
        while time_diff * 10**shifts < 1:
            shifts += 1

        return np.round(time_diff, shifts)




