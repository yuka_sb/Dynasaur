from Data.data_interface import BinoutDataInterface, BinoutData


class DEForc(BinoutDataInterface, BinoutData):

    # TYPES = ['x_moment', 'y_moment', 'z_moment',
    #          'x_force', 'y_force', 'z_force',
    #          'tie_count', 'mass', 'tie_area']

    TYPES = ['x_force', 'y_force', 'z_force',
             'displacement', 'resultant_force']

    def __init__(self, binout, logger, dynasaur_definitions):
        BinoutData.__init__(self, binout, logger, dynasaur_definitions, 'deforc')

    # def __initData__(self, ids=None, names=None, time=None, data=None):
    #     self._ids = ids
    #     self._names = names
    #     self._time = time
    #     self._data = data
