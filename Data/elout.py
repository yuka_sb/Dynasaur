# ***********************************************************************************************************************
#
# Jakub Micorek
# Martin Schachner
#
# ***********************************************************************************************************************
from constants import LOGConstants

from Data.data_interface import BinoutDataInterface, BinoutData
from numpy.lib.stride_tricks import as_strided
from enum import Enum

import numpy as np
import time as t



class EloutIndex(Enum):
    STRAIN = 0
    STRESS = 1

    INDEX = 2
    DATA = 3

    SHELL = 4
    SOLID = 5
    BEAM = 6

    @staticmethod
    def translate_element(name):
        name = name.lower()
        if name == "shell":
            return EloutIndex.SHELL
        if name == "solid":
            return EloutIndex.SOLID
        if name == "beam":
            return EloutIndex.BEAM

    @staticmethod
    def translate_strain_stress(name):
        name = name.lower()
        if name == "strain":
            return EloutIndex.STRAIN
        if name == "stress":
            return EloutIndex.STRESS


class Elout(BinoutDataInterface, BinoutData):

    # applies for beam elements!
    TYPES = ['axial', 'shear_s', 'shear_t', 'moment_s', 'moment_t', 'torsion']

    def __init__(self, binout, logger, dynasaur_definitions):
        BinoutData.__init__(self, binout, logger, dynasaur_definitions, 'elout')
        self.__init_elout_data()

    def __init_elout_data(self):
        self._data_mapping_part_id_to_element_type = {}
        self._data_mapping_element_id_to_part_id = {}
        self._data_mapping_part_id_to_element_ids = {}
        self._data = {}

        self._time = None

    ##################################################
    def __append_data(self, index_matrix, data_tensor, strain_stress_type, element_type):
        if strain_stress_type not in self._data:
            self._data[strain_stress_type] = {}

        self._data[strain_stress_type][element_type] = {}
        # indexing: [element id, part id, integration point]
        self._data[strain_stress_type][element_type][EloutIndex.INDEX] = index_matrix

        # shape: [time]:[element id, part id, integration point]:[lambda1, lambda2, lambda3]
        #
        # e.g.: 7 time steps, 3 parts, each part 2 elements with 2 integration points, 3 eigenvalues
        # e.g.: results in shape of (7, 12, 3)
        self._data[strain_stress_type][element_type][EloutIndex.DATA] = data_tensor

    ##################################################
    def __append_mapping_part_id_to_element_type(self, index_matrix, strain_stress_type, element_type):
        if strain_stress_type not in self._data_mapping_part_id_to_element_type:
            self._data_mapping_part_id_to_element_type[strain_stress_type] = {}

        unique_part_ids = np.unique(index_matrix[:, 1])
        for part_id in unique_part_ids:
            if part_id not in self._data_mapping_part_id_to_element_type[strain_stress_type]:
                self._data_mapping_part_id_to_element_type[strain_stress_type][part_id] = element_type
            else:
                self._logger.emit(LOGConstants.ERROR[0], "elout: part consists of multiple element types")
                self._logger.emit(LOGConstants.ERROR[0], "elout: could not read")
                self._data = {}

    ##################################################
    def __append_mapping_element_id_to_part_id(self, index_matrix, strain_stress_type, elout_elem):
        if strain_stress_type not in self._data_mapping_element_id_to_part_id:
            self._data_mapping_element_id_to_part_id[strain_stress_type] = {}

        # elout_elem can be solid, shell or beam ...
        if elout_elem not in self._data_mapping_element_id_to_part_id[strain_stress_type]:
            self._data_mapping_element_id_to_part_id[strain_stress_type][elout_elem] = {}

        # TODO: find a faster method to filter unique element ids paired with part ids
        unique_element_part_tuple = np.vstack({tuple(row) for row in index_matrix[:, [0, 1]]})
        for element_part_tuple in unique_element_part_tuple:
            if element_part_tuple[0] not in self._data_mapping_element_id_to_part_id[strain_stress_type][elout_elem]:
                self._data_mapping_element_id_to_part_id[strain_stress_type][elout_elem][element_part_tuple[0]] = \
                    element_part_tuple[1]
            else:
                self._logger.emit(LOGConstants.ERROR[0], "elout: one element is in multiple parts!")
                self._logger.emit(LOGConstants.ERROR[0], "elout: could not be read")
                self._data = {}

    ##################################################
    def __append_mapping_part_id_to_element_ids(self, strain_stress_type):
        if strain_stress_type not in self._data_mapping_element_id_to_part_id:
            return

        if strain_stress_type not in self._data_mapping_part_id_to_element_ids:
            self._data_mapping_part_id_to_element_ids[strain_stress_type] = {}

        for elout_elem in self._data_mapping_element_id_to_part_id[strain_stress_type]:
            for element_id in self._data_mapping_element_id_to_part_id[strain_stress_type][elout_elem]:
                part_id = self._data_mapping_element_id_to_part_id[strain_stress_type][elout_elem][element_id]
                if part_id not in self._data_mapping_part_id_to_element_ids[strain_stress_type]:
                    self._data_mapping_part_id_to_element_ids[strain_stress_type][part_id] = []
                self._data_mapping_part_id_to_element_ids[strain_stress_type][part_id].append(element_id)

    ##################################################
    def __concatenated_ranges(self, ranges_list):
        ranges_list = np.array(ranges_list, copy=False)
        base_range = np.arange(1, ranges_list.max() + 1)
        base_range = as_strided(base_range,
                                shape=ranges_list.shape + base_range.shape,
                                strides=(0,) + base_range.strides)
        return base_range[base_range <= ranges_list[:, None]]
        # return base_range.flatten()

    ##################################################
    def __get_data(self, part_id, strain_stress_type):
        element_type = self._data_mapping_part_id_to_element_type[strain_stress_type][part_id]
        return self._data[strain_stress_type][element_type]

    ##################################################
    def _get_all_part_ids(self):
        return np.unique([y for x in [list(self._data_mapping_part_id_to_element_type[vals].keys()) for vals in
                                      self._data_mapping_part_id_to_element_type] for y in x])

    def __get_part_ids(self, strain_stress_type):
        return list(self._data_mapping_part_id_to_element_type[strain_stress_type].keys())

    ##################################################
    # get the data from binout and extend the given indices with dummy values
    def __get_elout_data_and_extend(self, elout_elem, name, extendable_indices):
        if extendable_indices.size == 0:
            return self._binout.read("elout", elout_elem, name).reshape((-1, 1))

        # merge lower and upper integration points for
        if name.startswith("eps_") and elout_elem == "shell":
            # flatted_data_array = np.list(sum(self._binout.read("elout", elout_elem, name),())) list(sum(self._binout.read("elout", elout_elem, name),()))
            lower = list(sum(self._binout.read("elout", elout_elem, "lower_" + name), ()))
            upper = list(sum(self._binout.read("elout", elout_elem, "upper_" + name), ()))
            nr_elements = len(lower)
            ind_array = np.arange(1, nr_elements + 1)

            flatted_data_array = np.insert(lower, ind_array, upper)

        else:
            flatted_data_array = list(sum(self._binout.read("elout", elout_elem, name), ()))

        zeros = np.zeros(len(extendable_indices))

        a = np.insert(flatted_data_array, extendable_indices, zeros).reshape((-1, 1)),

        return a[0]

    ##################################################
    # duplicate all entries of first time entry.
    # part id, integration points, element id
    def __info_matrix(self, elout_elem, time, what):
        if ((elout_elem == "shell" or elout_elem == "beam") and what == "stress"):
            # ok
            nip = np.tile(self._binout.read('elout', elout_elem, 'nip')[0], time.shape[0]).flatten()
            mat = np.repeat(np.tile(self._binout.read('elout', elout_elem, 'mat')[0], time.shape[0]).flatten(),
                            nip).reshape((-1, 1))
            ipts = self.__concatenated_ranges(nip).reshape(-1, 1)
            ids = np.repeat(np.tile(self._binout.read('elout', elout_elem, 'ids')[0], time.shape[0]).flatten(),
                            nip).reshape((-1, 1))

        elif (elout_elem == "shell" and what == "strain"):
            # ok
            nr_elements = time.shape[0] if isinstance(self._binout.read('elout', elout_elem, 'ids')[0],
                                                      np.int32) else len(
                self._binout.read('elout', elout_elem, 'ids')[0]) * time.shape[0]
            rep_strain = np.repeat([2], nr_elements, axis=0)
            ids = np.repeat(np.tile(self._binout.read('elout', elout_elem, 'ids')[0], time.shape[0]).flatten(),
                            rep_strain).reshape((-1, 1))
            mat = np.repeat(np.tile(self._binout.read('elout', elout_elem, 'mat')[0], time.shape[0]).flatten(),
                            rep_strain).reshape((-1, 1))
            ipts = np.repeat([[1, 2]], nr_elements, axis=0).reshape(-1, 1)

        elif elout_elem == "solid":
            # ok
            mat = np.tile(self._binout.read('elout', elout_elem, 'mtype')[0], time.shape[0]).flatten().reshape(-1, 1)
            ipts = np.ones(mat.size).reshape(-1, 1)
            ids = np.tile(self._binout.read('elout', elout_elem, 'ids')[0], time.shape[0]).flatten().reshape(-1, 1)

        if len(mat) == 0 or len(ids) == 0 or len(ipts) == 0:
            self._logger.emit(LOGConstants.READ_BINOUT[0], ("nothing to extract"))
            return None

        return (np.concatenate((ids, mat, ipts), axis=1)).astype(int)

    ####################################################################################################################
    #   indices list of destroyed elements over time
    #   used to insert dummy values [used for padding to full sized matrix
    #     -> meaning, each element is available over the entire time]
    #
    #  Functionality:
    #       in case elements are the destroyed during the simulation
    #       ids = self._binout.read('elout', elout_elem, 'ids') has different lengths for each timestep
    #
    #   NOTE:
    #       if   : stress - shell : extensions for the integration points
    #
    #   RETURN:
    #        return 1.) insert_index
    def __indices_of_destroyed_elements(self, elout_elem, type):

        array = []
        insert_indices = np.array([], dtype=int)
        ind = np.array([], dtype=int)

        ids = self._binout.read('elout', elout_elem, 'ids')

        # check if elements have been destroyed
        if isinstance(ids[0], np.int32):
            ids = [np.array([i]) for i in ids]
        if sum([len(i) - len(ids[0]) for i in ids]) == 0:
            return insert_indices, ind

        # if stress and shell, there might be various integration points:
        #    the read ids have to be extended accordingly
        if type == "stress" and elout_elem == "shell":
            nip = self._binout.read('elout', elout_elem, 'nip')
            ids_nip = []
            for i in range(0, ids.shape[0]):
                id = np.repeat(ids[i], nip[i]).reshape((-1, 1))
                ids_nip.append(id)

            ids = np.array(ids_nip)

        len_indices = 0

        # calculate where dummy values have to be inserted -> guarantees equal length for each timestep
        #  1) insert_indices
        #  2) the real index of the element
        for i in range(0, ids.shape[0]):
            logic_intersection = np.in1d(ids[0], ids[i])
            array.append(
                np.where(np.logical_not(logic_intersection)))  # np.where(np.logical_not(np.in1d(ids[0], ids[i])))])
            indices = np.where(np.logical_not(logic_intersection))
            if indices[0].size != 0:
                insert_indices = np.append(insert_indices, indices[0] + len(ids[0]) * i - np.arange(len_indices,
                                                                                                    len_indices + len(
                                                                                                        indices[0])))
                ind = np.append(ind, indices[0] + len(ids[0]) * i)

                len_indices += len(indices[0])

        return insert_indices, ind

    ####################################################################################################################
    # set dummy values to NaN (done for deleted elements)
    def __set_nans(self, w_stress, ind):
        if ind.size != 0:
            self._logger.emit(LOGConstants.WARNING[0], "Evaluated object contains failed elements!")
            w_stress[ind] = np.tile(np.array([np.NaN]), 3)

    ####################################################################################################################
    # calculate stress from the elout_elem
    def __assign_stresses(self, elout_elem):
        if (elout_elem != "solid" and elout_elem != "shell" and elout_elem != "beam"):
            return

        self._logger.emit(LOGConstants.READ_BINOUT[0], "elout: initialize stress " + elout_elem)
        self._logger.emit(LOGConstants.READ_BINOUT[0], "assign stresses for " + elout_elem + " elements")
        start = t.time()

        time = self._binout.read('elout', elout_elem, 'time').flatten()

        elem_part_ipts_info = self.__info_matrix(elout_elem, time, what="stress")

        if elem_part_ipts_info is None:
            return

        # 1) get indices of destroyed elements
        (insert_indices, ind) = self.__indices_of_destroyed_elements(elout_elem, "stress")

        # 2) extract sigma
        if elout_elem == "shell" or elout_elem == "solid":
            sig = np.concatenate((self.__get_elout_data_and_extend(elout_elem, "sig_xx", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sig_xy", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sig_zx", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sig_xy", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sig_yy", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sig_yz", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sig_zx", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sig_yz", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sig_zz", insert_indices)), axis=1)

        elif elout_elem == "beam":
            # sigma_11 = sig_xx, sigma_12 = sig_xy, sigma_31 = sig_zx
            sig = np.concatenate((self.__get_elout_data_and_extend(elout_elem, "sigma_11", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sigma_12", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sigma_31", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "sigma_12", insert_indices),
                                  np.zeros((elem_part_ipts_info.shape[0], 1)),
                                  np.zeros((elem_part_ipts_info.shape[0], 1)),
                                  self.__get_elout_data_and_extend(elout_elem, "sigma_31", insert_indices),
                                  np.zeros((elem_part_ipts_info.shape[0], 1)),
                                  np.zeros((elem_part_ipts_info.shape[0], 1))), axis=1)

        self._logger.emit(LOGConstants.READ_BINOUT[0], ("extract data took : " + str(t.time() - start)))

        # 3) get eigenvalues of the matrix
        self._logger.emit(LOGConstants.READ_BINOUT[0], "calc eigenvalue")

        start = t.time()
        w_stress = np.linalg.eigvalsh(np.reshape(sig, (-1, 3, 3)))
        self.__set_nans(w_stress, ind)
        self._logger.emit(LOGConstants.READ_BINOUT[0], ("calc eigenvalues took : " + str(t.time() - start)))

        time_step_size = len(self._time)
        element_type = EloutIndex.translate_element(elout_elem)
        # shape: [time]:[element id, part id, integration point]:[lambda1, lambda2, lambda3]
        #
        # e.g.: 7 time steps, 3 parts, each part 2 elements with 2 integration points, 3 eigenvalues
        # e.g.: results in shape of (7, 12, 3)
        data_tensor = w_stress.reshape(time_step_size, int(w_stress.shape[0] / time_step_size), w_stress.shape[1])

        # indexing: [element id, part id, integration point]
        index_matrix = elem_part_ipts_info[0:int(elem_part_ipts_info.shape[0] / time_step_size), :].astype(int)

        self.__append_data(index_matrix, data_tensor, EloutIndex.STRESS, element_type)
        self.__append_mapping_part_id_to_element_type(index_matrix, EloutIndex.STRESS, element_type)
        self.__append_mapping_element_id_to_part_id(index_matrix, EloutIndex.STRESS, elout_elem)

    ##################################################
    def __assign_strains(self, elout_elem):
        # check the cases where to reject to read strains (if shells or solids do not have an epsilon component)
        self._logger.emit(LOGConstants.READ_BINOUT[0], ("assign strains for " + elout_elem + " elements"))

        if elout_elem != "solid" and elout_elem != "shell":
            self._logger.emit(LOGConstants.READ_BINOUT[0], ("nothing to extract"))
            return

        if elout_elem == "shell" and "lower_eps_xx" not in self._binout.read('elout', elout_elem):
            self._logger.emit(LOGConstants.READ_BINOUT[0], ("nothing to extract"))
            return

        if elout_elem == "solid" and "eps_xx" not in self._binout.read('elout', elout_elem):
            self._logger.emit(LOGConstants.READ_BINOUT[0], ("nothing to extract"))
            return

        self._logger.emit(LOGConstants.READ_BINOUT[0], "elout: initialize strain " + elout_elem)

        start = t.time()

        # depends on elout_elem
        time = self._binout.read('elout', elout_elem, 'time').flatten()
        elem_part_ipts_info = self.__info_matrix(elout_elem, time, what="strain")

        if elem_part_ipts_info is None:
            return

        (insert_indices, ind) = self.__indices_of_destroyed_elements(elout_elem, "strain")

        if elout_elem == "shell":
            nr_elements = time.shape[0] if isinstance(self._binout.read('elout', elout_elem, 'ids')[0],
                                                      np.int32) else len(
                self._binout.read('elout', elout_elem, 'ids')[0]) * time.shape[0]

            ind_array = np.arange(1, nr_elements + 1)
            eps = np.concatenate((np.insert(
                self.__get_elout_data_and_extend(elout_elem, "lower_eps_xx", insert_indices).flatten(), ind_array,
                self.__get_elout_data_and_extend(elout_elem, "upper_eps_xx", insert_indices).flatten()).reshape(-1, 1),
                                  np.insert(self.__get_elout_data_and_extend(elout_elem, "lower_eps_xy",
                                                                             insert_indices).flatten(), ind_array,
                                            self.__get_elout_data_and_extend(elout_elem, "upper_eps_xy",
                                                                             insert_indices).flatten()).reshape(-1, 1),
                                  np.insert(self.__get_elout_data_and_extend(elout_elem, "lower_eps_zx",
                                                                             insert_indices).flatten(), ind_array,
                                            self.__get_elout_data_and_extend(elout_elem, "upper_eps_zx",
                                                                             insert_indices).flatten()).reshape(-1, 1),
                                  np.insert(self.__get_elout_data_and_extend(elout_elem, "lower_eps_xy",
                                                                             insert_indices).flatten(), ind_array,
                                            self.__get_elout_data_and_extend(elout_elem, "upper_eps_xy",
                                                                             insert_indices).flatten()).reshape(-1, 1),
                                  np.insert(self.__get_elout_data_and_extend(elout_elem, "lower_eps_yy",
                                                                             insert_indices).flatten(), ind_array,
                                            self.__get_elout_data_and_extend(elout_elem, "upper_eps_yy",
                                                                             insert_indices).flatten()).reshape(-1, 1),
                                  np.insert(self.__get_elout_data_and_extend(elout_elem, "lower_eps_yz",
                                                                             insert_indices).flatten(), ind_array,
                                            self.__get_elout_data_and_extend(elout_elem, "upper_eps_yz",
                                                                             insert_indices).flatten()).reshape(-1, 1),
                                  np.insert(self.__get_elout_data_and_extend(elout_elem, "lower_eps_zx",
                                                                             insert_indices).flatten(), ind_array,
                                            self.__get_elout_data_and_extend(elout_elem, "upper_eps_zx",
                                                                             insert_indices).flatten()).reshape(-1, 1),
                                  np.insert(self.__get_elout_data_and_extend(elout_elem, "lower_eps_yz",
                                                                             insert_indices).flatten(), ind_array,
                                            self.__get_elout_data_and_extend(elout_elem, "upper_eps_yz",
                                                                             insert_indices).flatten()).reshape(-1, 1),
                                  np.insert(self.__get_elout_data_and_extend(elout_elem, "lower_eps_zz",
                                                                             insert_indices).flatten(), ind_array,
                                            self.__get_elout_data_and_extend(elout_elem, "upper_eps_zz",
                                                                             insert_indices).flatten()).reshape(-1, 1)),
                                 axis=1)

        elif elout_elem == "solid":
            eps = np.concatenate((self.__get_elout_data_and_extend(elout_elem, "eps_xx", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "eps_xy", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "eps_zx", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "eps_xy", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "eps_yy", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "eps_yz", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "eps_zx", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "eps_yz", insert_indices),
                                  self.__get_elout_data_and_extend(elout_elem, "eps_zz", insert_indices)), axis=1)

        self._logger.emit(LOGConstants.READ_BINOUT[0], ("extract data took : " + str(t.time() - start)))
        self._logger.emit(LOGConstants.READ_BINOUT[0], "calc eigenvalue")
        start = t.time()
        w_strain = np.linalg.eigvalsh(np.reshape(eps, (-1, 3, 3)))
        self.__set_nans(w_strain, ind)
        self._logger.emit(LOGConstants.READ_BINOUT[0], ("calc eigenvalues took : " + str(t.time() - start)))

        time_step_size = len(self._time)
        element_type = EloutIndex.translate_element(elout_elem)
        # shape: [time]:[element id, part id, integration point]:[lambda1, lambda2, lambda3]
        #
        # e.g.: 7 time steps, 3 parts, each part 2 elements with 2 integration points, 3 eigenvalues
        # e.g.: results in shape of (7, 12, 3)
        data_tensor = w_strain.reshape(time_step_size, int(w_strain.shape[0] / time_step_size), w_strain.shape[1])

        # indexing: [element id, part id, integration point]
        index_matrix = elem_part_ipts_info[0:int(elem_part_ipts_info.shape[0] / time_step_size), :].astype(int)

        self.__append_data(index_matrix, data_tensor, EloutIndex.STRAIN, element_type)
        self.__append_mapping_part_id_to_element_type(index_matrix, EloutIndex.STRAIN, element_type)
        self.__append_mapping_element_id_to_part_id(index_matrix, EloutIndex.STRAIN, elout_elem)

    ####################################################################################################################
    # function will be called before a plugin controller is instantiated which needs elout data
    #
    # return True when data is read in
    # return False when no data available
    def read_binout_data(self):
        # data already read
        if len(self._data) != 0:
            return True

        # no elout in binout
        if "elout" not in self._binout.read():
            self._logger.emit(LOGConstants.ERROR[0], "no elout data in binout!")
            return False

        # actual elout read
        elements = self._binout.read('elout')

        if len(elements) == 0:
            self._logger.emit(LOGConstants.ERROR[0], 'no elout data available')
            return False

        self._logger.emit(LOGConstants.READ_BINOUT[0], 'read elout data ...')


        # Problem if the timed data from the element has different lengths
        time_lengths = [len(self._binout.read('elout', elem, 'time').flatten()) for elem in elements]
        assert(all([time_lengths[0] == length for length in time_lengths]))

        self._time = self._binout.read('elout', elements[0], 'time').flatten()

        for elout_elem in elements:
            if elout_elem not in ['shell', 'solid', "beam"] or len(self._binout.read('elout', elout_elem, 'ids')) == 0:
                self._logger.emit(LOGConstants.WARNING[0], elout_elem + " not supported or does not contain ids")
                continue

            self.__assign_strains(elout_elem)
            self.__assign_stresses(elout_elem)
            if elout_elem == 'beam':
                # write time and and timestep
                if not self._check_negative_intersection(self._binout.read('elout', 'beam')):
                    return False

                self._names = self._read_legend(elements)
                time_interp = self._read_time(elements)
                time = self._binout.read('elout', elout_elem, 'time').flatten()
                data = self._read_data_types(time_interp, time, elem_name='beam')
                # merge data
                self._data = {**self._data, **data}
                self._ids = self._binout.read(self._name, 'beam', 'ids')[0]



        self.__append_mapping_part_id_to_element_ids(EloutIndex.STRAIN)
        self.__append_mapping_part_id_to_element_ids(EloutIndex.STRESS)

        self._dynasaur_definitions.define_dynasaur_everything(self._get_all_part_ids())

        # append general data to the self._data

        self._logger.emit(LOGConstants.READ_BINOUT[0], "done reading elout!")
        return True

    def _read_time(self, elements):
        # init nodout data
        time = self._binout.read('elout', elements[0], 'time').flatten()

        # round time_diff

        if len(time) < 2:
            self._logger.emit(LOGConstants.WARNING[0], "The simulation time has only one timestep!")
            return time

        time_diff = time[1] - time[0]
        rounded_time_diff = self._round(time_diff)
        time_interp = np.arange(0, time[-1] + rounded_time_diff * 0.1, rounded_time_diff)
        return time_interp

    def get_data_mapping_part_i_dto_element_ids(self, strain_stress_type):
        return self._data_mapping_part_id_to_element_ids[strain_stress_type]

    def get_element_type_from_part_id(self, part_id, strain_stress_type):
        return self._data_mapping_part_id_to_element_type[strain_stress_type][part_id]

    def get_defined_objects(self):
        return self._dynasaur_definitions.get_defined_objects_containing_parts(self._get_all_part_ids())

    def get_defined_time_steps(self, append_string_all):
        if append_string_all:
            list_time_step = ["All"]
        else:
            list_time_step = []

        for time_step in sorted(self._time):
            list_time_step.append(str(time_step))

        return list_time_step

    def get_part_ids_by_object_name(self, object_name, strain_stress_type):
        return self._dynasaur_definitions.get_parts_by_object_containing_part_ids(object_name,
                                                                                  self.__get_part_ids(strain_stress_type))
    def get_element_ids_by_part_id(self, part_id, strain_stress_type):
        return self._data_mapping_part_id_to_element_ids[strain_stress_type][part_id]

    def get_time(self):
        return self._time

    ###########################################################
    # description: return filtered index matrix and data tensor over all time steps for a given part_id
    #   will return all element, part, integration combinations
    #
    # optional parameter element_id: filter additional element_id with part_id
    #   will return just the element part combination with all their integration points
    #
    # optional parameter time_step_indices: reduce tensor for given time step indices
    #   will return a reduced data tensor with the given time step indices
    #   if None: will return all time steps -> full tensor
    #
    def get_part_data(self, strain_stress_type, part_id, element_id=None, time_step_indices=None):
        tmp_data = self.__get_data(part_id, strain_stress_type)

        if element_id is None:
            row_index = np.where(tmp_data[EloutIndex.INDEX][:, 1] == part_id)[0]
        else:
            row_index = \
                np.where((tmp_data[EloutIndex.INDEX][:, 1] == part_id) & (tmp_data[EloutIndex.INDEX][:, 0] == element_id))[
                    0]

        if time_step_indices is None:
            data = tmp_data[EloutIndex.DATA][:, row_index, :]
        else:
            data = (tmp_data[EloutIndex.DATA][:, row_index, :])[time_step_indices, :, :]

        return tmp_data[EloutIndex.INDEX][row_index, :], data

    def get_element_type_name(self, element_id, strain_stress_type):
        #part_id = self._data_mapping_element_id_to_part_id[strain_stress_type][element_id]
        element_types = []
        for element_type in self._data_mapping_element_id_to_part_id[strain_stress_type].keys():
            if element_id in self._data_mapping_element_id_to_part_id[strain_stress_type][element_type]:
                part_id = self._data_mapping_element_id_to_part_id[strain_stress_type][element_type][element_id]
                element_types.append(self._data_mapping_part_id_to_element_type[strain_stress_type][part_id].name)
        return element_types

    ###########################################################
    # further filtering by element_id of given results from getPartData()
    #
    # index and data are return values of getPartData()
    #
    # return row indices of filtered elements
    # - returns list with one indices when just one integration point is available
    # - returns list with multiple indices when multiple integration points are available
    def get_integration_point_indices_of_part_data_by_element_id(self, index, element_id):
        return np.where(index[:, 0] == element_id)[0]

    ###########################################################
    # parameter uses index matrix of getPartData()
    # using this function instead of the data_mappings will enhance performance
    #
    # get element IDs of filtered getPartData()
    def get_element_ids_of_part_data(self, index):
        return np.unique(index[:, 0])

    def data_contains_type(self, strain_stress_type):
        return strain_stress_type in self._data
