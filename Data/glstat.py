from Data.data_interface import BinoutDataInterface, BinoutData
from constants import LOGConstants
import numpy as np


class Glstat(BinoutDataInterface, BinoutData):

    #TODO: Types
    TYPES = ['global_y_velocity', 'internal_energy',
             'kinetic_energy', 'percent_increase',
             'ts_element', 'energy_ratio_wo_eroded',
             'spring_and_damper_energy', 'eroded_kinetic_energy',
             'num_bad_shells', 'energy_ratio', 'external_work', 'time_step',
             'global_z_velocity', 'global_x_velocity',
             'eroded_internal_energy', 'time', 'total_energy', 'system_damping_energy',
             'hourglass_energy', 'sliding_interface_energy', 'cycle', 'ts_eltype',
             'joint_internal_energy', 'eroded_hourglass_energy', 'nzc', 'added_mass']

    def __init__(self, binout, logger, dynasaur_definitions):
        #TODO: ID from binout
        BinoutData.__init__(self, binout, logger, dynasaur_definitions, 'glstat')


    def get_measurement_channel(self, id, channel_name):
        if channel_name == 'time':
            return self._time.reshape(-1, 1)

        if channel_name not in self._data.keys():
            self._logger.emit(LOGConstants.ERROR[0], str(id) + ' has no data with the identifier : ' + channel_name)
            return []
        self._logger.emit(LOGConstants.DATA_PLUGIN[0], 'ENERGY_GLOBAL read from channel name: ' + channel_name)

        d = self._data[channel_name]
        return d