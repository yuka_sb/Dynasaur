from Data.data_interface import BinoutDataInterface, BinoutData


class Matsum(BinoutDataInterface, BinoutData):

    #TODO: Types
    TYPES = ['x_momentum', 'y_rbvelocity', 'z_rbvelocity', 'internal_energy',
              'kinetic_energy', 'max_shell_mass',
              'y_momentum', 'mass', 'eroded_kinetic_energy', 'brick_id',
              'max_brick_mass', 'shell_id', 'eroded_internal_energy', 'time',
             'z_momentum', 'hourglass_energy', 'eroded_hourglass_energy' 'x_rbvelocity']

    def __init__(self, binout, logger, dynasaur_definitions):
        BinoutData.__init__(self, binout, logger, dynasaur_definitions, 'matsum')
