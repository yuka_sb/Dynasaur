# **********************************************************************************************************************
#
# Martin Schachner
#
# **********************************************************************************************************************

from Data.data_interface import BinoutDataInterface, BinoutData
from Data.madymo import Madymo
from constants import CodeType

class Nodout(BinoutDataInterface, BinoutData, Madymo):
    TYPES = ['rx_velocity', 'ry_velocity', 'rz_velocity',              # BRIC
             'rx_displacement', 'ry_displacement', 'rz_displacement',  #
             'rx_acceleration', 'ry_acceleration', 'rz_acceleration',
             'x_coordinate', 'y_coordinate', 'z_coordinate',           # general Kinematics
             'x_displacement', 'y_displacement', 'z_displacement',
             'x_velocity', 'y_velocity', 'z_velocity',
             'x_acceleration', 'y_acceleration', 'z_acceleration']     # HIC, a3ms
             # 'title', 'version', 'legend', 'legend_ids', 'time',
             # 'date', 'revision', 'ids', 'cycle']

    def __init__(self, data, logger, dynasaur_definitions, code_type):
        self._code_type = code_type
        if code_type == CodeType.BINOUT:
            BinoutData.__init__(self, data, logger, dynasaur_definitions, 'nodout')
        else:
            Madymo.__init__(self, data, logger, dynasaur_definitions, 'nodout')

    def get_channels_ids_object_name(self, object_name, plugin_name):
        if self._code_type == CodeType.BINOUT:
            return BinoutData.get_channels_ids_object_name(self, object_name, plugin_name)
        else:
            return Madymo.get_channels_ids_object_name(self, object_name, plugin_name)

    def get_measurement_channel(self, id, channel_name):
        if self._code_type == CodeType.BINOUT:
            return BinoutData.get_measurement_channel(self, id, channel_name)
        else:
            return Madymo.get_measurement_channel(self, id, channel_name)

    def get_time(self):
        if self._code_type == CodeType.BINOUT:
            return BinoutData.get_time(self)
        else:
            return Madymo.get_time(self)
