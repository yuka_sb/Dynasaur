import numpy as np
from constants import LOGConstants
from Data.data_interface import BinoutDataInterface, BinoutData


class RCForc(BinoutData, BinoutDataInterface):

    TYPES = ['x_moment', 'y_moment', 'z_moment',
             'x_force', 'y_force', 'z_force',
             'tie_count', 'mass', 'tie_area']

    def __init__(self, binout, logger, dynasaur_definitions):
        BinoutData.__init__(self, binout, logger, dynasaur_definitions, "rcforc")

    # def __initData__(self, ids=None, names=None, time=None, data=None):
    #     self._ids = ids
    #     self._names = names
    #     self._time = time
    #     self._data = data

    def get_measurement_channel(self, id, channel_name):
        if channel_name == 'time':
            return self._time.reshape(-1, 1)

        # split id into number and master slave indicator
        id_nr = int(id[:-1])
        id_slave_master = id[-1]

        assert(id_slave_master == 's' or id_slave_master == 'm')

        id_slave_master = 0 if id_slave_master == 's' else 1
        data_index = np.where(int(id_nr) == self._ids)[0]

        data_index = [data_index[id_slave_master]]

        self._logger.emit(LOGConstants.DATA_PLUGIN[0], 'read id ' + str(id) + ' from channel name: ' + channel_name)
        assert(len(data_index) >= 1)

        if channel_name not in self._data.keys():
            self._logger.emit(LOGConstants.ERROR[0], str(id) + ' has no data with the identifier : ' + channel_name)
            return []

        d = self._data[channel_name][:, data_index]
        return d.reshape(-1, len(data_index))
