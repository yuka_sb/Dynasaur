# **********************************************************************************************************************
#
# Martin Schachner
#
# **********************************************************************************************************************

from Data.data_interface import BinoutDataInterface, BinoutData


class SBTout(BinoutData, BinoutDataInterface):

    TYPES = ['belt_length', 'belt_force']

    def __init__(self, binout, logger, dynasaur_definitions):
        BinoutData.__init__(self, binout, logger, dynasaur_definitions, "sbtout")

    # def __initData__(self, ids=None, names=None, time=None, data=None):
    #     self._ids = ids
    #     self._names = names
    #     self._time = time
    #     self._data = data