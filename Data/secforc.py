# ***********************************************************************************************************************
#
# Jakub Micorek
#
# ***********************************************************************************************************************
import numpy as np

from constants import LOGConstants
from Data.data_interface import BinoutDataInterface, BinoutData


class Secforc(BinoutDataInterface, BinoutData):
    TYPES = ['x_force', 'y_force', 'z_force', 'total_force',
             'x_moment', 'y_moment', 'z_moment', 'total_moment',
             'x_centroid', 'y_centroid', 'z_centroid', 'area']

    def __init__(self, binout, logger, dynasaur_definitions):
        BinoutData.__init__(self, binout, logger, dynasaur_definitions, 'secforc')

    # def __initData__(self, ids=None, names=None, time=None, data=None):
    #     self._ids = ids
    #     self._names = names
    #     self._time = time
    #     self._data = data

    def __get_indices_of_ids(self, ids):
        tmp_ids = self._ids.tolist()
        return [tmp_ids.index(val) for val in tmp_ids if val in ids]

    def get_defined_section_names(self):
        return self._dynasaur_definitions.get_section_names_containing_ids(self._ids)

    def get_max_secforc_of_section(self, secforc_section, secforc_type):
        selected_ids = self._dynasaur_definitions.get_defined_sections_containing_ids(self._ids)[secforc_section]
        ids = self.__get_indices_of_ids(selected_ids)
        selected_data = self._data[secforc_type][:, ids]
        row_result_idx = np.argmax(selected_data, axis=0)
        max_values = selected_data[row_result_idx, range(0, len(row_result_idx))]

        result_idx = np.argmax(row_result_idx)

        true_idx_column = ids[result_idx]
        true_idx_row = row_result_idx[result_idx]

        # return results: max value, time when max value occurred, ID of max value, name/title/legend of max value
        return max_values[result_idx], self._time[true_idx_row], self._ids[true_idx_column], self._names[
            true_idx_column]
