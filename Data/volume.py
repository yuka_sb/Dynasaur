# ***********************************************************************************************************************
#
# Jakub Micorek
#
# ***********************************************************************************************************************
import numpy as np

from constants import LOGConstants
from Data.elout import EloutIndex


class Volume:
    def __init__(self, path, logger, dynasaur_definitions):
        self._path = path
        self._logger = logger
        self._dynasaur_definitions = dynasaur_definitions

        self._file_data_mapping_element_id_to_volume = None

        self._data_mapping_part_id_to_volume = {}
        self._data_mapping_part_id_to_element_id_to_volume = {}

    def _read_volume_data_from_file(self):
        if self._file_data_mapping_element_id_to_volume is not None:
            return True

        self._logger.emit(LOGConstants.READ_VOLUME[0], "read volume data")

        try:
            tmp_file = open(self._path + "/Volume.key", "r")
        except:
            self._logger.emit(LOGConstants.ERROR[0], "volume: could not read Volume.key file!")
            return False

        tmp_data = np.array([line.split() for line in tmp_file.readlines() if line[0].isdigit()])
        self._file_data_mapping_element_id_to_volume = {int(key): float(value) for (key, value) in tmp_data}

        self._logger.emit(LOGConstants.READ_VOLUME[0], "done reading volume!")
        tmp_file.close()
        return True

    def _init_data(self, data_mapping_part_id_to_element_ids):
        if len(self._data_mapping_part_id_to_volume) != 0:
            return True

        error_part_ids = []
        for part_id, elements in data_mapping_part_id_to_element_ids.items():
            try:
                part_volumes = {element_id: self._file_data_mapping_element_id_to_volume[element_id] for element_id in
                                elements}
                self._data_mapping_part_id_to_volume[part_id] = sum(part_volumes.values())
                self._data_mapping_part_id_to_element_id_to_volume[part_id] = part_volumes
            except:
                error_part_ids.append(part_id)

        if len(error_part_ids) != 0:
            objects = self._dynasaur_definitions.get_defined_objects_containing_parts(error_part_ids)
            self._logger.emit(LOGConstants.READ_VOLUME[0], "Warning: could not assign volume for parts (" +
                              ", ".join(map(str, error_part_ids)) + ") of objects (" + ", ".join(objects) + ")")

        return True

    def read_binout_data(self, elout, strain_stress_type=EloutIndex.STRAIN):
        if self._read_volume_data_from_file():
            return self._init_data(elout.get_data_mapping_part_i_dto_element_ids(strain_stress_type))
        return False

    def get_defined_objects(self):
        part_ids_volume_not_zero = [key for key in self._data_mapping_part_id_to_volume.keys()
                                    if self._data_mapping_part_id_to_volume[key] != 0.0]
        return self._dynasaur_definitions.get_defined_objects_containing_all_parts(part_ids_volume_not_zero)

    def get_part_volume_by_part_ID(self, part_id):
        return self._data_mapping_part_id_to_volume[part_id]

    def get_element_volume_by_part_id_and_element_id(self, part_id, element_id):
        return self._data_mapping_part_id_to_element_id_to_volume[part_id][element_id]
