# ***********************************************************************************************************************
#
# Jakub Micorek
#
# ***********************************************************************************************************************


import matplotlib.pyplot as plt
import numpy as np

from Plugins.Analysis.universal_gui import UniversalGUI
from Plugins.Injury.limits import Limits
from Plugins.Injury.limits import UniversalLimit

from Data.elout import EloutIndex
from constants import LOGConstants, ObjectConstantsForData, PluginsParamDictDef, OutputStringForPlugins
from plugin import Plugin, PluginInterface


class UniversalController(Plugin, PluginInterface):
    def __init__(self, root, logger, risk_functions, units, name, code_type, limits=None):
        Plugin.__init__(self, root, logger, units=units, data_requirements=[ObjectConstantsForData.ELEMENT],
                        risk_functions=risk_functions, name=name, code_type=code_type)

        self._limits_from_user = limits

        self.__root = root
        self.__limits = Limits()

        self.__histogram_data = [0] * 11
        self.__percentile_values = []
        self.__elements_greater_limit = []

        self.__calculated_object_name = ""
        self.__calculated_strain_stress_type = None

        # create list of all GUI labels
        self.__output_names = []
        #self.__output_names.append(OutputStringForPlugins.PERCENTILE_LIMIT)
        self.__output_names.append(OutputStringForPlugins.VALUE)
        self.__output_names.append(OutputStringForPlugins.ELEMENTS)

        for i in range(0, 100, 10):
            self.__output_names.append(" Nr. of Elements (" +
                                       str(i).zfill(2) + "-" + str(i + 10).zfill(2) + "% of limit):")
        self.__output_names.append("Nr. of Elements (  >100% of limit):")

        # create GUI
        if root is not None:
            self.init_plugin_data(update=True)

            self.__gui = UniversalGUI(self, self.__root, self.__output_names,
                                      self._data[ObjectConstantsForData.ELEMENT].get_defined_objects(),
                                      self._data[ObjectConstantsForData.ELEMENT].get_defined_time_steps(True),
                                      self._data[ObjectConstantsForData.ELEMENT].data_contains_type(EloutIndex.STRAIN),
                                      self._data[ObjectConstantsForData.ELEMENT].data_contains_type(EloutIndex.STRESS))
            self.change_limit_value()

        self.__vonMises_factor = None
        self.__script_overall_elements_greater_limit = []

    def __overall_function(self, integration_points, axis=1):
        abs_min = np.abs(np.min(integration_points, axis=axis))
        abs_max = np.abs(np.max(integration_points, axis=axis))
        return np.max([abs_min, abs_max], axis=0)

    def __von_mises_function(self, integration_points, axis=1):
        return np.sqrt(self.__vonMises_factor * ((integration_points[:, 0] - integration_points[:, 1]) ** 2 +
                                                 (integration_points[:, 1] - integration_points[:, 2]) ** 2 +
                                                 (integration_points[:, 2] - integration_points[:, 0]) ** 2))

    def _list_duplicates(self, seq):
        seen = set()
        seen_add = seen.add
        # adds all elements it doesn't know yet to seen and all other to seen_twice
        seen_twice = set(x for x in seq if x in seen or seen_add(x))
        # turn the set into a list (as requested)
        return list(seen_twice)


    def calculate(self, start_time, end_time, integration_point, selection_tension_compression, selection_strain_stress,
                  selected_object, limit, percentile, is_percentile_set):

        # TODO
        # sanity checks here
        #   are all parameters valid ... ?
        #
        start_time = start_time if start_time != "" and start_time != "None" else None
        end_time = end_time if end_time != "" and start_time != "None" else None

        self.__vonMises_factor = None

        # SETUP
        # get all indices of time step
        # if "All" selected, all indices of all time step
        # if not "All" selected, index to the selected one
        time_step_indices = []
        if start_time == None and end_time == None:
            time_step_indices = list(range(0, len(self._data[ObjectConstantsForData.ELEMENT].get_time())))
        elif start_time != None and end_time == None:
            index = self.find_index(start_time, self._data[ObjectConstantsForData.ELEMENT].get_time())
            time_step_indices = list(range(index, len(self._data[ObjectConstantsForData.ELEMENT].get_time())))
        elif start_time == None and end_time != None:
            index = self.find_index(end_time, self._data[ObjectConstantsForData.ELEMENT].get_time())
            time_step_indices = list(range(0, index))
        else:
            if start_time >= end_time:
                # TODO: Discuss maybe brake
                self._logger.emit(LOGConstants.ERROR[0],
                                  "End time is smaller or equal as start time! End time set to last value in time step list.")
                end_time = self._data[ObjectConstantsForData.ELEMENT].get_time()[-1]
            index_offset = self.find_index(start_time, self._data[ObjectConstantsForData.ELEMENT].get_time())
            index_delta_t = self.find_index(end_time, self._data[ObjectConstantsForData.ELEMENT].get_time())
            time_step_indices = list(range(index_offset, index_delta_t))


        self.__elements_greater_limit = {}
        for i in time_step_indices:
            self.__elements_greater_limit[i] = []

        self.__script_overall_elements_greater_limit.append(self.__elements_greater_limit)

        # take max or mean of all integration points at a given time step
        if integration_point == UniversalGUI.INTEGRATION_POINT_OPTIONS[0]:
            integration_point_function = np.max
        elif integration_point == UniversalGUI.INTEGRATION_POINT_OPTIONS[1]:
            integration_point_function = np.mean
        elif integration_point == UniversalGUI.INTEGRATION_POINT_OPTIONS[2]:
            integration_point_function = np.min

        # indicates what should be calculated: strain or stress
        what_strain_stress = EloutIndex.translate_strain_stress(selection_strain_stress)

        # selected function for tension compression and overall calculation
        if selection_tension_compression == UniversalLimit.TENSION_COMPRESSION_VALUES[0]:  # Overall
            function_overall_tension_compression = self.__overall_function
        elif selection_tension_compression == UniversalLimit.TENSION_COMPRESSION_VALUES[1]:  # Tension
            function_overall_tension_compression = np.max
        elif selection_tension_compression == UniversalLimit.TENSION_COMPRESSION_VALUES[2]:  # Compression
            function_overall_tension_compression = np.min
        elif selection_tension_compression == UniversalLimit.TENSION_COMPRESSION_VALUES[3]:  # vonMises
            function_overall_tension_compression = self.__von_mises_function

            if what_strain_stress == EloutIndex.STRESS:
                self.__vonMises_factor = 1. / 2
            elif what_strain_stress == EloutIndex.STRAIN:
                self.__vonMises_factor = 2. / 9
            else:
                self.__vonMises_factor = None

        # indicates if should calculate percentile
        calculate_percentile = is_percentile_set

        # reset data
        element_count = 0
        self.__histogram_data = [0] * 11
        self.__percentile_values = []

        self.__calculated_object_name = selected_object
        self.__calculated_strain_stress_type = what_strain_stress

        limit = float(limit)

        # calculation
        part_ids = self._data[ObjectConstantsForData.ELEMENT].get_part_ids_by_object_name(selected_object, what_strain_stress)
        for part_id in part_ids:

            index, data = self._data[ObjectConstantsForData.ELEMENT].get_part_data(what_strain_stress, part_id, time_step_indices=time_step_indices)
            element_ids = self._data[ObjectConstantsForData.ELEMENT].get_element_ids_of_part_data(index)
            element_count += len(element_ids)

            for element_id in element_ids:
                row_ids = self._data[ObjectConstantsForData.ELEMENT].get_integration_point_indices_of_part_data_by_element_id(index, element_id)

                reduced_integration_point_data = integration_point_function(data[:, row_ids, :], axis=1)
                result_data = function_overall_tension_compression(reduced_integration_point_data, axis=1)
                result_data = result_data[~np.isnan(result_data)]

                # indices which exceed limit
                for time_step_id in np.where(result_data >= limit)[0]:
                    # when "all" selected time_step_id is correct key
                    #
                    # otherwise one time step selected, result time step index key always 0
                    # although dictionary contains different key
                    try:
                        self.__elements_greater_limit[time_step_id].append(element_id)
                    except:
                        continue



                # max value for histogram
                max_value_time_index = np.nanargmax(result_data)
                max_value = result_data[max_value_time_index]

                histogram_index = min(10, int(10 * max_value / limit))
                self.__histogram_data[histogram_index] += 1

                if calculate_percentile:
                    self.__percentile_values.append(max_value)

        percentile_limit_str = "-"
        # percentile calculation
        if calculate_percentile and len(part_ids):
            percentile_values = sorted(self.__percentile_values)
            if selection_tension_compression == UniversalLimit.TENSION_COMPRESSION_VALUES[2]:
                # compression, flip array to descending order
                percentile_values.reverse()

            value_size = len(percentile_values)
            percentile_value = float(percentile)

            percentile_upper_limit = 1 - (1 / element_count)
            if percentile_value <= percentile_upper_limit:
                percentile_limit = percentile_values[int(np.ceil(percentile_value * value_size))]
            else:
                percentile_limit = percentile_values[int(np.ceil(percentile_upper_limit * value_size))]
                self._logger.emit(LOGConstants.WARNING[0],
                                  OutputStringForPlugins.PERCENTILE_LIMIT[:-1] + str(percentile_value) +
                                  " can not be calculated: Percentile Value set to upper limit" + str(
                                      percentile_upper_limit))
                percentile = round(percentile_upper_limit, 5)

            percentile_limit_str = "-" if percentile_limit == 0 else str(percentile_limit)

        return percentile_limit_str, percentile, element_count, self.__histogram_data

    def __getTimePLot(self, axes, elements_exceeding_limit, logo_width):
        times = []
        results = []
        for time_index in elements_exceeding_limit:
            times.append(self._data[ObjectConstantsForData.ELEMENT].get_time()[time_index])
            results.append(len(elements_exceeding_limit[time_index]))

        times = np.array(times) * 1000 / self._units.second()
        times = np.array(times)
        axes.bar(times, results)

        # plot the maximum with a red bar
        t = np.round(times[np.where(results == np.max(results))[0][0]], decimals=0)
        axes.bar(t, np.max(results), color="red")
        ticks = axes.get_xticks()
        if t not in ticks:
            prev = ticks[np.where(ticks > t)[0][0] - 1]
            follow = ticks[np.where(ticks > t)[0][0]]
            if abs(prev - t) <= (follow - prev) * 0.5:
                ticks = np.delete(ticks, np.where(ticks == prev)[0][0])
            if abs(follow - t) <= (follow - prev) * 0.5:
                ticks = np.delete(ticks, np.where(ticks == follow)[0][0])
            ticks = np.insert(ticks, np.where(ticks > t)[0][0], t)

        if t not in ticks:
            ticks = np.insert(ticks, np.where(ticks > t)[0][0], t)

        axes.set_xticks(ticks[np.where(ticks >= 0)])
        axes.set_xlabel("Time [ms]")
        axes.set_ylabel("Number of elements")
        self.brand_plot_TU(axes, logo_width=logo_width)

    def __get_histogram_plot(self, axes, logo_width):
        axes.bar(list(range(0, 101, len(self.__histogram_data) - 1)), self.__histogram_data, width=10, align='edge')
        axes.set_xlabel("%")
        axes.set_ylabel("Number of elements")
        self.brand_plot_TU(axes, logo_width=logo_width)

    def plot_histogram(self, show_plot=False):
        f = plt.figure()
        self.__get_histogram_plot(f.gca(), logo_width=0.15)
        if show_plot:
            plt.show(block=False)

    def plot_time(self, show_plot=False):
        fig = plt.figure()
        self.__getTimePLot(fig.gca(), self.__elements_greater_limit, logo_width=0.15)
        if show_plot:
            plt.show(block=False)

    def export_element_set(self, tmp_file=None):
        time_sorting_key = "###___TIME___ORDER___###"
        out_dict = {}
        for time_index in self.__elements_greater_limit:
            # time step string
            time_step = str(self._data[ObjectConstantsForData.ELEMENT].get_time()[time_index])

            # go through all elements at the given time step
            for element_index in range(0, len(self.__elements_greater_limit[time_index])):
                # get solid, shell, ... in upper case: SOLID, SHELL, ...
                types = self._data[ObjectConstantsForData.ELEMENT].get_element_type_name(self.__elements_greater_limit[time_index][element_index],
                                                                                         self.__calculated_strain_stress_type)
                for type in types:
                    if type not in out_dict:
                        out_dict[type] = {}

                    if time_step not in out_dict[type]:
                        if time_sorting_key not in out_dict[type]:
                            out_dict[type][time_sorting_key] = []
                        out_dict[type][time_step] = []
                        out_dict[type][time_sorting_key].append(time_step)

                    out_dict[type][time_step].append(
                        str(self.__elements_greater_limit[time_index][element_index]).rjust(10))

        tmp_file.write("*KEYWORD\n")
        for type in out_dict:
            for index, time in enumerate(out_dict[type][time_sorting_key]):
                # write header
                tmp_file.write("*SET_" + type + "_TITLE\n")
                tmp_file.write("$#                                                                         TITLE\n")

                # TODO: convert time (float to string without dot!)
                self._logger.emit(LOGConstants.ERROR[0],
                                  "UniversalController.py::exportElementSet(): convert time (float to string without dot!)")

                tmp_file.write("SET_" + type + "_" + str(time) + "\n")  # write time step

                tmp_file.write("$#     SID\n")
                tmp_file.write(str(index + 1).rjust(10) + "\n")
                tmp_file.write("$#    EID1      EID2      EID3      EID4      EID5      EID6      EID7      EID8\n")

                # build element id output string
                tmp_output = out_dict[type][time][0]
                for element_index in range(1, len(out_dict[type][time])):
                    if element_index % 8 == 0:
                        tmp_output += "\n"
                    tmp_output += out_dict[type][time][element_index]

                # write elements
                tmp_file.write(tmp_output + "\n")

        tmp_file.write("*END\n")
        tmp_file.close()

    def change_limit_value(self):
        selected_object_name = self.__gui.selection_object.get()
        value = self.__limits.get_universal_object_limit_by_name(selected_object_name,
                                                                 self.__gui.selection_strain_stress.get(),
                                                                 self.__gui.selection_tension_compression.get())
        self.__gui.limit.set(value)

    ####################################################################################################################
    #
    def get_output_string(self, separator=" ", start_time=None, end_time=None, integration_point=None,
                          selection_tension_compression=None, selection_strain_stress=None,
                          selected_object=None, limit=None, percentile=None, percentile_limit_str=None,
                          histogram_data=None, elements=None, is_percentile_set=False):
        percentile_calculation_set = is_percentile_set

        # setup info
        setup_string = OutputStringForPlugins.SETUP
        setup_string += OutputStringForPlugins.OBJECT + separator + (
            str(self.__gui.selection_object.get()) if selected_object == None else str(selected_object)) + "\n"
        setup_string += OutputStringForPlugins.STRAIN_STRESS + separator + (
            str(self.__gui.selection_strain_stress.get()) if selection_strain_stress == None else str(
                selection_strain_stress)) + "\n"
        setup_string += OutputStringForPlugins.TENSION_COMPRESSION + separator + (
            str(self.__gui.selection_tension_compression.get()) if selection_tension_compression == None else str(
                selection_tension_compression)) + "\n"
        setup_string += OutputStringForPlugins.WORST_LIMIT + separator + (str(self.__gui.limit.get()) if limit == None else str(limit)) + "\n"
        setup_string += OutputStringForPlugins.MIDDLE_LIMIT + separator + (str(float(self.__gui.limit.get() * 0.75)) if limit == None else str(limit * 0.75)) + "\n"
        setup_string += OutputStringForPlugins.BEST_LIMIT + separator + (str(float(self.__gui.limit.get() * 0.5)) if limit == None else str(limit * 0.5)) + "\n"
        setup_string += OutputStringForPlugins.INTERGATION_POINT + separator + (
            str(self.__gui.selection_integration_point.get()) if integration_point == None else str(
                integration_point)) + "\n"
        setup_string += OutputStringForPlugins.START_TIME + separator + (
            str(self.__gui.start_time.get()) if start_time == None else str(start_time)) + "\n"
        setup_string += OutputStringForPlugins.END_TIME + separator + (
            str(self.__gui.end_time.get()) if end_time == None else str(end_time)) + "\n"
        if percentile_calculation_set:
            setup_string += OutputStringForPlugins.PERCENTILE_LIMIT + separator + (
                str(self.__gui.percentile.get()) if percentile == None else str(percentile)) + "\n"

        # result
        output_string = OutputStringForPlugins.RESULTS
        if (histogram_data == None):
            for key in self.__output_names:
                if key == OutputStringForPlugins.PERCENTILE_LIMIT:
                    continue
                if percentile_calculation_set == False and key == OutputStringForPlugins.VALUE:
                    continue

                output_string += key + separator + str(self.__gui.dict_output_values[key].get()) + "\n"

        else:
            for (key, hd) in zip(self.__output_names,
                                 [item for sublist in [[percentile_limit_str], [elements], histogram_data] for item in
                                  sublist]):
                if percentile_calculation_set == False and key == OutputStringForPlugins.VALUE:
                    continue
                output_string += key + separator + str(hd) + "\n"

        return setup_string + output_string

    def check_if_params_are_valid(self, param_dict):
        # TODO
        return True

    def calculate_and_store_results(self, param_dict):
        selected_object = param_dict[PluginsParamDictDef.SELECTED_OBJECT].replace("_", " ")
        selection_strain_stress = param_dict[PluginsParamDictDef.SELECTION_STRAIN_STRESS]
        integration_point = param_dict[PluginsParamDictDef.INTEGRATION_POINT]
        selection_tension_compression = param_dict[PluginsParamDictDef.SELECTION_TENSION_COMPRESSION]
        limit = param_dict[PluginsParamDictDef.LIMIT]
        percentile = param_dict[PluginsParamDictDef.PERCENTILE]
        start_time = param_dict[PluginsParamDictDef.START_TIME] if PluginsParamDictDef.START_TIME in param_dict else None
        end_time = param_dict[PluginsParamDictDef.END_TIME] if PluginsParamDictDef.END_TIME in param_dict else None

        percentile_limit_str, percentile, element_count, histogram_data = self.calculate(start_time=start_time, end_time=end_time,
                                                                             integration_point=integration_point,
                                                                             selection_tension_compression=selection_tension_compression,
                                                                             selection_strain_stress=selection_strain_stress,
                                                                             selected_object=selected_object,
                                                                             limit=limit,
                                                                             percentile=percentile,
                                                                             is_percentile_set=True)

        result_str = self.get_output_string(separator=";", start_time=start_time, end_time=end_time,
                                            integration_point=integration_point,
                                            selection_tension_compression=selection_tension_compression,
                                            selection_strain_stress=selection_strain_stress,
                                            selected_object=selected_object, limit=limit, percentile=percentile,
                                            percentile_limit_str=percentile_limit_str, histogram_data=histogram_data,
                                            elements=element_count, is_percentile_set=True)

        self.extract_script_calc_array_and_title(result_str)

        self._overall_plot_data.append(histogram_data)

    def plot_calculations_to_pdf(self, pp):
        for __histogram_data, limit, title in zip(self._overall_plot_data, self.__script_overall_elements_greater_limit,
                                                  self._titles):
            f, axarr = plt.subplots(2, 1, figsize=(10, 10))

            # Plots Histogram for the elements
            f.suptitle(title, fontsize=10)
            self.__get_histogram_plot(axarr[0].axes, logo_width=0.1)

            # Plots the Histogram over time
            self.__getTimePLot(axarr[1].axes, limit, logo_width=0.1)

            pp.savefig(f)  # saves the current figure into a pdf page

    def find_index(self, value, time_history_list):
        if value != None:
            array = np.asarray(time_history_list)
            index = (np.abs(array - (float(value) * self._units.second()))).argmin()
        return index
