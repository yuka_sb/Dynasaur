# ***********************************************************************************************************************
#
# Jakub Micorek
#
# ***********************************************************************************************************************

import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
from tkinter import ttk

from Plugins.Injury.limits import UniversalLimit


class UniversalGUI:
    INTEGRATION_POINT_OPTIONS = ["Max", "Mean", "Min"]

    def __init__(self, controller, root, output_names, object_names, time_step_names, contains_strain_data,
                 contains_stress_data):
        self._controller = controller
        self.__window = tk.Toplevel(root)

        # limit
        self.limit = tk.StringVar()

        # strain stress
        self.selection_strain_stress = tk.StringVar()
        self.__list_strain_stress = []
        if contains_strain_data:
            self.__list_strain_stress.append(UniversalLimit.STRAIN_STRESS_VALUES[0])
        if contains_stress_data:
            self.__list_strain_stress.append(UniversalLimit.STRAIN_STRESS_VALUES[1])
        self.selection_strain_stress.set(self.__list_strain_stress[0])

        # tension compression
        self.__list_tension_compression = UniversalLimit.TENSION_COMPRESSION_VALUES
        self.selection_tension_compression = tk.StringVar()
        self.selection_tension_compression.set(self.__list_tension_compression[0])

        # objects
        self.__list_objects = object_names
        self.selection_object = tk.StringVar()
        self.selection_object.set(self.__list_objects[0])

        # time steps
        self.__list_time_step = time_step_names
        self.start_time = tk.StringVar()
        self.end_time = tk.StringVar()
        self.start_time.set("")
        self.end_time.set("")
        #self.selection_time_step.set(self.__list_time_step[0])

        # integration points
        self.__list_integration_point = UniversalGUI.INTEGRATION_POINT_OPTIONS
        self.selection_integration_point = tk.StringVar()
        self.selection_integration_point.set(self.__list_integration_point[0])

        # percentile
        self.percentile = tk.StringVar()
        self.percentile_is_calculate = tk.IntVar()

        self.__list_output_names = output_names
        self.dict_output_values = {}

        for key in self.__list_output_names:
            self.dict_output_values[key] = tk.StringVar()

        self.__width_definition = [[20, 33], [30, 23]]
        self.__pad_x = [(5, 10), (0, 5)]
        self.__pad_y = [(3, 3)]

        self.__guiResetResults()
        self.__buildGUI()

        self.limit.trace("w", self.__toggleGUIButtons)
        self.limit.set("")
        self.percentile.set("0.95")

    def __guiResetResults(self):
        for key in self.__list_output_names:
            self.dict_output_values[key].set("-")

    def __buildGUI(self):

        self.__window.resizable(width=False, height=False)
        self.__window.title("Universal")

        menu = tk.Menu(self.__window)
        self.__window.config(menu=menu)

        # File menu
        file_menu = tk.Menu(menu)
        file_menu.add_command(label="Copy Text", accelerator="Ctrl+C", command=self.__guiCopyText)
        self.__window.bind("<Control-c>", self.__guiCopyText)

        file_menu.add_command(label="Save Text", accelerator="Ctrl+S", command=self.__guiSaveText)
        self.__window.bind("<Control-s>", self.__guiSaveText)
        menu.add_cascade(label="File", menu=file_menu)

        #### define global frames and buttons
        # for margin of all inner frames
        frame_margin = ttk.Frame(self.__window, width=sum(self.__width_definition[0]))
        frame_margin.grid(row=0, column=0, padx=(10, 10), pady=(10, 10))

        # define setup frame
        row = 0
        frame_setup = ttk.Labelframe(frame_margin, text="Setup", width=sum(self.__width_definition[0]))
        frame_setup.grid(row=row, column=0)

        # calculate button
        row += 1
        self.__calc_btn = ttk.Button(frame_margin, text="Calculate", width=(sum(self.__width_definition[0]) + 4),
                                     command=self.__guiCalculate)
        self.__calc_btn.grid(row=row, column=0, pady=(3, 20))
        self.__calc_btn.bind("<Button>", self.__togglePLTButtons)

        # define result frame
        row += 1
        frame_results = ttk.Labelframe(frame_margin, text="Results", width=sum(self.__width_definition[0]))
        frame_results.grid(row=row, column=0)

        # plot button
        row += 1
        self.__plt_hist_btn = ttk.Button(frame_margin, text="Plot Histogram",
                                         width=(sum(self.__width_definition[0]) + 4),
                                         command=self.__guiPlotHistogram, state="disabled")
        self.__plt_hist_btn.grid(row=row, column=0, pady=(3, 0))

        # plot button
        row += 1
        self.__plt_time_btn = ttk.Button(frame_margin, text="Plot Time", width=(sum(self.__width_definition[0]) + 4),
                                         command=self.__guiPlotTime, state="disabled")
        self.__plt_time_btn.grid(row=row, column=0, pady=(3, 0))

        # plot button
        row += 1
        self.__plt_exprt_btn = ttk.Button(frame_margin, text="Export Element Set",
                                          width=(sum(self.__width_definition[0]) + 4),
                                          command=self.__guiExportElementSet, state="disabled")
        self.__plt_exprt_btn.grid(row=row, column=0, pady=(3, 0))

        #### setup section

        width = self.__width_definition[0]
        ##############################################
        row_setup = 0
        ttk.Label(frame_setup, text="Object:", anchor="e", width=width[0]).grid(row=row_setup, column=0,
                                                                                padx=self.__pad_x[0],
                                                                                pady=self.__pad_y[0])
        combo_box_object = ttk.Combobox(frame_setup, state="readonly",
                                        textvariable=self.selection_object,
                                        values=self.__list_objects, width=(width[1] - 3))
        combo_box_object.bind("<<ComboboxSelected>>", self.__guiChangeLimit)
        combo_box_object.grid(row=row_setup, column=1, padx=self.__pad_x[1])

        ##############################################
        row_setup += 1
        ttk.Label(frame_setup, text="Strain/Stress:", anchor="e", width=width[0]).grid(row=row_setup, column=0,
                                                                                       padx=self.__pad_x[0],
                                                                                       pady=self.__pad_y[0])
        combo_box_strain_stress = ttk.Combobox(frame_setup, state="readonly",
                                               textvariable=self.selection_strain_stress,
                                               values=self.__list_strain_stress, width=(width[1] - 3))
        combo_box_strain_stress.bind("<<ComboboxSelected>>", self.__guiChangeLimit)
        combo_box_strain_stress.grid(row=row_setup, column=1, padx=self.__pad_x[1])

        ##############################################
        row_setup += 1
        ttk.Label(frame_setup, text="Tension/Compression:", anchor="e", width=width[0]).grid(row=row_setup, column=0,
                                                                                             padx=self.__pad_x[0],
                                                                                             pady=self.__pad_y[0])
        combo_box_tension_compression = ttk.Combobox(frame_setup, state="readonly",
                                                     textvariable=self.selection_tension_compression,
                                                     values=self.__list_tension_compression, width=(width[1] - 3))
        combo_box_tension_compression.bind("<<ComboboxSelected>>", self.__guiChangeLimit)
        combo_box_tension_compression.grid(row=row_setup, column=1, padx=self.__pad_x[1])

        ##############################################
        row_setup += 1
        ttk.Label(frame_setup, text="Limit:", anchor="e", width=width[0]).grid(row=row_setup, column=0,
                                                                               padx=self.__pad_x[0],
                                                                               pady=self.__pad_y[0])
        ttk.Entry(frame_setup, textvar=self.limit, justify='right', width=width[1]).grid(row=row_setup, column=1,
                                                                                         padx=self.__pad_x[1])

        ##############################################
        row_setup += 1
        ttk.Label(frame_setup, text="Integration Point:", anchor="e", width=width[0]).grid(row=row_setup, column=0,
                                                                                           padx=self.__pad_x[0],
                                                                                           pady=self.__pad_y[0])
        combo_box_integration_point = ttk.Combobox(frame_setup, state="readonly",
                                                   textvariable=self.selection_integration_point,
                                                   values=self.__list_integration_point, width=(width[1] - 3))
        combo_box_integration_point.grid(row=row_setup, column=1, padx=self.__pad_x[1])

        ##############################################
        row_setup += 1
        ttk.Label(frame_setup, text="Start time [s]:", anchor="e", width=width[0]).grid(row=row_setup, column=0,
                                                                                   padx=self.__pad_x[0],
                                                                                   pady=self.__pad_y[0])

        ttk.Entry(frame_setup, textvar=self.start_time, justify='right', width=width[1]).grid(row=row_setup, column=1,
                                                                                         padx=self.__pad_x[1])

        row_setup += 1
        ttk.Label(frame_setup, text="End time [s]:", anchor="e", width=width[0]).grid(row=row_setup, column=0,
                                                                                        padx=self.__pad_x[0],
                                                                                        pady=self.__pad_y[0])

        ttk.Entry(frame_setup, textvar=self.end_time, justify='right', width=width[1]).grid(row=row_setup, column=1,
                                                                                              padx=self.__pad_x[1])

        ##############################################

        row_setup += 1
        ttk.Label(frame_setup, text="Percentile:", anchor="e", width=width[0]).grid(row=row_setup, column=0,
                                                                                    padx=self.__pad_x[0],
                                                                                    pady=self.__pad_y[0])
        percentile_frame = tk.Frame(
            frame_setup)  # , width=width[1])#, highlightbackground="#BBBBBB", highlightcolor="#BBBBBB", highlightthickness=2)
        percentile_frame.grid(row=row_setup, column=1)
        ttk.Checkbutton(percentile_frame, text="Calculate Percentile", variable=self.percentile_is_calculate).grid(
            row=0, column=0, padx=(0, 30))
        ttk.Entry(percentile_frame, textvar=self.percentile, justify='right', width=6).grid(row=0, column=1)
        self.percentile_is_calculate.trace("w", self.__toggleGUIButtons)
        self.percentile.trace("w", self.__toggleGUIButtons)

        #### result section
        width = self.__width_definition[1]
        row_results = 0
        for key in self.__list_output_names:
            ttk.Label(frame_results, text=key, anchor="e", width=width[0]).grid(row=row_results, column=0,
                                                                                pady=self.__pad_y[0],
                                                                                padx=self.__pad_x[0])
            ttk.Label(frame_results, anchor="w", width=width[1], textvar=self.dict_output_values[key]).grid(
                row=row_results, column=1, pady=self.__pad_y[0])
            row_results += 1

    def updatePercentile(self, value):
        self.percentile.set(str(value))

    def __guiCalculate(self):
        self.__guiResetResults()
        try:
            limit = float(self.limit.get())
            what = self.selection_tension_compression.get()

            # Overall
            if what == UniversalLimit.TENSION_COMPRESSION_VALUES[0] and limit < 0.0:
                messagebox.showerror("Error", "Limit for " + what.lower() + " calculation must be positive!")
                return
            # Tension
            elif what == UniversalLimit.TENSION_COMPRESSION_VALUES[1] and limit < 0.0:
                messagebox.showerror("Error", "Limit for " + what.lower() + " calculation must be positive!")
                return
            # Compression
            elif what == UniversalLimit.TENSION_COMPRESSION_VALUES[2] and limit >= 0.0:
                messagebox.showerror("Error", "Limit for " + what.lower() + " calculation must be negative!")
                return
        except Exception:
            messagebox.showerror("Error", "Limit is not a number!")
            return

        try:
            percentile = float(self.percentile.get())
        except Exception:
            messagebox.showerror("Error", "Percentile is not a number!")
            return
        percentile_limit_str, percentile, element_count, histogram_data = self._controller.calculate(
            self.start_time.get(), self.end_time.get(), self.selection_integration_point.get(),
            self.selection_tension_compression.get(), self.selection_strain_stress.get(),
            self.selection_object.get(), self.limit.get(), self.percentile.get(), self.isCalculatePercentileSet())

        # set results in gui
        self.percentile.set(percentile)
        self.dict_output_values[self.__list_output_names[0]].set(percentile_limit_str)
        self.dict_output_values[self.__list_output_names[1]].set(str(element_count))
        names_without_elements = self.__list_output_names[2:]
        for index in range(len(names_without_elements)):
            self.dict_output_values[names_without_elements[index]].set(str(histogram_data[index]))

    def __toggleGUIButtons(self, *args):
        # enable if the the limit is valid
        # extra condition: if the percentile should be calculated the percentile value has to be valid
        A = self.isCalculatePercentileSet()
        B = self.__isfloat(self.percentile.get()) and 1 > float(self.percentile.get()) > 0
        C = self.__isfloat(self.limit.get()) and float(self.limit.get()) != 0

        # calculated with truth table
        enable = (not A and C) or (B and C)
        state = "normal" if enable else "disable"
        self.__calc_btn.config(state=state)

    def __togglePLTButtons(self, *args):
        state = "normal"
        self.__plt_exprt_btn.config(state=state)
        self.__plt_hist_btn.config(state=state)
        self.__plt_time_btn.config(state=state)

    def __guiPlotHistogram(self):
        self._controller.plot_histogram(show_plot=True)

    def __guiPlotTime(self):
        self._controller.plot_time(show_plot=True)

    def __guiChangeLimit(self, event=None):
        self._controller.change_limit_value()

    def __guiExportElementSet(self):
        file = filedialog.asksaveasfile(mode='w', defaultextension=".key",
                                        filetypes=[("KEY file", "*.key")])
        if file is None:
            print("no file selected")
            return

        print("Exporting ...")
        self._controller.export_element_set(file)
        print("Exporting done!")

    def isCalculatePercentileSet(self):
        calculate_percentile = False
        if self.percentile_is_calculate.get() != 0:
            calculate_percentile = True
        return calculate_percentile

    def __isfloat(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    def __guiSaveText(self, event=None):
        file = filedialog.asksaveasfile(mode='w', defaultextension=".csv",
                                        filetypes=(("CSV file", "*.csv"), ("Text file", "*.txt")))
        if file is None:
            print("no file selected")
            return

        self._controller.save_text(file)
        file.close()

    def __guiCopyText(self, event=None):
        self._controller.copy_text()
