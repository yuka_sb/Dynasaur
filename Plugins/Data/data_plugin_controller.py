from Plugins.Data.data_plugin_gui import DataPluginGUI
from Data.elout import EloutIndex
from plugin import PluginInterface, Plugin
import numpy as np
import copy
from constants import LOGConstants, ObjectConstantsForData, PluginsParamDictDef, DataPluginConstants


class DataPluginController(PluginInterface, Plugin):

    def __init__(self, code_type, root=None, logger=None, risk_functions=None, data_requirements=None, units=None, name=None):
        Plugin.__init__(self, root, logger, data_requirements=data_requirements, risk_functions=risk_functions, units=units, name=name, code_type=code_type)

        self._root = root
        if root is not None:
            self.init_plugin_data(update=True)
            self._gui = DataPluginGUI(self, self._root, self._data[ObjectConstantsForData.ELEMENT].get_defined_objects(),
                                      [ObjectConstantsForData.STRAIN, ObjectConstantsForData.STRESS])

    def get_part_ids_by_object_name(self, object_name, strain_stress_type):
        return sorted(
            self._data[ObjectConstantsForData.ELEMENT].get_part_ids_by_object_name(object_name, EloutIndex.translate_strain_stress(strain_stress_type)))

    def get_element_ids_by_part_id(self, part_id, strain_stress_type):
        return sorted(
            self._data[ObjectConstantsForData.ELEMENT].get_element_ids_by_part_id(int(part_id), EloutIndex.translate_strain_stress(strain_stress_type)))

    def get_plot_data(self, strain_stress_type, part_id, element_id):
        index, data = self._data[ObjectConstantsForData.ELEMENT].get_part_data(EloutIndex.translate_strain_stress(strain_stress_type), int(part_id),
                                                                               int(element_id))
        return index, data, self._data[ObjectConstantsForData.ELEMENT].get_time()

    def get_output_string(self, separator=" ", x_data_name=None, y_data_name=None,
                          x_data=[], y_data=[]):

        # TODO
        # implement GUI support
        header = x_data_name + separator + y_data_name + "\n"

        data_string = ''
        for (x, y) in zip(x_data, y_data):
            data_string += str(x) + separator + str(y) + '\n'

        return header + data_string

    def check_if_params_are_valid(self, param_dict):
        # TODO
        return True

    def calculate_and_store_results(self, param_dict):

        json_object = param_dict[PluginsParamDictDef.DYNASAUR_JSON]
        x_name = param_dict[PluginsParamDictDef.X_NAME]
        y_name = param_dict[PluginsParamDictDef.Y_NAME]
        start_time = param_dict[PluginsParamDictDef.START_TIME] if PluginsParamDictDef.START_TIME in param_dict\
                                                                   and param_dict[PluginsParamDictDef.START_TIME] != "None" else None
        end_time = param_dict[PluginsParamDictDef.END_TIME] if PluginsParamDictDef.END_TIME in param_dict \
                                                               and param_dict[PluginsParamDictDef.END_TIME] != "None" else None

        sample_offsets = []
        for key in self._data.keys():
            if start_time != None and end_time != None:
                if start_time >= end_time:
                    self._logger.emit(LOGConstants.ERROR[0], "End time is smaller or equal as start time! End time set to last value in time step list.")
                    end_time = self._data[key].get_time()[-1]
                indices = np.argwhere(
                    (start_time * self._units.second() < self._data[key].get_time()) & (self._data[key].get_time() < end_time * self._units.second()))
            elif start_time != None and end_time == None:
                indices = np.argwhere(start_time * self._units.second() < self._data[key].get_time())
            elif start_time == None and end_time != None:
                indices = np.argwhere(self._data[key].get_time() < end_time * self._units.second())
            else:
                indices = np.argwhere(self._data[key].get_time())
            sample_offsets.append((key, indices[0][0], indices[-1][-1]))

        reduced_sample_offsets_x = self._reduce_sample_offset(json_object[DataPluginConstants.X], sample_offsets)
        reduced_sample_offsets_y = self._reduce_sample_offset(json_object[DataPluginConstants.Y], sample_offsets)

        start_time = start_time if start_time != None else 0
        x_data = copy.copy(self._get_data_from_dynasaur_JSON(json_object=json_object[DataPluginConstants.X], data_offsets=reduced_sample_offsets_x))
        if x_name.startswith(DataPluginConstants.TIME):
            x_data -= start_time * self._units.second()
        y_data = copy.copy(self._get_data_from_dynasaur_JSON(json_object=json_object[DataPluginConstants.Y], data_offsets=reduced_sample_offsets_y))
        if y_name.startswith(DataPluginConstants.TIME):
            y_data -= start_time * self._units.second()

        # TODO
        x_data_name = param_dict[DataPluginConstants.DIAGRAM_NAME].split("_")[0] + ":" + "_".join(param_dict[DataPluginConstants.DIAGRAM_NAME].split("_")[1:]) + ":" + x_name
        y_data_name = param_dict[DataPluginConstants.DIAGRAM_NAME].split("_")[0] + ":" + "_".join(param_dict[DataPluginConstants.DIAGRAM_NAME].split("_")[1:]) + ":" + y_name
        result_str = self.get_output_string(separator=";", x_data_name=x_data_name,
                                            y_data_name=y_data_name,
                                            x_data=x_data.flatten(), y_data=y_data.flatten())

        self.extract_script_calc_array_and_title(result_str, merge=False)

    def plot_calculations_to_pdf(self):
        return