import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plt
import numpy as np


class DataPluginGUI:
    def __init__(self, controller, root, object_names, type_names):
        self._controller = controller
        self._window = tk.Toplevel(root)

        self._type_names = type_names
        self._object_names = object_names

        self._last_selection_setting = (None, None, None, None)

        self._listbox_objects = None
        self._listbox_types = None
        self._listbox_parts = None
        self._listbox_elements = None

        self.__width_definition = [[20, 10, 10, 10]]
        self.__pad_x = [(5, 5)]
        self.__pad_y = [(3, 3)]

        self.__buildGUI()

    def __buildGUI(self):
        self._window.title("Data")
        self._window.resizable(width=False, height=False)

        #################################################
        #################################################
        #################################################
        # for margin of all inner frames
        frame_margin = ttk.Frame(self._window, width=sum(self.__width_definition[0]))
        frame_margin.grid(row=0, column=0, padx=(10, 10), pady=(10, 0))

        #################################################
        # define object frame
        row = 0
        column = 0
        frame_object = ttk.LabelFrame(frame_margin, text="Objects", width=self.__width_definition[0][column])
        frame_object.grid(row=row, column=column, padx=self.__pad_x[0], pady=self.__pad_y[0])

        #################################################
        # define type frame
        column += 1
        frame_type = ttk.LabelFrame(frame_margin, text="Type", width=self.__width_definition[0][column])
        frame_type.grid(row=row, column=column, padx=self.__pad_x[0], pady=self.__pad_y[0])

        #################################################
        # define part frame
        column += 1
        frame_part = ttk.LabelFrame(frame_margin, text="Parts", width=self.__width_definition[0][column])
        frame_part.grid(row=row, column=column, padx=self.__pad_x[0], pady=self.__pad_y[0])

        #################################################
        # define element frame
        column += 1
        frame_element = ttk.LabelFrame(frame_margin, text="Elements", width=self.__width_definition[0][column])
        frame_element.grid(row=row, column=column, padx=self.__pad_x[0], pady=self.__pad_y[0])

        #################################################
        # define data/result frame
        column = 0
        row += 1
        frame_margin_button = ttk.Frame(self._window, width=sum(self.__width_definition[0]))
        frame_margin_button.grid(row=row, column=column, padx=(10, 10), pady=(0, 10))
        frame_data_result = ttk.Frame(frame_margin_button, width=self.__width_definition[0][column])
        frame_data_result.grid(row=0, column=0, padx=self.__pad_x[0], pady=self.__pad_y[0])

        #################################################
        #################################################
        #################################################
        # create list boxes
        column = 0
        self._listbox_objects = self._init_list_box(frame_object, self._gui_select_object,
                                                    self.__width_definition[0][column],
                                                    names=self._object_names)

        column += 1
        self._listbox_types = self._init_list_box(frame_type, self._gui_select_type, self.__width_definition[0][column],
                                                  names=self._type_names, add_scrollbar=False)

        column += 1
        self._listbox_parts = self._init_list_box(frame_part, self._gui_select_part, self.__width_definition[0][column])

        column += 1
        self._listbox_elements = self._init_list_box(frame_element, self._gui_select_element,
                                                     self.__width_definition[0][column])

        tk.Button(frame_data_result, text="Plot", width=sum(self.__width_definition[0]),
                  command=self.plot_lambdas).grid(row=0, column=0)

    def _init_list_box(self, frame, method, width, names=None, add_scrollbar=True):
        if add_scrollbar == True:
            scrollbar = tk.Scrollbar(frame)
            scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        listbox = tk.Listbox(frame, selectmode=tk.SINGLE, width=width, exportselection=False)
        if names is not None:
            listbox.insert(tk.END, *names)
        listbox.bind('<<ListboxSelect>>', method)
        listbox.pack()

        if add_scrollbar == True:
            listbox.config(yscrollcommand=scrollbar.set)
            scrollbar.config(command=listbox.yview)

        return listbox

    def _get_listbox_selection(self, listbox):
        value = None
        selection = listbox.curselection()
        if len(selection) != 0:
            value = listbox.get(selection)
        return value

    def _gui_select_object(self, event):
        self._update_last_selection_setting()
        self._gui_show_parts()

    def _gui_select_type(self, event):
        self._update_last_selection_setting()
        self._gui_show_parts()

    def _gui_show_parts(self):
        self._listbox_parts.delete(0, tk.END)
        self._listbox_elements.delete(0, tk.END)

        object_name = self._get_listbox_selection(self._listbox_objects)
        type_name = self._get_listbox_selection(self._listbox_types)
        if object_name is not None and type_name is not None:
            part_ids = self._controller.get_part_ids_by_object_name(object_name, type_name)
            part_id_names = [str(part) for part in part_ids]
            # part_id_names.insert(0, "All")
            self._listbox_parts.delete(0, tk.END)
            self._listbox_parts.insert(tk.END, *part_id_names)

        try:
            self._listbox_parts.selection_set(self._listbox_parts.get(0, tk.END).index(self._last_selection_setting[2]))
            self._gui_show_elements()
        except ValueError:
            pass

    def _gui_select_part(self, event):
        self._update_last_selection_setting()
        self._gui_show_elements()

    def _gui_show_elements(self):
        part_id = self._get_listbox_selection(self._listbox_parts)
        type_name = self._get_listbox_selection(self._listbox_types)
        if part_id is not None and type_name is not None:
            element_ids = self._controller.get_element_ids_by_part_id(part_id, type_name)
            element_id_names = [str(part) for part in element_ids]
            # element_id_names.insert(0, "All")
            self._listbox_elements.delete(0, tk.END)
            self._listbox_elements.insert(tk.END, *element_id_names)

            try:
                self._listbox_elements.selection_set(
                    self._listbox_elements.get(0, tk.END).index(self._last_selection_setting[3]))
            except ValueError:
                pass

    def _gui_select_element(self, event):
        self._update_last_selection_setting()
        value = self._get_listbox_selection(self._listbox_elements)
        # if value is not None:
        #     print("_guiSelectElement: " + value)

    def _update_last_selection_setting(self):
        self._last_selection_setting = (self._get_listbox_selection(self._listbox_objects),
                                        self._get_listbox_selection(self._listbox_types),
                                        self._get_listbox_selection(self._listbox_parts),
                                        self._get_listbox_selection(self._listbox_elements))
        # print(self._last_selection_setting)

    def abs_max(self, data, axis=1):
        return np.max(np.abs(data), axis=axis)

    def abs_min(self, data, axis=1):
        return -np.min(np.abs(data), axis=axis)

    def abs_mean(self, data, axis=1):
        return np.mean(np.abs(data), axis=axis)

    def plot_lambdas(self):
        stress_strain_type = self._get_listbox_selection(self._listbox_types)
        part_id = self._get_listbox_selection(self._listbox_parts)
        element_id = self._get_listbox_selection(self._listbox_elements)

        lambda_str = r'$\lambda$'
        lambdas = [lambda_str + str(x + 1) for x in range(3)]

        if stress_strain_type is not None and part_id is not None and element_id is not None:
            index, data, time = self._controller.get_plot_data(stress_strain_type, part_id, element_id)
            time_axis = time[-1] - time[0]
            percent = 0.005

            plt.figure("Integration Points")
            number_integration_points = len(index[:, 2])
            number_of_cols = 3
            plt_rows = int(number_integration_points / number_of_cols)
            if plt_rows == 0:
                plt_rows = 1
                plt_cols = number_integration_points
            else:
                plt_cols = number_of_cols
                if number_integration_points % number_of_cols != 0:
                    plt_rows += 1

            for ip_index, integration_point in zip(range(number_integration_points), index[:, 2]):
                plt.subplot(plt_rows, plt_cols, ip_index + 1)
                plt.title("IP " + str(integration_point))
                lines = plt.plot(time, data[:, ip_index, :])
                plt.xlim(time[0] - time_axis * percent, time[-1] + time_axis * percent)

            plt.figlegend(lines, lambdas, loc='lower center', ncol=len(lambdas))

            defs = [("Max", np.max, 2.5),
                    ("Mean", np.mean, 2.5),
                    ("Min", np.min, 2.5),
                    ("AbsMax", self.abs_max, 1.0),
                    ("AbsMean", self.abs_mean, 1.0),
                    ("-AbsMin", self.abs_min, 1.0)]

            plt.figure("Lambda [" + ", ".join([name[0] for name in defs]) + "] over Integration Points")

            for ip_index, integration_point in zip(range(number_integration_points), index[:, 2]):
                lines = []
                plt.subplot(plt_rows, plt_cols, ip_index + 1)
                plt.title("IP " + str(integration_point))
                for idx_function, tmp_function, width in zip(range(len(defs)), [func[1] for func in defs],
                                                             [width[2] for width in defs]):
                    line = plt.plot(time, tmp_function(data[:, ip_index, :], axis=1), linewidth=width)
                    lines.append(line[0])
                plt.xlim(time[0] - time_axis * percent, time[-1] + time_axis * percent)

            plt.xlim(time[0] - time_axis * percent, time[-1] + time_axis * percent)
            plt.figlegend(lines, [name[0] for name in defs], loc='lower center', ncol=len(defs))
            plt.show()

        else:
            print("DATA PLUGIN: select part id and element id for plotting!")
