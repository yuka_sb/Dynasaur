# -----------------------------------------------------------------------
#
# Author: Martin Schachner
# Version: 12.July.2017
#
# -----------------------------------------------------------------------

import numpy as np
import ast

from cfc import CFC

from scipy.integrate import cumtrapz

from Plugins.Injury.criteria_gui import CriteriaGUI
from plugin import Plugin, PluginInterface
from constants import LOGConstants, JsonConstants, PluginsParamDictDef, OutputStringForPlugins


class CriteriaController(Plugin, PluginInterface):
    def __init__(self, code_type, root=None, logger=None, risk_functions=None, units=None, data_requirements=None, name=None):

        Plugin.__init__(self, root, logger, units=units, data_requirements=data_requirements,
                        risk_functions=risk_functions, name=name, code_type=code_type)
        self.__root = root

        if root is not None:
            self.__gui = CriteriaGUI(self, root)

    def get_output_string(self, separator=" ", criteria=None, label_list_calc=None):
        setup_string = OutputStringForPlugins.CRITERIA + separator + \
                       (str(self.__gui.limit.get()) if criteria is None else str(criteria)) + "\n"

        output_str = ""
        for row_list in label_list_calc:
            for index, entry in enumerate(row_list):
                if index != 0:
                    output_str += separator + str(entry[OutputStringForPlugins.TEXT])
                else:
                    output_str += str(entry[OutputStringForPlugins.TEXT])
            output_str += "\n"

        return setup_string + output_str

    def check_if_params_are_valid(self, param_dict):
        # TODO
        return True

    def calculate_and_store_results(self, param_dict):

        json_object = param_dict[PluginsParamDictDef.DYNASAUR_JSON]
        start_time = param_dict[PluginsParamDictDef.START_TIME] if PluginsParamDictDef.START_TIME in param_dict \
                                                                and param_dict[PluginsParamDictDef.START_TIME] != "None" \
                                                                else None
        criteria = param_dict[PluginsParamDictDef.CRITERIA]
        func = param_dict[PluginsParamDictDef.RISK_CURVE]
        end_time = param_dict[PluginsParamDictDef.END_TIME] if PluginsParamDictDef.END_TIME in param_dict \
                                                               and param_dict[PluginsParamDictDef.END_TIME] != "None" \
                                                                else None


        sample_offsets = []
        for key in self._data.keys():
            if start_time != None and end_time != None:
                if start_time >= end_time:
                    #TODO: Discuss maybe brake
                    self._logger.emit(LOGConstants.ERROR[0], "End time is smaller or equal as start time! End time set to last value in time step list.")
                    end_time = self._data[key].get_time()[-1]
                indices = np.argwhere(
                    (start_time * self._units.second() < self._data[key].get_time()) &
                    (self._data[key].get_time() < end_time * self._units.second()))
            elif start_time != None and end_time == None:
                indices = np.argwhere(start_time * self._units.second() < self._data[key].get_time())
            elif start_time == None and end_time != None:
                indices = np.argwhere(self._data[key].get_time() < end_time * self._units.second())
            else:
                indices = np.argwhere(self._data[key].get_time())
            sample_offsets.append((key, indices[0][0], indices[-1][-1]))

        reduced_sample_offsets = self._reduce_sample_offset(json_object, sample_offsets)

        # Problem might occur, because the time stamp for elout data is not interpolated ... other time values are
        # sampled and interpolated properly!
        #assert all([off == sample_offsets[0][1] for i,off in sample_offsets])
        
        ret = self._get_data_from_dynasaur_JSON(json_object, reduced_sample_offsets)

        ret_tuple = ret if isinstance(ret, tuple) else (ret, '-')

        aisrisk = self.calculate_risk(ret_tuple[0], curve=func) if func != 'None' else '-'
        ret_tuple = ret_tuple + (aisrisk,)

        label_list_calc = [[{OutputStringForPlugins.TEXT: "Value"}, {OutputStringForPlugins.TEXT: '-'}],
                           [{OutputStringForPlugins.TEXT: "Time"}, {OutputStringForPlugins.TEXT: '-'}],
                           [{OutputStringForPlugins.TEXT: "Risk"}, {OutputStringForPlugins.TEXT: '-'}]]
        for (r, label) in zip(ret_tuple, label_list_calc):
            label[1][OutputStringForPlugins.TEXT] = r

        limit_string = ""
        if JsonConstants.LIMITS in json_object and len(json_object[JsonConstants.LIMITS]) == 3:
            limit_string = ":" + str(json_object[JsonConstants.LIMITS])

        criteria = json_object[JsonConstants.PART_OF] + ":" + json_object[JsonConstants.TYPE_OF_CRTITERIA] + ":" + \
                      "_".join(criteria.split("_")[1:]) + limit_string

        result_str = self.get_output_string(separator=";", criteria=criteria,
                                            label_list_calc=label_list_calc)

        self.extract_script_calc_array_and_title(result_str)

    def plot_calculations_to_pdf(self, pp):
        # TODO
        pass

