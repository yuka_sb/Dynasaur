# ----------------------------------------------------------------------------------------------------------------------
#
# Author: Martin Schachner
#
# ----------------------------------------------------------------------------------------------------------------------

import tkinter as tk
from tkinter import ttk
from tkinter import filedialog

from plugin import GUI, GUIInterface


class CriteriaGUI(GUI, GUIInterface):
    def __init__(self, controller, root):
        GUI.__init__(self, root, controller, width_definition=[[9, 31], [17, 23], [15, 25]], pad_x=[(5, 10), (0, 5)],
                     pad_y=[(3, 3), (10, 10)])

        # GUI variables
        self.__window = tk.Toplevel(root)

        self._build_GUI()
        self._gui_update()

    def _build_GUI(self):
        self.__window.resizable(width=False, height=False)
        self.__window.title("Criterium")

        menu = tk.Menu(self.__window)
        self.__window.config(menu=menu)

        # File menu
        file_menu = tk.Menu(menu)
        file_menu.add_command(label="Copy Text", accelerator="Ctrl+C", command=self._gui_copy_text)
        self.__window.bind("<Control-c>", self._gui_copy_text)

        file_menu.add_command(label="Save Text", accelerator="Ctrl+S", command=self._gui_save_text)
        self.__window.bind("<Control-s>", self._gui_save_text)

        menu.add_cascade(label="File", menu=file_menu)

    def _gui_plot(self, event=None):
        pass
        # TODO ... not implemented
        function_name = self.selection_function.get()
        self._controller.plot_function(function_name=function_name, show_plot=True)

    def _gui_update(self, event=None):
        pass

