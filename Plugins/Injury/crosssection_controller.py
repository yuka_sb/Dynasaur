# ***********************************************************************************************************************
#
# Jakub Micorek
#
# ***********************************************************************************************************************

import matplotlib.pyplot as plt
import numpy as np

from Plugins.Injury.crosssection_gui import CrosssectionGUI
from plugin import Plugin, PluginInterface
from constants import LOGConstants, ObjectConstantsForData, PluginsParamDictDef, OutputStringForPlugins


class CrosssectionController(Plugin, PluginInterface):
    def __init__(self, root, logger, risk_functions, units, name, code_type, limits=None, type_of_criteria=None):

        Plugin.__init__(self, root, logger, units=units, data_requirements=[ObjectConstantsForData.CROSS_SECTION], risk_functions=risk_functions, name=name, code_type=code_type)
        self.__root = root
        self.__max_value = None

        self._limits = limits
        self._type_of_criteria = type_of_criteria

        if root is not None:
            self.init_plugin_data(update=True)
            self.__gui = CrosssectionGUI(self, self.__root, self._data[ObjectConstantsForData.CROSS_SECTION].get_defined_section_names(),
                                         self._get_proper_defined_risk_functions())

    def calculate_result(self, section, type):
        # reset result labels
        self.__max_value, time, id, name = self._data[ObjectConstantsForData.CROSS_SECTION].get_max_secforc_of_section(section, type)
        return str(self.__max_value), str(time), str(id), name

    def plot_function(self, function_name, show_plot=False):
        plt.figure()
        self.__generate_plot(function_name, self.__max_value)
        if show_plot:
            plt.show(block=False)

    def __generate_plot(self, function_name, result):

        x_risk_1 = self.get_upper_limit(function_name)
        x_values = list(np.arange(0.0, x_risk_1, 0.001))  # TODO: manage range, maybe in lsfunction?
        y_values = self.calculate_risk(x_values, function_name)

        plt.plot(x_values, y_values, color="black")

        # plot vertical line of function result
        if result > x_values[-1]:
            result = x_values[-1]
            self._logger.emit(LOGConstants.WARNING[0], "Max. crossection value is higher than the highest value in the defined curve range for visualisation!")

        y_max_val = self.calculate_risk(result, function_name)
        plt.plot([result, result], [0, y_max_val], color="red")

        # set limits
        ymin, ymax = plt.ylim()
        plt.ylim((0.0, ymax))

        xmin, xmax = plt.xlim()
        plt.xlim((0.0, xmax))

        plt.xlabel("value")
        plt.ylabel("risk")

        plt.gca().text(0.02*xmax, ymax*0.98,
                       "Function f: " + function_name + "\nmax. value: " + '{:.4}'.format(result) + "\nf(max. value): "
                       + '{:.4}'.format(y_max_val), verticalalignment='top', fontsize=7)
        self.brand_plot_TU(plt.gca(), logo_width=0.1)

    def check_if_params_are_valid(self, param_dict):
        # TODO

        return True

    def calculate_and_store_results(self, param_dict):
        section = param_dict[PluginsParamDictDef.SECTION]
        type = param_dict[PluginsParamDictDef.TYPE]
        curve = param_dict[PluginsParamDictDef.CURVE].replace("_", " ")

        max_value, time, id, name = self.calculate_result(section=section, type=type)
        risk = self.calculate_risk(self.__max_value, curve)

        label_list_calc = [[{OutputStringForPlugins.TEXT: "Max value:"}, {OutputStringForPlugins.TEXT: max_value}],
                           [{OutputStringForPlugins.TEXT: "Time"}, {OutputStringForPlugins.TEXT: time}],
                           [{OutputStringForPlugins.TEXT: "Crosssection ID"}, {OutputStringForPlugins.TEXT: id}],
                           [{OutputStringForPlugins.TEXT: "Crosssection Title"}, {OutputStringForPlugins.TEXT: name}],
                           [{OutputStringForPlugins.TEXT: "Risk"}, {OutputStringForPlugins.TEXT: risk}]
                           ]

        result_str = self.get_output_string(separator=";", section=section, type=type, label_list_calc=label_list_calc)

        # create array with data from the returned string array
        self.extract_script_calc_array_and_title(result_str)
        self._overall_plot_data.append([curve, risk])

    def plot_calculations_to_pdf(self, pp):
        for (function_name, result) in self._overall_plot_data:
            if not function_name in self._function.get_function_names():
                self._logger.emit(LOGConstants.ERROR[0],
                                  "Function is not defined! Check Dynasaur.def File " + function_name)
                return

            f, axarr = plt.subplots(1, 1, figsize=(10, 10))
            self.__generate_plot(function_name, result)
            pp.savefig(f)  # saves the current figure into a pdf page
            plt.close(f)

    #TODO: Check parameters
    def get_output_string(self, separator=" ", section=None, type=None, label_list_calc=None):
        output_str = OutputStringForPlugins.SECTION + separator + (
            self.__gui.selection_section.get() if section == None else section) + "\n"
        if self._type_of_criteria is not None:
            output_str += OutputStringForPlugins.TYPE_OF_CRITERIA + separator + self._type_of_criteria[self._gui.selection_function.get()] + "\n"
        if self._limits is not None:
            output_str += OutputStringForPlugins.LIMITS + separator + self._limits[self._gui.selection_function.get()] + "\n"

        output_str += OutputStringForPlugins.TYPE + separator + (self.__gui.selection_type.get() if type == None else type) + "\n\n"

        label_list_calc = self.__gui.label_list_calc if label_list_calc == None else label_list_calc

        for row_list in label_list_calc:
            for index, entry in enumerate(row_list):
                if index != 0:
                    output_str += separator + str(entry[OutputStringForPlugins.TEXT])
                else:
                    output_str += str(entry[OutputStringForPlugins.TEXT])
            output_str += "\n"

        return output_str
