# ***********************************************************************************************************************
#
# Jakub Micorek
#
# ***********************************************************************************************************************

import tkinter as tk
from tkinter import ttk

from Data.secforc import Secforc
from plugin import GUI, GUIInterface


class CrosssectionGUI(GUI, GUIInterface):
    def __init__(self, controller, root, section_names, function_names):
        GUI.__init__(self, root, controller, width_definition=[[9, 31], [17, 23], [15, 25]], pad_x=[(5, 10), (0, 5)],
                     pad_y=[(3, 3), (10, 10)])

        # GUI variables
        self.__window = tk.Toplevel(root)

        self.__list_section = section_names
        self.selection_section = tk.StringVar()
        self.selection_section.set(self.__list_section[0])

        self.__list_type = Secforc.TYPES
        self.selection_type = tk.StringVar()
        self.selection_type.set(self.__list_type[0])

        self.__list_functions = list(function_names)
        self.selection_function = tk.StringVar()
        self.selection_function.set(self.__list_functions[0])

        self._build_GUI()
        self._gui_update()

    def _build_GUI(self):
        self.__window.resizable(width=False, height=False)
        self.__window.title("Crosssection")

        menu = tk.Menu(self.__window)
        self.__window.config(menu=menu)

        # File menu
        file_menu = tk.Menu(menu)
        file_menu.add_command(label="Copy Text", accelerator="Ctrl+C", command=self._gui_copy_text)
        self.__window.bind("<Control-c>", self._gui_copy_text)

        file_menu.add_command(label="Save Text", accelerator="Ctrl+S", command=self._gui_save_text)
        self.__window.bind("<Control-s>", self._gui_save_text)

        menu.add_cascade(label="File", menu=file_menu)

        ################################################################################################################
        frame_margin = ttk.Frame(self.__window, width=sum(self._width_definition[0]))
        frame_margin.grid(row=0, column=0, padx=self._pad_x[1], pady=self._pad_y[1])

        global_row = 0
        frame_selection = ttk.Labelframe(frame_margin, text="Setup", width=sum(self._width_definition[0]))
        frame_selection.grid(row=global_row, column=0, pady=self._pad_y[0])

        global_row += 1
        frame_result = ttk.Labelframe(frame_margin, text="Result", width=sum(self._width_definition[0]))
        frame_result.grid(row=global_row, column=0, pady=self._pad_y[0])

        global_row += 1
        frame_risk = ttk.Labelframe(frame_margin, text="Risk", width=sum(self._width_definition[0]))
        frame_risk.grid(row=global_row, column=0, pady=self._pad_y[0])

        global_row += 1
        button_plot = ttk.Button(frame_margin, text="Plot", width=sum(self._width_definition[0]) + 4,
                                 command=self._gui_plot)
        button_plot.grid(row=global_row, column=0)

        ################################################################################################################
        #### setup section
        width = self._width_definition[0]
        row = 0
        lbl = ttk.Label(frame_selection, text="Section:", anchor="e", width=width[0])
        lbl.grid(row=row, column=0, padx=self._pad_x[0], pady=self._pad_y[0])
        combo_box_section = ttk.Combobox(frame_selection, state="readonly", width=(width[1] - 3),
                                         textvariable=self.selection_section,
                                         values=self.__list_section)
        combo_box_section.bind("<<ComboboxSelected>>", self._gui_update)
        combo_box_section.grid(row=row, column=1, padx=self._pad_x[1])

        row += 1

        lbl = ttk.Label(frame_selection, text="Type:", anchor="e", width=width[0])
        lbl.grid(row=row, column=0, padx=self._pad_x[0], pady=self._pad_y[0])
        combo_box_type = ttk.Combobox(frame_selection, state="readonly", width=(width[1] - 3),
                                      textvariable=self.selection_type,
                                      values=self.__list_type)
        combo_box_type.bind("<<ComboboxSelected>>", self._gui_update)
        combo_box_type.grid(row=row, column=1, padx=self._pad_x[1])

        ################################################################################################################
        #### result section

        row = 0
        self.label_list_calc.append([])
        self._create_label(frame_result, "Max value:", row, 0, 1)
        self._create_label(frame_result, "-", row, 1, 1)

        row += 1
        self.label_list_calc.append([])
        self._create_label(frame_result, "Time:", row, 0, 1)
        self._create_label(frame_result, "-", row, 1, 1)

        row += 1
        self.label_list_calc.append([])
        self._create_label(frame_result, "Crosssection ID:", row, 0, 1)
        self._create_label(frame_result, "-", row, 1, 1)

        row += 1
        self.label_list_calc.append([])
        self._create_label(frame_result, "Crosssection Title:", row, 0, 1)
        self._create_label(frame_result, "-", row, 1, 1)

        ################################################################################################################
        #### risk section
        width = self._width_definition[2]
        row = 0
        label = ttk.Label(frame_risk, text="Risk Function:", anchor="e", width=width[0])
        label.grid(row=row, column=0, padx=self._pad_x[0], pady=self._pad_y[0])
        combo_box_function = ttk.Combobox(frame_risk, state="readonly", width=(width[1] - 3),
                                          textvariable=self.selection_function,
                                          values=self.__list_functions)
        combo_box_function.bind("<<ComboboxSelected>>", self._gui_update)
        combo_box_function.grid(row=row, column=1, sticky="w", padx=self._pad_x[1])

        row += 1
        self.label_list_calc.append([])
        self._create_label(frame_risk, "Risk:", row, 0, 2)
        self._create_label(frame_risk, "-", row, 1, 2)

    def _gui_plot(self, event=None):
        function_name = self.selection_function.get()
        self._controller.plot_function(function_name=function_name, show_plot=True)

    def _gui_update(self, event=None):
        selected_section = self.selection_section.get()
        selected_type = self.selection_type.get()
        max_val, time, id, name = self._controller.calculate_result(section=selected_section, type=selected_type)
        risk = self._controller.calculate_risk(float(max_val), self.selection_function.get())

        self.label_list_calc[0][1]["text"] = max_val
        self.label_list_calc[1][1]["text"] = '{:.4}'.format(time)
        self.label_list_calc[2][1]["text"] = id
        self.label_list_calc[3][1]["text"] = name
        self.label_list_calc[4][1]["text"] = '{:.4}'.format(risk)
