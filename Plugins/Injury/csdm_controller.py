# Class describing CSDM Head Injury Criterion
#
# Author: Robert Greimel
# Version: 5.June.2014
#
# -----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt

from Data.elout import EloutIndex
from Plugins.Injury.csdm_gui import CSDM_GUI
from plugin import Plugin, PluginInterface
from constants import LOGConstants, OutputStringForPlugins, PluginsParamDictDef, ObjectConstantsForData


#

class CSDMController(Plugin, PluginInterface):
    def __init__(self, root, logger, risk_functions, units, name, code_type, limits=None, type_of_criteria=None):
        Plugin.__init__(self, root, logger, units=units, data_requirements=[ObjectConstantsForData.ELEMENT, ObjectConstantsForData.VOLUME],
                        risk_functions=risk_functions, name=name, code_type=code_type)

        # self._data['ELEMENT'] = elout
        # self._data['VOLUME'] = volume
        self.__limit = 0.2

        self._limits = limits
        self._type_of_criteria = type_of_criteria


        if root is not None:
            self.init_plugin_data(update=True)
            obj = self._data[ObjectConstantsForData.VOLUME].get_defined_objects()

            self.__gui = CSDM_GUI(self, root, obj, self.__limit, self._get_proper_defined_risk_functions())

    def calculate(self, limit, obj, function, start_time, end_time):

        start_time = float(start_time) if start_time != "" \
                                          and start_time is not None \
                                          and start_time != "None" \
                                       else self._data[ObjectConstantsForData.ELEMENT].get_time()[0]
        end_time = float(end_time) if end_time != "" \
                                      and end_time is not None \
                                      and start_time != "None" else self._data[ObjectConstantsForData.ELEMENT].get_time()[-1]

        if start_time >= end_time:
            end_time = self._data[ObjectConstantsForData.ELEMENT].get_time()[-1]
            self._logger.emit(LOGConstants.ERROR[0], "End time smaller than start time")

        self.__limit = float(limit) if 0.0 < float(limit) < 1.0 else self.__limit
        injvol = 0.0
        sumvol = 0.0

        time_step_indices = np.argwhere((start_time * self._units.second() <= self._data[ObjectConstantsForData.ELEMENT].get_time()) &
                                        (end_time * self._units.second() >= self._data[ObjectConstantsForData.ELEMENT].get_time()))

        for part_id in self._data[ObjectConstantsForData.ELEMENT].get_part_ids_by_object_name(obj, EloutIndex.STRAIN):
            index, data = self._data[ObjectConstantsForData.ELEMENT].get_part_data(EloutIndex.STRAIN, part_id, time_step_indices=time_step_indices.flatten())
            part__volume = self._data[ObjectConstantsForData.VOLUME].get_part_volume_by_part_ID(part_id)

            # calculate csdm
            # absolute maximum over all time steps for each element
            abs_max_all = np.max(np.max(np.abs(data), axis=2), axis=0)
            # get element ids which are greater than the limit
            element_ids_greater_limit = index[np.where(abs_max_all > self.__limit)[0], 0]

            csdm = 0.0
            if len(element_ids_greater_limit) != 0:
                volumes = [self._data[ObjectConstantsForData.VOLUME].get_element_volume_by_part_id_and_element_id(part_id, element_id) for element_id in
                           element_ids_greater_limit]
                csdm = sum(volumes) / part__volume

            sumvol += part__volume
            injvol += (part__volume * csdm)

        csdm = injvol / sumvol

        aisrisk = self.calculate_risk(csdm, curve=function)
        return csdm, aisrisk

    def __generate_plot(self, curve, csdm, logo_width=0.1):
        pts = 500

        x = np.arange(0, 1, 1 / pts)
        y = self.calculate_risk(list(x), curve=curve)

        plt.plot(x, y, color="black")

        v = np.minimum(1.0, csdm)
        plt.plot([v, v], [0, self.calculate_risk(v, curve=curve)], color="red", label=curve)


        # set limits
        ymin, ymax = plt.ylim()
        plt.ylim((0.0, ymax))

        xmin, xmax = plt.xlim()
        plt.xlim((0.0, xmax))

        plt.xlabel("csdm")
        plt.ylabel("risk")

        plt.gca().text(0.02 * xmax, ymax * 0.98,
                       "Function f: " + curve + "\ncsdm: " + '{:.4}'.format(csdm) +
                       "\nrisk: " + '{:.4}'.format(self.calculate_risk(v, curve=curve)),
                       verticalalignment='top', fontsize=7)

        #plt.legend(loc=2)

        self.brand_plot_TU(plt.gca(), logo_width=logo_width)

    def plot_csdm(self, curve, show_plot=False, csdm=0):

        plt.figure()
        self.__generate_plot(curve=curve, csdm=csdm, logo_width=0.15)

        if show_plot:
            plt.show(block=False)

    ####################################################################################################################
    def get_output_string(self, separator=" ", selected_object=None, limit=None, selected_function=None,
                          label_list_calc=None, start_time=None, end_time=None):
        # setup info
        output_string = OutputStringForPlugins.OBJECT + separator + (
            str(self.__gui.selection_object.get()) if selected_object == None else str(selected_object)) + "\n"
        output_string += OutputStringForPlugins.LIMIT + separator + (str(self.__gui.limit.get()) if limit == None else str(limit)) + "\n"
        output_string += OutputStringForPlugins.RISK_FUNCTION + separator + (
            str(self.__gui.selection_function.get()) if selected_function == None else selected_function) + "\n"
        output_string += OutputStringForPlugins.START_TIME + separator + str(
            self._gui.start_time.get() if start_time is None else start_time) + "\n"
        output_string += OutputStringForPlugins.END_TIME + separator + str(
            self._gui.end_time.get() if end_time is None else end_time) + "\n\n"

        if self._type_of_criteria is not None and len(self._type_of_criteria):
            output_string += OutputStringForPlugins.TYPE_OF_CRITERIA + separator + (
                self._type_of_criteria[str(self.__gui.selection_function.get()) if selected_function == None else selected_function]) + "\n"
        if self._limits is not None and len(self._limits):
            output_string += OutputStringForPlugins.LIMITS + separator + (
                self._limits[
                    str(self.__gui.selection_function.get()) if selected_function == None else selected_function]) + "\n\n"

        # result
        #output_string = ""
        label_list_calc = self.__gui.label_list_calc if label_list_calc == None else label_list_calc
        for key in label_list_calc:
            for index, entry in enumerate(key):
                if index != 0:
                    output_string += separator + str(entry[OutputStringForPlugins.TEXT]) + "\n"
                else:
                    output_string += str(entry[OutputStringForPlugins.TEXT])

        return output_string

    def check_if_params_are_valid(self, param_dict):
        # TODO

        return True

    def calculate_and_store_results(self, param_dict):

        obj = param_dict[PluginsParamDictDef.OBJECT].replace("_", " ")
        limit = param_dict[PluginsParamDictDef.LIMIT]
        func = param_dict[PluginsParamDictDef.CURVE]
        start_time = param_dict[PluginsParamDictDef.START_TIME] if PluginsParamDictDef.START_TIME in param_dict \
                                                             else self._data[ObjectConstantsForData.ELEMENT].get_time()[0]
        end_time = param_dict[PluginsParamDictDef.END_TIME] if PluginsParamDictDef.END_TIME in param_dict \
                                                            else self._data[ObjectConstantsForData.ELEMENT].get_time()[-1]

        csdm, risk = self.calculate(limit, obj, func, start_time=start_time, end_time=end_time)

        label_list_calc = [[{OutputStringForPlugins.TEXT: "CSDM:"}, {OutputStringForPlugins.TEXT: csdm}],
                           [{OutputStringForPlugins.TEXT: "Risk:"}, {OutputStringForPlugins.TEXT: risk}]]

        result_str = self.get_output_string(separator=";", limit=limit, selected_object=obj, selected_function=func,
                                            label_list_calc=label_list_calc, start_time=start_time, end_time=end_time)

        self.extract_script_calc_array_and_title(result_str)

        # stores the CSDM data
        self._overall_plot_data.append([csdm, func])

    def plot_calculations_to_pdf(self, pp):
        for (result) in self._overall_plot_data:
            f, axarr = plt.subplots(1, 1, figsize=(10, 10))
            self.__generate_plot(result[1], result[0], logo_width=0.1)
            pp.savefig(f)  # saves the current figure into a pdf page
            plt.close(f)
