# Class describing CSDM Head Injury Criterion
#
# Author: Robert Greimel
# Version: 5.June.2014
#
# -----------------------------------------------------------------------
import tkinter as tk
from tkinter import ttk
from plugin import GUI, GUIInterface


class CSDM_GUI(GUI, GUIInterface):
    def __init__(self, controller, root, objects, limit, function_names):
        GUI.__init__(self, root, controller, width_definition=[[10, 33], [20, 23]], pad_x=[(5, 10), (0, 5)],
                     pad_y=[(3, 3), (10, 10)])

        self.__window = tk.Toplevel(root)

        # GUI variables
        self.__list_objects = objects
        self.selection_object = tk.StringVar()
        self.selection_object.set(self.__list_objects[0])

        self.limit = tk.StringVar()
        self.limit.set(str(limit))

        self.start_time = tk.StringVar()
        self.end_time = tk.StringVar()

        self.__list_functions = list(function_names)
        self.selection_function = tk.StringVar()
        self.selection_function.set(self.__list_functions[0])

        self._build_GUI()
        self._gui_update()

    def _build_GUI(self):
        # create windowdynasaur.dsf
        self.__window.title("CSDM")

        # create menu
        mainmenu = tk.Menu(self.__window)
        self.__window.config(menu=mainmenu)

        # File menu
        filemenu = tk.Menu(mainmenu)
        filemenu.add_command(label="Copy Text", command=self._gui_copy_text)
        filemenu.add_command(label="Save Text", command=self._gui_save_text)

        mainmenu.add_cascade(label="File", menu=filemenu)

        ################################################################################################################
        #### define global frames and buttons

        # for margin of all inner frames
        frame_margin = ttk.Frame(self.__window, width=sum(self._width_definition[0]))
        frame_margin.grid(row=0, column=0, padx=self._pad_x[0], pady=self._pad_y[1])

        # define setup frame
        row = 0
        frame_setup = ttk.Labelframe(frame_margin, text="Setup", width=sum(self._width_definition[0]))
        frame_setup.grid(row=row, column=0)

        # define result frame
        row += 1
        frame_results = ttk.Labelframe(frame_margin, text="Result", width=sum(self._width_definition[0]))
        frame_results.grid(row=row, column=0)

        # define result frame
        row += 1
        frame_risk = ttk.Labelframe(frame_margin, text="Risk", width=sum(self._width_definition[0]))
        frame_risk.grid(row=row, column=0)

        # plot button
        row += 1
        btn = ttk.Button(frame_margin, text="Plot", width=(sum(self._width_definition[0]) + 4), command=self._gui_plot)
        btn.grid(row=row, column=0, pady=self._pad_y[0])

        ################################################################################################################
        #### setup section
        width = self._width_definition[0]
        row_setup = 0
        lbl = ttk.Label(frame_setup, text="Object:", anchor="e", width=width[0])
        lbl.grid(row=row_setup, column=0, padx=self._pad_x[0], pady=self._pad_y[0])

        combo_box_object = ttk.Combobox(frame_setup, state="readonly", textvariable=self.selection_object,
                                        values=self.__list_objects, width=(width[1] - 3))
        combo_box_object.bind("<<ComboboxSelected>>", self._gui_update)
        combo_box_object.grid(row=row_setup, column=1, padx=self._pad_x[1])

        row_setup += 1
        lbl = ttk.Label(frame_setup, text="Limit:", anchor="e", width=width[0])
        lbl.grid(row=row_setup, column=0, padx=self._pad_x[0], pady=self._pad_y[0])

        entry = ttk.Entry(frame_setup, textvar=self.limit, justify='right', width=width[1])
        entry.grid(row=row_setup, column=1, padx=self._pad_x[1])

        row_setup += 1
        lbl = ttk.Label(frame_setup, text="Start time [s]:", anchor="e", width=15)
        lbl.grid(row=row_setup, column=0, padx=self._pad_x[0], pady=self._pad_y[0])

        entry = ttk.Entry(frame_setup, textvar=self.start_time, justify='right', width=width[1])
        entry.grid(row=row_setup, column=1, padx=self._pad_x[1])

        row_setup += 1
        lbl = ttk.Label(frame_setup, text="End time [s]:", anchor="e", width=15)
        lbl.grid(row=row_setup, column=0, padx=self._pad_x[0], pady=self._pad_y[0])

        entry = ttk.Entry(frame_setup, textvar=self.end_time, justify='right', width=width[1])
        entry.grid(row=row_setup, column=1, padx=self._pad_x[1])

        self.limit.trace('w', lambda name, index, mode: self._gui_update())
        self.start_time.trace('w', lambda name, index, mode: self._gui_update())
        self.end_time.trace('w', lambda name, index, mode: self._gui_update())

        ################################################################################################################
        #### result section
        width = self._width_definition[1]

        self.label_list_calc.append([])
        row = 0
        self._create_label(frame_results, "CSDM:", row, 0, 1)
        self._create_label(frame_results, "-", row, 1, 1)

        ################################################################################################################
        #### risk section
        row_risk = 0
        lbl = ttk.Label(frame_risk, text="Risk Function:", anchor="e", width=width[0])
        lbl.grid(row=row_risk, column=0, padx=self._pad_x[0], pady=self._pad_y[0])
        combo_box_function = ttk.Combobox(frame_risk, state="readonly", width=(width[1] - 3),
                                          textvariable=self.selection_function, values=self.__list_functions)
        combo_box_function.bind("<<ComboboxSelected>>", self._gui_update)
        combo_box_function.grid(row=row_risk, column=1, padx=self._pad_x[1])

        row_risk += 1

        self.label_list_calc.append([])
        row += 1
        self._create_label(frame_risk, "Risk:", row, 0, 1)
        self._create_label(frame_risk, "-", row, 1, 1)

    def _gui_plot(self, event=None):
        csdm, risk = self._controller.calculate(self.limit.get(), self.selection_object.get(),
                                                self.selection_function.get(), self.start_time.get(), self.end_time.get())
        self._controller.plot_csdm(curve=self.selection_function.get(), show_plot=True, csdm=csdm)

    def _gui_update(self, event=None):
        csdm, risk = self._controller.calculate(self.limit.get(), self.selection_object.get(),
                                                self.selection_function.get(), self.start_time.get(), self.end_time.get())
        self.label_list_calc[0][1]["text"] = '{:.4}'.format(csdm)
        self.label_list_calc[1][1]["text"] = '{:.4}'.format(risk)
