# Class describing limits for principal stress or strain data
#
# Author: Peter Luttenberger, Robert Greimel, Jakub Micorek
# Version: 29.June.2016
# -----------------------------------------------------------------------
class UniversalLimit():
    STRAIN_STRESS_VALUES = ["Strain", "Stress"]
    TENSION_COMPRESSION_VALUES = ["Overall", "Tension", "Compression", "vonMises"]

    def __init__(self, strain_tension=None, strain_compression=None, stress_tension=None, stress_compression=None):
        if (strain_compression is not None and strain_compression >= 0.0) or (
            stress_compression is not None and stress_compression >= 0.0):
            raise Exception("UniversalLimit(): Compression has to be negative!")

        self.__limits = {}
        self.__limits["Strain"] = {}
        self.__limits["Strain"]["Tension"] = strain_tension
        self.__limits["Strain"]["Compression"] = strain_compression

        self.__limits["Stress"] = {}
        self.__limits["Stress"]["Tension"] = stress_tension
        self.__limits["Stress"]["Compression"] = stress_compression

    def get_limit(self, strain_stress, tension_compression):
        # both None
        if self.__limits[strain_stress]["Tension"] is None and self.__limits[strain_stress]["Compression"] is None:
            return None

        if tension_compression == UniversalLimit.TENSION_COMPRESSION_VALUES[0]:
            if self.__limits[strain_stress]["Tension"] is not None and self.__limits[strain_stress][
                "Compression"] is not None:
                return min(abs(self.__limits[strain_stress]["Tension"]),
                           abs(self.__limits[strain_stress]["Compression"]))
            elif self.__limits[strain_stress]["Tension"] is not None:
                tension_compression = "Tension"
            elif self.__limits[strain_stress]["Compression"] is not None:
                tension_compression = "Compression"

        try:
            # tension or compression selected
            return self.__limits[strain_stress][tension_compression]
        except:
            return None


class Limits(object):
    def __init__(self):
        self.__limit_te = {}
        self.__limit_te["femur"] = 0.014
        self.__limit_te["tibia"] = 0.023
        self.__limit_te["fibra"] = 0.016
        #
        self.__limit_co = {}
        self.__limit_co["femur"] = 0.019
        self.__limit_co["tibia"] = 0.016
        self.__limit_co["fibra"] = 0.016
        #
        self.__limit_te["humerus"] = self.__limit_te["femur"]
        self.__limit_te["ulna"] = self.__limit_te["tibia"]
        self.__limit_te["radius"] = self.__limit_te["fibra"]
        #
        self.__limit_co["humerus"] = self.__limit_co["femur"]
        self.__limit_co["ulna"] = self.__limit_co["tibia"]
        self.__limit_co["radius"] = self.__limit_co["fibra"]

        #
        self.__limit_universal = {}
        self.__limit_universal["Heart (Spon)"] = UniversalLimit(strain_tension=0.3)
        self.__limit_universal["Left Lung (Spon)"] = UniversalLimit(strain_tension=1e-05)
        self.__limit_universal["Liver (Spon)"] = UniversalLimit(strain_tension=0.3)
        self.__limit_universal["Right Lung (Spon)"] = UniversalLimit(strain_tension=1e-05)
        self.__limit_universal["Small Intestine (Spon)"] = UniversalLimit(strain_tension=1.2)
        self.__limit_universal["Spleen (Spon)"] = UniversalLimit(strain_tension=0.3)

        self.__limit_universal["Brain"] = UniversalLimit(strain_tension=0.2, strain_compression=-0.2)
        self.__limit_universal["Left Brain"] = UniversalLimit(strain_tension=0.2, strain_compression=-0.2)
        self.__limit_universal["Right Brain"] = UniversalLimit(strain_tension=0.2, strain_compression=-0.2)

        self.__limit_universal["Skull (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Skull external (cort)"] = UniversalLimit(strain_tension=0.015,
                                                                         strain_compression=-0.015)
        self.__limit_universal["Skull internal (cort)"] = UniversalLimit(strain_tension=0.015,
                                                                         strain_compression=-0.015)
        self.__limit_universal["Skull (spon)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)

        self.__limit_universal["Ribs"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Left Ribs"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Right Ribs"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)

        self.__limit_universal["Left Femur (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Right Femur (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Left Tibia (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Right Tibia (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Left Humerus (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Right Humerus (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)

        self.__limit_universal["Right Pelvis (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Left Pelvis (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)
        self.__limit_universal["Pelvis (cort)"] = UniversalLimit(strain_tension=0.015, strain_compression=-0.015)

        self.__limit_universal["Right Pelvis without Acetabulum (cort)"] = UniversalLimit(strain_tension=0.015,
                                                                                          strain_compression=-0.015)
        self.__limit_universal["Left Pelvis without Acetabulum (cort)"] = UniversalLimit(strain_tension=0.015,
                                                                                         strain_compression=-0.015)
        self.__limit_universal["Pelvis without Acetabulum (cort)"] = UniversalLimit(strain_tension=0.015,
                                                                                    strain_compression=-0.015)

    def get_universal_object_limit_by_name(self, name, strain_stress, tension_compression):
        if name in self.__limit_universal:
            limit = self.__limit_universal[name].get_limit(strain_stress, tension_compression)
            if limit is not None:
                return str(limit)

        return ""

# Right Radius (cort)
# Left Pelvis (Spon)
# PCL Right
# Spleen (Spon)
# PCL Left
# Liver, Spleen, Small Intestine, Large Intestine, Heart, Left Lung, Right Lung (Spon)
# Upper Leg General
# Sternum Left (Spon)
# Left Ulna (cort)
# Left Fibra (cort)
# Right Pelvis (Spon)
# Left Pelvis (cort)
# Right Lung (Spon)
# Large Intestine (Spon)
# Left Upper Extremities, Long Bones (Cort)
# Right Tibia (cort)
# Ligaments General
# Left Radius (cort)
# Brain
# Organs General
# Right Brain
# Ligaments Right
# Left Brain
# Left and Right Long Bones (Cort)
# Right Upper Extremities, Long Bones (Cort)
# Left Tibia (cort)
# Long Bones General
# Sternum Right (Spon)
# Right Ulna (cort)
# Ligaments Left
# ACL Right
# Left Lung (Spon)
# Left Humerus (cort)
# Left Lower Extremities, Long Bones (Cort)
# Left Femur (cort)
# MCL Left
# FTL Right
# Right Pelvis (cort)
# LCL Right
# Liver (Spon)
# Dynasaur Everything
# Right Lower Extremities, Long Bones (Spon)
# Left Lower Extremities, Long Bones (Spon)
# Sternum Right (Cort)
# Left Lower Thigh Skin
# Right Femur (cort)
# ACL Left
# Right Humerus (cort)
# Sternum Left (Cort)
# Sternum (Spon, Cort)
# Small Intestine (Spon)
# Left Ribs
# MCL Right
# FTL Left
# Right Lower Extremities, Long Bones (Cort)
# Cerebrum
# LCL Left
# Right Fibra (cort)
# Right Ribs
# Heart (Spon)
