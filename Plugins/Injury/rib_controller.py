import numpy as np
import itertools

from plugin import Plugin, PluginInterface
from Plugins.Injury.rib_gui import RibGUI
from Data.elout import EloutIndex
from constants import RibFractureConstants, LOGConstants, ObjectConstantsForData, OutputStringForPlugins, PluginsParamDictDef
from exportset import ExportSet


class RibController(Plugin, PluginInterface):
    def __init__(self, root, logger, units, risk_functions, name, code_type, limits=None, type_of_criteria=None):
        Plugin.__init__(self, root, logger, units=units, data_requirements=[ObjectConstantsForData.ELEMENT],
                        risk_functions=risk_functions, name=name, code_type=code_type)

        self.__max_value = None
        self.__defined_rib_objects = None

        self._limits = limits
        self._type_of_criteria = type_of_criteria

        if root is not None:
            self.init_plugin_data(update=True)
            if not self.init_rib_definitions():
                self._logger.emit(LOGConstants.ERROR[0],
                                   "No Ribs defined. Check your model and/or definition file")
                return

            self._gui = RibGUI(self, root, self.__defined_rib_objects, self._get_proper_defined_risk_functions())

    def __calculate_age_risk(self, us, age, function_name):
        if np.isnan(us):
            return 0.0
        sus = 100. * (us / (1.0 - (age - 25.) * 0.0051))
        result = self._function.calculate_function(function_name, sus)
        return result

    def init_rib_definitions(self):
        defined_rib_objects = [val for val in
                               [RibFractureConstants.LEFT_RIBS, RibFractureConstants.RIGHT_RIBS] if
                               val in self._data[ObjectConstantsForData.ELEMENT].get_defined_objects()]

        self.__defined_rib_objects = [RibFractureConstants.ALL_RIBS] + defined_rib_objects if len(
            defined_rib_objects) == 2 else defined_rib_objects

        if len(defined_rib_objects) == 0:
            return False

        return True

    def __calc_num_frac(self, smax, function_name, age=25):
        rib_ids = list(smax.keys())
        # calculate risks
        risk = {id: self.__calculate_age_risk(smax[id], age, function_name) for id in rib_ids}

        # remove zero risk entries
        # due to performance issues in the calculation (combinatoric!!)
        for i in reversed(list(range(len(rib_ids)))):
            if risk[rib_ids[i]] == 0.0:
                del risk[rib_ids[i]]
                del rib_ids[i]

        # X = 0 to 7 fractures
        sf = []
        for f in range(8):
            s = 0.0
            c = 0
            # for i in itertools.permutations(a,f):
            for i in itertools.combinations(rib_ids, f):
                c += 1
                p1 = 1.0
                for j in i:
                    p1 = p1 * risk[j]
                p2 = 1.0
                for k in np.setdiff1d(np.array(rib_ids), np.array(i)):
                    p2 = p2 * (1.0 - risk[k])
                s = s + p1 * p2
            sf.append(s)
            # print(("fractures", f, s, c))
        sf.append(1 - np.sum(sf))
        return sf

    def calc_risk_for_rib_object(self, object_name, nr_largest_elements, age, function_name, start_time, end_time):
        smax = {}
        start_time = float(start_time) if start_time != "" \
                                          and start_time is not None \
                                          and start_time != "None" \
                                        else self._data[ObjectConstantsForData.ELEMENT].get_time()[0]
        end_time = float(end_time) if end_time != "" \
                                      and end_time is not None \
                                      and start_time != "None" \
                                    else self._data[ObjectConstantsForData.ELEMENT].get_time()[-1]

        if start_time >= end_time:
            end_time = self._data[ObjectConstantsForData.ELEMENT].get_time()[-1]
            self._logger.emit(LOGConstants.ERROR[0], "End time smaller than start time, end time set to last time step")

        solid = {OutputStringForPlugins.ALL_MAX: [], OutputStringForPlugins.FRAC_MAX: [], OutputStringForPlugins.ALL_FRAC: np.array([]).astype(dtype=int)}
        shell = {OutputStringForPlugins.ALL_MAX: [], OutputStringForPlugins.FRAC_MAX: [], OutputStringForPlugins.ALL_FRAC: np.array([]).astype(dtype=int)}
        self.d = {EloutIndex.SHELL: shell, EloutIndex.SOLID: solid}

        time_step_indices = np.argwhere((start_time * self._units.second() <= self._data[ObjectConstantsForData.ELEMENT].get_time()) &
                                        (end_time * self._units.second() >= self._data[ObjectConstantsForData.ELEMENT].get_time()))

        ls = [RibFractureConstants.LEFT_RIBS,
              RibFractureConstants.RIGHT_RIBS] if object_name == RibFractureConstants.ALL_RIBS else [object_name]
        detail_risk_matrix = {}
        for object_name in [val for val in ls if val in self._data[ObjectConstantsForData.ELEMENT].get_defined_objects()]:
            detail_risk_matrix[object_name] = np.empty((0, 4))
            element_count = 0

            x = self._function.get_x_where_risk_is_value(function_name, 0.5)

            # NOTE
            # TODO
            # compare with universal controller
            # calculate mean for the main directions ... maybe
            # maximum value for all timesteps
            # maximum over the time!
            for part_id in self._data[ObjectConstantsForData.ELEMENT].get_part_ids_by_object_name(object_name, EloutIndex.STRAIN):
                abs_max_all = np.empty(shape=(0,2))
                index, data = self._data[ObjectConstantsForData.ELEMENT].get_part_data(EloutIndex.STRAIN, part_id, time_step_indices=time_step_indices.flatten())
                element_ids = self._data[ObjectConstantsForData.ELEMENT].get_element_ids_of_part_data(index)
                element_count += len(element_ids)

                element_type = self._data[ObjectConstantsForData.ELEMENT].get_element_type_from_part_id(part_id, EloutIndex.STRAIN)

                for element_id in element_ids:
                    row_ids = self._data[ObjectConstantsForData.ELEMENT].get_integration_point_indices_of_part_data_by_element_id(index, element_id)
                    reduced_integration_point_data = np.mean(data[:, row_ids, :], axis=1)
                    result_data = np.max(reduced_integration_point_data, axis=1)
                    try:
                        res = np.max(np.abs(result_data[~np.isnan(result_data)]))
                    except ValueError:
                        res = 0
                    abs_max_all = np.concatenate((abs_max_all, [[res, element_id]]), axis=0)
                    if res > x / 100:
                        self.d[element_type][OutputStringForPlugins.ALL_FRAC] = np.append(self.d[element_type][OutputStringForPlugins.ALL_FRAC], element_id)

                ind_of_largest_elements = abs_max_all[:,0].argsort()[-nr_largest_elements:][::-1]
                max_index = ind_of_largest_elements[-1]

                abs_max_value = abs_max_all[max_index][0]
                if np.isnan(abs_max_value):
                    self._logger.emit(LOGConstants.WARNING[0],
                                      "Due to deleted elements, the maximum Value for " + str(part_id) + " is NaN!")


                element_id_abs_max = abs_max_all[max_index][1]

                # store the values for the export set
                self.d[element_type][OutputStringForPlugins.ALL_MAX].append(int(element_id_abs_max))

                if self.__calculate_age_risk(abs_max_value, age, function_name) > 0.0:
                    self.d[element_type][OutputStringForPlugins.FRAC_MAX].append(int(element_id_abs_max))


                smax[part_id] = abs_max_value
                detail_entries = np.array([[int(part_id), int(element_id_abs_max), '{:.4}'.format(abs_max_value),
                                            '{:.2%}'.format(
                                                self.__calculate_age_risk(abs_max_value, age, function_name))]])
                detail_risk_matrix[object_name] = np.append(detail_risk_matrix[object_name], detail_entries, axis=0)


        broken_ribs_prob = np.array(self.__calc_num_frac(smax, function_name, age))

        return broken_ribs_prob, detail_risk_matrix

    def save_max(self, file_name, object_name, age, function_name):
        # open output file
        hw = ExportSet()
        hw.open(file_name)

        if len(self.d[EloutIndex.SOLID][OutputStringForPlugins.ALL_MAX]) > 0:
            hw.set_solid(self.d[EloutIndex.SOLID][OutputStringForPlugins.ALL_MAX], 666001, "Rib_Max_Solid_List")
        if len(self.d[EloutIndex.SOLID][OutputStringForPlugins.FRAC_MAX]) > 0:
            hw.set_solid(self.d[EloutIndex.SOLID][OutputStringForPlugins.FRAC_MAX], 666011, "Rib_Fracture_Max_Solid_List")
        if len(self.d[EloutIndex.SOLID][OutputStringForPlugins.ALL_FRAC]) > 0:
            hw.set_solid(self.d[EloutIndex.SOLID][OutputStringForPlugins.ALL_FRAC], 666021, "Rib_Fracture_Solid_List")

        # write shells
        if len(self.d[EloutIndex.SHELL][OutputStringForPlugins.ALL_MAX]) > 0:
            hw.set_shell(self.d[EloutIndex.SHELL][OutputStringForPlugins.ALL_MAX], 666002, "Rib_Max_Shell_List")
        if len(self.d[EloutIndex.SHELL][OutputStringForPlugins.FRAC_MAX]) > 0:
            hw.set_shell(self.d[EloutIndex.SHELL][OutputStringForPlugins.FRAC_MAX], 666012, "Rib_Fracture_Max_Shell_List")
        if len(self.d[EloutIndex.SHELL][OutputStringForPlugins.ALL_FRAC]) > 0:
            hw.set_shell(self.d[EloutIndex.SHELL][OutputStringForPlugins.ALL_FRAC], 666022, "Rib_Fracture_Shell_List")
        hw.close()

    def check_if_params_are_valid(self, param_dict):
        # TODO

        return True

    # script support
    def calculate_and_store_results(self, param_dict):

        if not self.init_rib_definitions():  # define left and right parts of the ribs
            return

        object_name = "CHEST:" + param_dict[PluginsParamDictDef.OBJECT_NAME].replace("_", " ")
        nr_largest_elements = param_dict[PluginsParamDictDef.NR_LARGESR_EL]
        age = param_dict[PluginsParamDictDef.AGE]
        function_name = param_dict[PluginsParamDictDef.FUNCTION_NAME]
        start_time = param_dict[PluginsParamDictDef.START_TIME] if PluginsParamDictDef.START_TIME in param_dict \
                                                            else self._data[ObjectConstantsForData.ELEMENT].get_time()[0]
        end_time = param_dict[PluginsParamDictDef.END_TIME] if PluginsParamDictDef.END_TIME in param_dict \
                                                            else self._data[ObjectConstantsForData.ELEMENT].get_time()[-1]

        risk_matrix, detail_risk_matrix = self.calc_risk_for_rib_object(object_name, int(nr_largest_elements), int(age),
                                                                        function_name, start_time, end_time)

        label_list_calc = []
        for i in range(0, risk_matrix.shape[0]):
            name = str(i) if i < 8 else "+ " + str(i)
            label_list_calc.append([{OutputStringForPlugins.TEXT: name + ":"}, {OutputStringForPlugins.TEXT: risk_matrix[i]}])

        d = {}
        for key in detail_risk_matrix.keys():
            ls = []
            for rows in range(len(detail_risk_matrix[key])):
                inner = []
                for cols in range(len(detail_risk_matrix[key][rows])):
                    inner.append({OutputStringForPlugins.TEXT: detail_risk_matrix[key][rows][cols]})
                ls.append(inner)
            d[key] = ls

        result_str = self.get_output_string(separator=";", object=object_name,
                                            nr_values_exceed=str(int(nr_largest_elements)), age=str(int(age)),
                                            sel_function=function_name,
                                            detail_risk_matrix=d, label_list_calc=label_list_calc, start_time=start_time,
                                            end_time=end_time)
        self.extract_script_calc_array_and_title(result_str)

    def get_output_string(self, separator=" ", object=None, limits = [], type_of_criteria=None, nr_values_exceed=None, age=None, sel_function=None,
                          detail_risk_matrix=None, label_list_calc=None, start_time=None, end_time=None):
        output_str = OutputStringForPlugins.OBJECT + separator + (
            self._gui.selection_ribs_object.get() if object is None else object) + "\n"
        if self._type_of_criteria is not None:
            output_str += OutputStringForPlugins.TYPE_OF_CRITERIA + separator + \
                          self._type_of_criteria[self._gui.selection_function.get() if sel_function is None
                                                                                    else sel_function] + "\n"
        if self._limits is not None:
            output_str += OutputStringForPlugins.LIMITS + separator + self._limits[self._gui.selection_function.get()
                                                                    if sel_function is None else sel_function] + "\n"
        output_str += OutputStringForPlugins.NR_EXCEEDING_EL + separator + (
            self._gui.nr_elements_exceed.get() if nr_values_exceed is None else nr_values_exceed) + "\n"
        output_str += OutputStringForPlugins.AGE + separator + (self._gui.age.get() if age is None else age) + "\n"
        output_str += OutputStringForPlugins.RISK_FUNCTION + separator + (
            self._gui.selection_function.get() if sel_function is None else sel_function) + "\n"
        output_str += OutputStringForPlugins.START_TIME + separator + str(
            self._gui.start_time.get() if start_time is None else start_time) + "\n"
        output_str += OutputStringForPlugins.END_TIME + separator + str(
            self._gui.end_time.get() if end_time is None else end_time) + "\n\n"

        # print header of the results
        # risk for Number of ribs to break
        output_str += str("#") + separator + "Prob(#)" + "\n"

        label_list_calc = self._gui.label_list_calc if label_list_calc == None else label_list_calc
        for key in label_list_calc:
            for index, entry in enumerate(key):
                if index != 0:
                    output_str += separator + str(entry[OutputStringForPlugins.TEXT]) + "\n"
                else:
                    output_str += str(entry[OutputStringForPlugins.TEXT])

        # risk details for each rib
        # 1) Header
        detail_risk_matrix = self._gui.label_list_details if detail_risk_matrix == None else detail_risk_matrix
        for key in detail_risk_matrix.keys():
            output_str += "\n"
            for row_list in detail_risk_matrix[key]:
                for index, entry in enumerate(row_list):
                    if index != 0:
                        output_str = output_str + separator + str(entry[OutputStringForPlugins.TEXT])
                    else:
                        output_str = output_str + str(entry[OutputStringForPlugins.TEXT])
                output_str += "\n"
        return output_str

    def plot_calculations_to_pdf(self, pp):
        self._logger.emit(LOGConstants.WARNING[0], "PDF Creation is not supported!")
