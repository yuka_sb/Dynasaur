import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import numpy as np
from constants import RibFractureConstants

from plugin import GUI, GUIInterface


class RibGUI(GUI, GUIInterface):
    def __init__(self, controller, root, defined_rib_objects, function_names):
        GUI.__init__(self, root, controller, width_definition=[[28, 20], [20, 28], [10, 10, 10, 12, 12, 12]],
                     pad_x=[(5, 10), (0, 5)], pad_y=[(3, 3), (10, 10)])

        self.__window = tk.Toplevel(root)
        self.__results_available = False

        # function
        self.__list_functions = list(function_names)
        self.selection_function = tk.StringVar()
        self.selection_function.set(self.__list_functions[0])

        # parts to select ... must be ribs
        self.__object_to_be_selected = defined_rib_objects
        self.selection_ribs_object = tk.StringVar()
        self.selection_ribs_object.set(self.__object_to_be_selected[0])

        # Nr of Elements that have to exceed the limit
        self.nr_elements_exceed = tk.StringVar()
        self.nr_elements_exceed.set(1)

        self.age = tk.StringVar()
        self.age.set(25)

        self.start_time = tk.StringVar()
        self.end_time = tk.StringVar()

        self.label_list_details = {}

        self._build_GUI()
        self.age.trace('w', self._gui_update)
        self.nr_elements_exceed.trace('w', self._gui_update)
        self.start_time.trace('w', self._gui_update)
        self.end_time.trace('w', self._gui_update)
        self._gui_update()

    def _build_GUI(self):
        self.__window.resizable(width=False, height=False)
        self.__window.title("Rib fractures")

        menu = tk.Menu(self.__window)
        self.__window.config(menu=menu)

        # File menu
        file_menu = tk.Menu(menu)
        file_menu.add_command(label="Copy Text", accelerator="Ctrl+C", command=self._gui_copy_text)
        self.__window.bind("<Control-c>", self._gui_copy_text)

        file_menu.add_command(label="Save Text", accelerator="Ctrl+S", command=self._gui_save_text)
        self.__window.bind("<Control-s>", self._gui_save_text)

        menu.add_cascade(label="File", menu=file_menu)

        ##############################################
        frame_margin = ttk.Frame(self.__window, width=sum(self._width_definition[0]))
        frame_margin.grid(row=0, column=0, padx=(10, 10), pady=(10, 10))

        global_row = 0
        frame_setup = ttk.Labelframe(frame_margin, text="Setup", width=sum(self._width_definition[0]))
        frame_setup.grid(row=global_row, column=0, pady=(0, 10))

        global_row += 1
        frame_result = ttk.Labelframe(frame_margin, text="Result", width=sum(self._width_definition[0]))
        frame_result.grid(row=global_row, column=0, pady=(0, 10))

        global_row += 1
        self.__button_details = ttk.Button(frame_margin, text="Show Details", width=sum(self._width_definition[0]) + 4,
                                           command=self.__show_detail_window)
        self.__button_details.grid(row=global_row, column=0, pady=self._pad_y[0])

        global_row += 1
        self.__button_export = ttk.Button(frame_margin, text="Export Data Set",
                                          width=sum(self._width_definition[0]) + 4, command=self.__gui_export_set)
        self.__button_export.grid(row=global_row, column=0, pady=self._pad_y[0])

        width = self._width_definition[0]
        ##############################################
        row = 0
        ttk.Label(frame_setup, text="Object:", anchor="e", width=width[0]).grid(row=row, column=0, padx=self._pad_x[0],
                                                                                pady=self._pad_y[0])
        combo_box_section = ttk.Combobox(frame_setup, state="readonly", width=(width[1] - 3),
                                         textvariable=self.selection_ribs_object,
                                         values=self.__object_to_be_selected)
        combo_box_section.bind("<<ComboboxSelected>>", self._gui_update)
        combo_box_section.grid(row=row, column=1, padx=self._pad_x[1])

        row += 1
        ttk.Label(frame_setup, text="Nr. Elements exceeding Limit:", anchor="e", width=width[0]).grid(row=row, column=0,
                                                                                                      padx=self._pad_x[
                                                                                                          0],
                                                                                                      pady=self._pad_y[
                                                                                                          0])
        ttk.Entry(frame_setup, textvar=self.nr_elements_exceed, justify='right', width=width[1]).grid(row=row, column=1,
                                                                                                      padx=self._pad_x[
                                                                                                          1])

        row += 1
        ttk.Label(frame_setup, text="Age:", anchor="e", width=width[0]).grid(row=row, column=0, padx=self._pad_x[0],
                                                                             pady=self._pad_y[0])
        ttk.Entry(frame_setup, textvar=self.age, justify='right', width=width[1]).grid(row=row, column=1,
                                                                                       padx=self._pad_x[1])

        row += 1
        label = ttk.Label(frame_setup, text="RiskFunction:", anchor="e", width=width[0])
        label.grid(row=row, column=0, padx=self._pad_x[0], pady=self._pad_y[0])
        combo_box_function = ttk.Combobox(frame_setup, state="readonly", width=(width[1] - 3),
                                          textvariable=self.selection_function,
                                          values=self.__list_functions)
        combo_box_function.bind("<<ComboboxSelected>>", self._gui_update)
        combo_box_function.grid(row=row, column=1, padx=self._pad_x[1])

        row += 1
        ttk.Label(frame_setup, text="Start time [s]:", anchor="e", width=width[0]).grid(row=row, column=0, padx=self._pad_x[0],
                                                                             pady=self._pad_y[0])
        ttk.Entry(frame_setup, textvar=self.start_time, justify='right', width=width[1]).grid(row=row, column=1,
                                                                                       padx=self._pad_x[1])
        row += 1
        ttk.Label(frame_setup, text="End time [s]:", anchor="e", width=width[0]).grid(row=row, column=0, padx=self._pad_x[0],
                                                                             pady=self._pad_y[0])
        ttk.Entry(frame_setup, textvar=self.end_time, justify='right', width=width[1]).grid(row=row, column=1,
                                                                                       padx=self._pad_x[1])
        row = 0
        for i in range(0, 9):
            name = str(i) if i < 8 else "+ " + str(i)
            self.label_list_calc.append([])
            self._create_label(frame_result, name, row, 0, 1)
            self._create_label(frame_result, "-", row, 1, 1)

            row += 1

    def __toggleGUIButtons(self, *args):

        # enable if the the limit is valid
        # extra condition: if the percentile should be calculated the percentile value has to be valid
        enable = self.nr_elements_exceed.get().isdigit() and int(self.nr_elements_exceed.get())

        state = "normal" if self.__results_available else "disable"
        self.__button_details.config(state=state)
        self.__button_export.config(state=state)

    def _gui_plot(self):
        # not used yet
        pass

    def _gui_update(self, *args):

        risk_matrix, detail_risk_matrices = self._controller.calc_risk_for_rib_object(self.selection_ribs_object.get(),
                                                                                      int(self.nr_elements_exceed.get()),
                                                                                      int(self.age.get()),
                                                                                      self.selection_function.get(),
                                                                                      self.start_time.get(),
                                                                                      self.end_time.get())
        self.__results_available = True

        for i in range(0, risk_matrix.shape[0]):
            self.label_list_calc[i][1]["text"] = risk_matrix[i]

        self.__gui_details_window(detail_risk_matrices, show=False)
        self.__toggleGUIButtons()

    def __show_detail_window(self, *args):
        _, detail_risk_matrices = self._controller.calc_risk_for_rib_object(self.selection_ribs_object.get(),
                                                                            int(self.nr_elements_exceed.get()),
                                                                            int(self.age.get()),
                                                                            self.selection_function.get(),
                                                                            self.start_time.get(),
                                                                            self.end_time.get())
        self.__gui_details_window(detail_risk_matrices, show=True)

    def __gui_details_window(self, detail_risk_matrices, show=True):
        self.label_list_details = {}
        if detail_risk_matrices == None:
            return

        # window to show the elements, which exceed the limit
        self.win = tk.Toplevel(self.__window)
        self.win.resizable(width=False, height=False)
        self.win.title("Rib Fractures Details")
        if not show:
            self.win.withdraw()

        # frames: 1. Left  Rib
        #         2. Right Rib
        frame_margin = ttk.Frame(self.win, width=sum(self._width_definition[0]))
        frame_margin.grid(row=0, column=0, padx=(10, 10), pady=(10, 10))

        global_row = 0
        if self.selection_ribs_object.get() == RibFractureConstants.LEFT_RIBS or self.selection_ribs_object.get() == RibFractureConstants.ALL_RIBS:
            frame_left_rib = ttk.Labelframe(frame_margin, text=RibFractureConstants.LEFT_RIBS,
                                            width=sum(self._width_definition[0]))
            frame_left_rib.grid(row=global_row, column=0, pady=(0, 10))
            self.__write_details_matrix(frame=frame_left_rib,
                                        risk_matrix=detail_risk_matrices[RibFractureConstants.LEFT_RIBS],
                                        what=RibFractureConstants.LEFT_RIBS)
            global_row += 1

        if self.selection_ribs_object.get() == RibFractureConstants.RIGHT_RIBS or self.selection_ribs_object.get() == RibFractureConstants.ALL_RIBS:
            frame_right_rib = ttk.Labelframe(frame_margin, text=RibFractureConstants.RIGHT_RIBS,
                                             width=sum(self._width_definition[0]))
            frame_right_rib.grid(row=global_row, column=0, pady=(0, 10))
            self.__write_details_matrix(frame=frame_right_rib,
                                        risk_matrix=detail_risk_matrices[RibFractureConstants.RIGHT_RIBS],
                                        what=RibFractureConstants.RIGHT_RIBS)

    def __write_details_matrix(self, frame, risk_matrix, what):
        self.label_list_details[what] = []

        # 1) write header
        ls_header = []
        row = 0
        lb = ttk.Label(frame, text="Part ID", anchor="c", width=self._width_definition[2][0])
        lb.grid(row=row, column=0, padx=self._pad_x[0], pady=self._pad_y[0])
        ls_header.append(lb)

        lb = ttk.Label(frame, text="Element ID", anchor="c", width=self._width_definition[2][1])
        lb.grid(row=row, column=1, padx=self._pad_x[0], pady=self._pad_y[0])
        ls_header.append(lb)

        lb = ttk.Label(frame, text="Max. Strain", anchor="c", width=self._width_definition[2][2])
        lb.grid(row=row, column=2, padx=self._pad_x[0], pady=self._pad_y[0])
        ls_header.append(lb)

        lb = ttk.Label(frame, text="Risk", anchor="c", width=self._width_definition[2][3])
        lb.grid(row=row, column=3, padx=self._pad_x[0], pady=self._pad_y[0])
        ls_header.append(lb)

        self.label_list_details[what].append(ls_header)

        # 2) write data
        for i in range(risk_matrix.shape[0]):
            ls = []
            for j in range(risk_matrix.shape[1]):
                color = "red" if str(risk_matrix[i, j]) == "nan" else "black"
                lb = ttk.Label(frame, text=str(risk_matrix[i, j]), foreground=color, anchor="c",
                               width=self._width_definition[2][j])
                lb.grid(row=i + 1, column=j)
                ls.append(lb)
            self.label_list_details[what].append(ls)

    def __gui_export_set(self):
        file = filedialog.asksaveasfile(mode='w', defaultextension=".key",
                                        filetypes=[("KEY file", "*.key")])
        if file is None:
            print("no file selected")
            return

        self._controller.save_max(file.name, self.selection_ribs_object.get(), int(self.age.get()),
                                  self.selection_function.get())
