# Welcome to Dynasaur

Dynasaur (*Dynamic simulation analysis of numerical results*) is an open source post processing tool for 
data analysis of FEM simulations. The project was launched by the [*Vehicle Safety Institute*](https://www.tugraz.at/institute/vsi/) at
*Graz University of Technology*.

A description of the how to install Dynasaur on your local machine can be 
found [here](https://gitlab.com/VSI-TUGraz/Dynasaur/wikis/installation).

You can also checkout the available source code from the gitlab repository and change it to your needs.
Feel free to contact us if you want to push your changes to the upstream.

We are continously working on the documentation of the project, which can be obtained from 
the [wiki](https://gitlab.com/VSI-TUGraz/Dynasaur/wikis/home).




## Licensing

Copyright (C) 2018 TU Graz


Dynasaur is a free software and published under the GNU GPL v3 (see the LICENSE file),
with the aim to encourage everyone to share their code as well.

You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 
of the License, or
(at your option) any later version.. You should have
received a copy of the GNU General Public License
along with the source code. If not, see <http://www.gnu.org/licenses/>.

Dynasaur is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the [acknowledgment](ACKNOWLEDGMENT.md) for details about the used external
libraries and funding within the Dynasaur software project.



## Authors

* Martin Schachner, VSI TU Graz
* Jakub Micorek, VSI TU Graz
* Peter Luttenberger, VSI TU Graz
* Robert Greimel, VSI TU Graz
* Corina Klug, VSI TU Graz




