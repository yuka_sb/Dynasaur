# CFC filter
#
# Author: Martin Schachner
#
# Implementation according to Crash Analysis Criteria Description, v2.1.1
# -----------------------------------------------------------------------
import math
import numpy as np
from scipy import signal


class CFC(object):
    # use different values for t < 0, mirror data here!
    def __init__(self, cfc, T):
        wd = 2. * math.pi * cfc * 2.0775
        wa = math.sin(wd * T / 2.) / math.cos(wd * T / 2.)

        self._a0 = wa * wa / (1. + math.sqrt(2.) * wa + wa * wa)
        self._a1 = 2. * self._a0
        self._a2 = self._a0
        self._b1 = -2. * (wa**2 - 1.) / (1. + math.sqrt(2.) * wa + wa**2)
        self._b2 = (-1. + math.sqrt(2.) * wa - wa**2) / (1. + math.sqrt(2.) * wa + wa**2)

    def filter(self, sampled_array, time, time_to_seconds_factor):

        # 10 ms for data extension!
        start_index = np.where(time > time[0] + .010 * time_to_seconds_factor)
        assert(len(start_index) > 0)
        start_index = start_index[0][0]

        # Data augmentation
        #  https://law.resource.org/pub/us/cfr/ibr/005/sae.j211-1.1995.pdf
        #
        # 1. pre-event data - mirror data on point of origin
        duplicated = np.transpose(np.transpose(sampled_array[:, 0:start_index])[::-1]) * -1
        extended_data = np.append(duplicated, sampled_array, axis=1)

        # 2. post-event data
        # magnitude method to extend the data array
        # subtract the last values from the
        # x[n+1] = 2 * x[n] - x[n-1]
        # x[n+2] = 2 * x[n] - x[n-2]
        # x[n+3] = 2 * x[n] - x[n-3]
        # etc
        end_value = np.repeat(np.transpose(extended_data[:, -1]), start_index, axis=0).reshape((sampled_array.shape[0], -1))
        last_n_data_values = sampled_array[:, -(start_index+1):-1]
        extension = 2 * end_value - last_n_data_values
        extended_data = np.append(extended_data, np.transpose(np.transpose(extension)[::-1]), axis=1)

        denominator = [1, -self._b1, -self._b2]
        numerator = [self._a0, self._a1, self._a2]
        output1 = signal.filtfilt(numerator, denominator, extended_data)

        return output1[:, start_index:-start_index]

