import configparser, os
from constants import ConfigStoreConstants


class ConfigStore:
    def __init__(self, projectname):

        self.__config = configparser.ConfigParser()

        if os.name != "posix":
            slash = "\\"
            homedir = "{}\\".format(os.getenv('APPDATA'))
        else:
            slash = '/'
            homedir = "{}/".format(os.path.expanduser("~"))

        self.__config_file_path = "{0}.{1}".format(homedir, projectname) + slash + 'pref.ini'
        if not os.path.isdir("{0}.{1}".format(homedir, projectname)):
            os.mkdir("{0}.{1}".format(homedir, projectname))

        # if file does not exist create it!
        if not os.path.isfile(self.__config_file_path):
            with open(self.__config_file_path, 'w+') as configfile:
                self.__config[ConfigStoreConstants.FILE] = {ConfigStoreConstants.BINOUT: '',
                                                            ConfigStoreConstants.DYNADEF: '',
                                                            ConfigStoreConstants.MADYMO_FILE: ''}
                self.__config.write(configfile)

        self.__config.read(self.__config_file_path)

    def store_binout_dir(self, value):
        self.__store_data(ConfigStoreConstants.FILE, ConfigStoreConstants.BINOUT, value)

    def store_madymo_file(self, value):
        self.__store_data(ConfigStoreConstants.FILE, ConfigStoreConstants.MADYMO_FILE, value)

    def store_dyna_def(self, value):
        self.__store_data(ConfigStoreConstants.FILE, ConfigStoreConstants.DYNADEF, value)

    def get_binout_dir(self):
        if self.__config.has_section(ConfigStoreConstants.FILE):
            if ConfigStoreConstants.BINOUT in self.__config[ConfigStoreConstants.FILE]:
                if os.path.isdir(self.__config[ConfigStoreConstants.FILE][ConfigStoreConstants.BINOUT]):
                    return self.__config[ConfigStoreConstants.FILE][ConfigStoreConstants.BINOUT]
        return ''

    def get_madymo_file(self):
        if self.__config.has_section(ConfigStoreConstants.FILE):
            if ConfigStoreConstants.MADYMO_FILE in self.__config[ConfigStoreConstants.FILE]:
                if os.path.isfile(self.__config[ConfigStoreConstants.FILE][ConfigStoreConstants.MADYMO_FILE]):
                    return self.__config[ConfigStoreConstants.FILE][ConfigStoreConstants.MADYMO_FILE]
        return ''

    def get_dynasaur_def(self):
        if self.__config.has_section(ConfigStoreConstants.FILE):
            if ConfigStoreConstants.DYNADEF in self.__config[ConfigStoreConstants.FILE]:
                if os.path.isfile(self.__config[ConfigStoreConstants.FILE][ConfigStoreConstants.DYNADEF]):
                    return self.__config[ConfigStoreConstants.FILE][ConfigStoreConstants.DYNADEF]
        return ''

    def __store_data(self, section, key, value):
        with open(self.__config_file_path, 'w') as configfile:
            if not self.__config.has_section(section):
                self.__config.add_section(section)
            self.__config.set(section, key, value)
            self.__config.write(configfile)
            configfile.close()
