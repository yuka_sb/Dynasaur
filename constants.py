from enum import Enum, auto
import numpy as np
import datetime

class GUIConstants(object):
    FILEMENU = "File"
    QUIT = "Quit"

    ##
    DATA = "Data explorer"
    PARTS = "Parts"
    ELEMENTS = "Elements"

    ##
    ANALYSISMENU = "Analysis"
    UNIVERSAL = "Universal"
    NODOUT = "Nodout Criteria"

    ##
    INJURYMENU = "HBM"
    CROSSSECTION = "Crosssection"
    CSDM = "CSDM"
    RIB = "Rib Fracture"

    ##
    HELPMENU = "?"
    MANUAL = "Manual"


class DummyInjuries(object):
    FUNC_DEF = {"Maximum": np.max,
                "Minimum": np.min}

class DefFileConstants(object):
    FUNC_CROSSSECTION = "crosssection"
    FUNC_CSDM = "csdm"
    FUNC_RIB = "rib"
    FUNC_UNIVERSAL = "universal"
    FUNC_KINEMATIC_CRITERIA = "kinematic_criteria"
    FUNC_DATA_VISUALISATION = "data_visualisation"


class ConfigStoreConstants(object):
    BINOUT = "binout_dir"
    DYNADEF = "dynadef"
    MADYMO_FILE = "madymo_file"
    FILE = "file"


class RibFractureConstants(object):
    LEFT_RIBS = "CHEST:Left Ribs"
    RIGHT_RIBS = "CHEST:Right Ribs"
    ALL_RIBS = "CHEST:Ribs Overall"

class IsommeConstants(object):
    CHANNELS = "Channels"
    INSTUMENTATION_STANDARD = "ISO 6487 (1987) / SAE J211 (MAR95)"

    MODEL_6YO = "6yo"
    MODEL_AM50 = "AM50"

    TIME = "Time"
    CONTACT_FORCE = "Contact Force"
    TRAJECTORY = "Trajectory"
    HEAD_COG = "Head COG"
    ENERGY = "Energy"
    ENERGIES = "Energies"
    ADDED_MASS = "Added Mass"
    TIMESTEP = "Timestep"
    GV = "GV"
    ENERGY_HBM = "Energy HBM"
    ADDED_MASS_HBM = "Added Mass HBM"
    HIT = "HIT"
    WAD = "WAD"

    PEDESTRIAN_GV = "pedestrian - GV"
    HEAD_GV = "head - GV"
    ARM_GV = "arm - GV"
    RIGHT_LEG_BUMPER = "right leg - bumper"
    TORSO_BUMPER = "torso - bumper"
    PEDESTRIAN_BONNET = "pedestrian - bonnet"
    PEDESTRIAN_BUMPER = "pedestrian - bumper"

    HC = "HC"
    C7 = "C7"
    T12 = "T12"
    AC = "AC"
    T8 = "T8"
    FER = "FER"
    ML = "ML"
    FEL = "FEL"
    MR = "MR"
    TOTAL_HOURGLASS_ENERGY_HBM = "total hourglass energy HBM"
    HBM = "HBM"

    RESULTANT = "resultant"
    RESULTANT_CF = "resultant CF"
    X_COORDINATE = "x-Coordinate"
    Z_COORDINATE = "z-Coordinate"
    Z_ACCELERATION = "z-acceleration"  # Split in 2?
    RESULTANT_ACCELERATION = "resultant acceleration"  # Split in 2?
    RESULTANT_VELOCITY = "resultant velocity"
    TOTAL_HOURGLASS_ENERGY = "total hourglass energy"
    TOTAL_INTERNAL_ENERGY = "total internal energy"
    TOTAL_ENERGY = "total energy"
    CONTACT_ENERGY = "contact energy"
    WHOLE_SETUP = "whole setup"

    NO_VALUE = "NOVALUE"


class UnitsConstants(object):
    TIME = "time"
    LENGTH = "length"
    WEIGHT = "weight"
    SECOND = "s"
    MILLISECOND = "ms"
    METER = "m"
    MILLIMETER = "mm"
    KILOGRAM = "kg"
    KNEWTON = "kN"
    VELOCITY = "m/s"
    TON = "t"
    ONE = 1
    THOUSAND = 1000
    WEIGHT_DEFAULT = 0.001
    JOULE = "J"

class TransducerConstants(object):
    COORDINATE = "Coordinate"
    FORCE = "Force"
    TIME = "Time"
    ACCELERATION = "Acceleration"
    VELOCITY = "Velocity"
    ENERGY = "Energy"
    MASS = "Mass"

class LOGConstants(object):
    ERROR = ("[ERROR]", "red")
    WARNING = ("[WARNING]", "magenta")
    READ_BINOUT = ("[READ BINOUT]", "blue")
    READ_MADYMO = ("[READ MADYMO]", "yellow")
    READ_VOLUME = ("[READ VOLUME]", "grey")
    READ_DYNASAUR_DEF = ("[READ DYNASAUR DEF]", "green")
    DATA_PLUGIN = ("[DATA]", "white")
    SCRIPT = ("[SCRIPT]", "cyan")
    INPUT = ("[USER INPUT]", "blue")

class MadymoConstants:
    CHANNEL_NAME = "channel_name"
    COMP = "COMP"
    Y_VALUES = "Y_VALUES"
    X_VALUES = "X_VALUES"
    IDS = "ids"
    SIGNALS = "signals"

class ScriptCommands(object):

    PLUGIN_UNIVERSAL = "PLUGIN_UNIVERSAL"
    PLUGIN_CSDM = "PLUGIN_CSDM"
    PLUGIN_CROSSSECTION = "PLUGIN_CROSSSECTION"
    PLUGIN_RIB = "PLUGIN_RIB"
    PLUGIN_CRITERIA = "PLUGIN_CRITERIA"
    PLUGIN_DATA_VISUALISATION = "PLUGIN_DATA_VISUALISATION"
    PLUGIN_DUMMY_EVAL = "PLUGIN_DUMMY_EVAL"

    TXT_REPORT = "TXT_REPORT"
    CSV_REPORT = "CSV_REPORT"
    PDF_REPORT = "PDF_REPORT"
    ISOMME_REPORT = "ISOMME_REPORT"

    BINOUT_FILE = "BINOUT_FILE"
    MADYMO_FILE = "MADYMO_FILE"
    DEF_FILE = "DEF_FILE"
    PARAMS_DEF = "PARAMS_DEF"
    PARAMS_VAR = "PARAMS_VAR"
    PARAMS_MERGE = "PARAMS_MERGE"
    PARAMS_CLEAR = "PARAMS_CLEAR"

    CLEAR = "CLEAR"

    DYNASAUR_JSON = "dynasaur_json"
    DIAGRAM_NAME = "diagram_name"
    CRITERIA = "criteria"


class FigureConstants(Enum):
    LEFT_UPPER_CORNER = auto()
    LEFT_LOWER_CORNER = auto()
    RIGHT_UPPER_CORNER = auto()
    RIGHT_LOWER_CORNER = auto()

class DefinitionConstants(object):
    CODE = "CODE"
    TITLE = "TITLE"
    UNIT = "UNIT"
    OBJECTS = "OBJECTS"
    DATA_VIS = "DATA VISUALIZATION"
    RISK_FUNCTION = "RISK FUNCTION"
    CRITERIA = "CRITERIA"


class TestConstants:
    ELOUT = "_element"
    DATA_VIS = "_data_vis"
    INJURY_CRIT = "_injury_crit"
    DEFORC = "_discrete"
    NODOUT = "_node"
    OBJECTS = "_objects"
    CONTACT = "_contact"
    RISK_FUN = "_riskfunction"
    SBTOUT = "_seatbelt"
    SECTION = "_section"
    TITLE = "_title"
    UNITS = "_units"

class ObjectConstants:
    OBJECT = "OBJECT"
    SECFORC = "secforc"
    NODE = "NODE"
    CONTACT = "CONTACT"
    DISCRETE = "DISCRETE"
    ELEMENT = "ELEMENT"
    SEAT_BELT = "SEAT_BELT"
    CROSS_SECTION = "CROSS_SECTION"
    ENERGY_PART = "ENERGY_PART"
    ENERGY_GLOBAL = "ENERGY_GLOBAL"

class ObjectConstantsForData:
    ELEMENT = "ELEMENT"
    CROSS_SECTION = "CROSS_SECTION"
    VOLUME = "VOLUME"
    DISCRETE = "DISCRETE"
    NODE = "NODE"
    SEAT_BELT = "SEAT_BELT"
    CONTACT = "CONTACT"
    MADYMO = "MADYMO"
    STRAIN = "STRAIN"
    STRESS = "STRESS"
    ENERGY_PART = "ENERGY_PART"
    ENERGY_GLOBAL = "ENERGY_GLOBAL"

class CodeType:
    MADYMO = "MADYMO"
    BINOUT = "LS_DYNA"

class JsonConstants:
    TYPE = "type"
    ID = "id"
    ID_UPPER_CASE = "ID"
    ID_RANGE = "id_range"
    NAME = "name"
    VALUE = "value"
    SECTIONS = "sections"
    SECTION = "section"
    FUNCTIONS = "functions"
    FUNCTION = "function"
    PLUGIN = "plugin"
    OBJECTS = "objects"
    CRITERIA = "criteria"
    DEFINITIONS = "definitions"
    DEFINITION = "definition"
    DCDF = "dcdf"
    CCDF = "ccdf"
    TYPE_OF_CRTITERIA = "type_of_criteria"
    PART_OF = "part_of"
    INFO = "info"
    HEADER_INFO = "header_info"
    CRITERIA_KINEMATIC = "kinematic"
    CRITERIA_LOAD = "load"
    CRITERIA_INJURY = "injury"
    CRITERIA_ENERGY = "energy"
    CRITERIA_METADATA = "metadata"
    CRITERIA_TIME = "time"
    CRITERIA_RISK = "risk"    
    PARAM = "param"
    ARRAY = "array"
    LIMITS = "limits"

class PluginsParamDictDef:
    DYNASAUR_JSON = "dynasaur_json"
    START_TIME = "start_time"
    END_TIME = "end_time"
    CRITERIA = "criteria"
    RISK_CURVE = 'risk_curve'
    SECTION = 'section'
    TYPE = 'type'
    CURVE = 'curve'
    OBJECT = 'object'
    LIMIT = 'limit'
    OBJECT_NAME = 'object_name'
    NR_LARGESR_EL = 'nr_largest_elements'
    AGE = 'age'
    FUNCTION_NAME = "function_name"
    X_NAME = "x_name"
    Y_NAME = "y_name"
    SELECTED_OBJECT = 'selected_object'
    SELECTION_STRAIN_STRESS = "selection_strain_stress"
    SELECTION_TENSION_COMPRESSION = "selection_tension_compression"
    INTEGRATION_POINT = "integration_point"
    PERCENTILE = "percentile"

class OutputStringForPlugins:
    VALUE = 'Value:'
    SECTION = "Section:"
    TYPE_OF_CRITERIA = "Type of criteria:"
    LIMITS = "Limits:"
    LIMIT = "Limit:"
    TYPE = "Type:"
    CRITERIA = "Criteria:"
    TEXT = "text"
    OBJECT = "Object:"
    RISK_FUNCTION = "RiskFunction:"
    START_TIME = "Start time:"
    END_TIME = "End time:"
    ALL_MAX = "allmax"
    FRAC_MAX = "fracmax"
    ALL_FRAC = "allfrac"
    NR_EXCEEDING_EL = "Nr exceeding Elements:"
    AGE = "Age:"
    PERCENTILE_LIMIT = "Percentile limit:"
    PERCENTILE = "Percentile:"
    ELEMENTS = "Elements:"
    SETUP = "Setup:\n"
    STRAIN_STRESS = "Strain/Stress:"
    TENSION_COMPRESSION = "Tension/Compression:"
    WORST_LIMIT = "Worst Limit:"
    MIDDLE_LIMIT = "Middle Limit:"
    BEST_LIMIT = "Best Limit:"
    INTERGATION_POINT = "Integration Point:"
    RESULTS = "\nResults:\n"


class ParsingReturns(Enum):
    MADYMO_FILE_DOES_NOT_EXIST = -56
    DEF_FILE_DOES_NOT_EXIST = -5
    NOT_ALL_BINOUTS_EXIST = -4
    PARM_MERGE_ERROR = -3
    DEPENDENCIES_ERROR = -2
    COMMAND_NOT_UNDERSTOOD = -1
    COMMENT = 0
    COMMAND_OK = 1

class DataPluginConstants:
    TIME = 'time'
    DIAGRAM_NAME = "diagram_name"
    X = "x"
    Y = "y"

class LoggerDefinitionFileValidator:
    ID_MUST_BE_DEFINITED = "ID has to be defied!"
    ID_IS_INTEGER_LIST = "ID has to be a list of integers!"
    DEFINITION_MISSING = "Definition is missing!"
    TITLE_MISSING = "Title value is missing!"
    STRING_TYPE = "Value type has to be a string!"
    TIME_MISSING = "Time has to be defined!"
    LENGTH_MISSING = "Length has to be defined!"
    WEIGHT_MISSING = "Weight has to be defined!"
    RISK_FUN_DEF = "Risk function has to be defined!"
    LIST_TYPE = "Value has to be defined as list!"
    PLUGIN_MISSING = "Plugin has to be defined!"
    LOWER_CASE = "Definition has to be be lowercase!"
    NAME_MISSING = "Name has to be defined!"
    DCDF_CCDF_MISSING = "Risk functions have to be defined as DCDF or CCDF!"
    OBJECT_MISSING = "Objects have to be defined in object!"
    TYPE_MISSING = "object type is missing!"
    PART_OF_MISSING = "\"part_of\" is missing! Definition name: "
    CRITERIA_MISSING = "Criteria has to be defined!"
    CRITERIA_DEFINITION_MISSING = "Criteria has to be a definitions{}"
    CRITERIA_TYPE_OF_MISSING = "Type of criteria has to be defined! Definition name: "
    CRITERIA_FUNCTION_MISSING = "function has to be defined! Definition name: "
    CRITERIA_TYPE_INCORRECT = "Type of criteria can be: load, kinematic or injury. Incorrect:"
    NOT_VALID_DEFINITION = "Invalid DEFINITION: "
    GITLAB_REFERENCE = " check out : https://gitlab.com/VSI-TUGraz/Dynasaur/wikis/definition-file"

class StandardFunctionsDefinition:
    HIC15 = "HIC_15"
    HIC36 = "HIC_36"
    BRIC = "BrIC"
    A3MS = "a3ms"
    NIJ = "nij"
    VC = "vc"
    TIBIA_INDEX = "tibia_index"
    CFC = "cfc"
    RES = "res"
    ABS = "abs"
    TRANSFORM_TO_ORIGIN = "transform2origin"
    MAX = "max"
    MIN = "min"
    MULT = "mult"
    SUB = "sub"
    ADD = "add"
    ABS_SUB = "abs_sub"
    ABS_ADD = "abs_add"
    DOUBLE_ABS_SUB_RES = "double_abs_sub_res"
    ABS_SUB_RES = "abs_sub_res"
    ABS_MAX = "absmax"
    ERROR_NOT_STANDARD_FUNCTION = " is not standard or user function"
    NIC = "NIC"
    VEL_NODE = "VEL_NODE" 
    VEL_NODE_MAX = "VEL_NODE_MAX"
    ABS_MAX_VALUE = "abs_max_value"
    THOR_Abdomen_Deflection = "THOR_Abdomen_Deflection"
    THOR_TCI_Rmax = "THOR_TCI_Rmax"
    uBRIC = "uBRIC"
    ALL_HIC15_AIS3 = "HIC15_AIS3"
    ALL_BRIC_AIS3 = "BRIC_AIS3"
    minimum_of_four = "minimum_of_four"
    maximum_of_four = "maximum_of_four"
    SIGMOID_RISK_1 = "SIGMOID_RISK_1"
    RES_ACC_from_XYZ_Velocities = "RES_ACC_from_XYZ_Velocities"
    central_differences = "central_differences"


class LoggerERROR:
    print_statements = {
        1: " is not supported as data type. Use ELEMENT, CROSS_SECTION, NODE, DISCRETE, SEAT_BELT, CONTACT",
        2: " not available in your binout",
        3: "No file to select",
        4: "JSON object issue: %s",
        5: " Definition does not contain key ",
        6: "In kinematic criteria plugin: Definition does not contain key ",
        7: "In DATA VISUALIZATION plugin: Definition does not contain key "
    }


class LoggerSCRIPT:
    print_statements = {
        1: " writing csv to ",
        2: " writing pdf to ",
        3: " writing channel files to ",
        4: " writing channel summary to ",
        5: " writing isomme to "
    }


class LoggerReadDynasaurDefinitionFile:
    READ = "read %s"
    DONE = "done"


class LoggerWARNING:
    print_statements = {
        1: " is not a valid definition!",
        2: "In DATA VISUALIZATION plugin: Definition does not contain header information for generating ISO-MME files. The information is preset to [" + "NOVALUE , " + datetime.date.today().strftime("%d/%m/%Y") + ", 1, 6yo]"
    }
