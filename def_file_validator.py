from six import string_types
from constants import LOGConstants, JsonConstants, ObjectConstants, DefinitionConstants, UnitsConstants, CodeType
from constants import LoggerDefinitionFileValidator as LD
import time


class TestDefinitionJSON:
    @staticmethod
    def __assert_objects(object, logger, code_type):
        assert JsonConstants.ID in object,  logger.emit(LOGConstants.ERROR[0], LD.ID_MUST_BE_DEFINITED)
        assert isinstance(object[JsonConstants.ID], list), logger.emit(LOGConstants.ERROR[0], LD.ID_IS_INTEGER_LIST)
        if object[JsonConstants.TYPE] != ObjectConstants.CONTACT and code_type != CodeType.MADYMO:
            assert all(isinstance(x, int) for x in object[JsonConstants.ID]),  \
                logger.emit(LOGConstants.ERROR[0], "ID must be list of integers")


    @staticmethod
    def test_def_json(json_objects, logger):
        code_type = CodeType.BINOUT
        for j_obj in json_objects:
            if DefinitionConstants.TITLE in j_obj:
                assert isinstance(j_obj[DefinitionConstants.TITLE], string_types), \
                    logger.emit(LOGConstants.ERROR[0],LD.STRING_TYPE)

            elif DefinitionConstants.UNIT in j_obj:
                assert UnitsConstants.TIME in j_obj[DefinitionConstants.UNIT], \
                    logger.emit(LOGConstants.ERROR[0], LD.TIME_MISSING)
                assert isinstance(j_obj[DefinitionConstants.UNIT][UnitsConstants.TIME], string_types), \
                    logger.emit(LOGConstants.ERROR[0], LD.STRING_TYPE)
                assert UnitsConstants.LENGTH in j_obj[DefinitionConstants.UNIT], \
                    logger.emit(LOGConstants.ERROR[0], LD.LENGTH_MISSING)
                assert isinstance(j_obj[DefinitionConstants.UNIT][UnitsConstants.LENGTH], string_types), \
                    logger.emit(LOGConstants.ERROR[0], LD.STRING_TYPE)
                assert UnitsConstants.WEIGHT in j_obj[DefinitionConstants.UNIT], \
                    logger.emit(LOGConstants.ERROR[0], LD.WEIGHT_MISSING)
                assert isinstance(j_obj[DefinitionConstants.UNIT][UnitsConstants.WEIGHT], string_types), \
                    logger.emit(LOGConstants.ERROR[0], LD.STRING_TYPE)

            elif DefinitionConstants.RISK_FUNCTION in j_obj:
                assert isinstance(j_obj[DefinitionConstants.RISK_FUNCTION], list), \
                    logger.emit(LOGConstants.ERROR[0], LD.LIST_TYPE)
                for fun in j_obj[DefinitionConstants.RISK_FUNCTION]:
                    assert JsonConstants.PLUGIN in fun, \
                        logger.emit(LOGConstants.ERROR[0], LD.PLUGIN_MISSING)
                    assert isinstance(fun[JsonConstants.PLUGIN], string_types), \
                        logger.emit(LOGConstants.ERROR[0], LD.STRING_TYPE)
                    assert fun[JsonConstants.PLUGIN].islower(),\
                        logger.emit(LOGConstants.ERROR[0], LD.LOWER_CASE)
                    assert JsonConstants.NAME in fun, \
                        logger.emit(LOGConstants.ERROR[0], LD.NAME_MISSING)
                    assert isinstance(fun[JsonConstants.NAME], string_types), \
                        logger.emit(LOGConstants.ERROR[0], LD.STRING_TYPE)
                    if JsonConstants.DCDF not in fun and JsonConstants.CCDF not in fun:
                        logger.emit(LOGConstants.ERROR[0], LD.DCDF_CCDF_MISSING)
                        raise IOError("Not defined")

            elif DefinitionConstants.CODE in j_obj:
                assert isinstance(j_obj[DefinitionConstants.CODE], string_types), \
                    logger.emit(LOGConstants.ERROR[0], LD.STRING_TYPE)
                if j_obj[DefinitionConstants.CODE] != CodeType.BINOUT and j_obj[DefinitionConstants.CODE] != CodeType.MADYMO:
                    assert False, logger.emit(LOGConstants.ERROR[0], "Not valid code type!")
                else:
                    code_type = j_obj[DefinitionConstants.CODE]

            elif DefinitionConstants.OBJECTS in j_obj:
                assert isinstance(j_obj[DefinitionConstants.OBJECTS], list), \
                    logger.emit(LOGConstants.ERROR[0], LD.LIST_TYPE)
                for obj in j_obj[DefinitionConstants.OBJECTS]:
                    assert JsonConstants.TYPE in obj, \
                        logger.emit(LOGConstants.ERROR[0], LD.TYPE_MISSING)
                    assert isinstance(obj[JsonConstants.TYPE], string_types)
                    assert JsonConstants.NAME in obj, \
                        logger.emit(LOGConstants.ERROR[0], LD.NAME_MISSING)
                    assert isinstance(obj[JsonConstants.NAME], string_types)
                    if obj[JsonConstants.TYPE] == ObjectConstants.NODE:
                        TestDefinitionJSON.__assert_objects(obj, logger, code_type)
                    elif obj[JsonConstants.TYPE] == ObjectConstants.CONTACT:
                        TestDefinitionJSON.__assert_objects(obj, logger, code_type)
                    elif obj[JsonConstants.TYPE] == ObjectConstants.ELEMENT:
                        TestDefinitionJSON.__assert_objects(obj, logger, code_type)
                    elif obj[JsonConstants.TYPE] == ObjectConstants.DISCRETE:
                        TestDefinitionJSON.__assert_objects(obj, logger, code_type)
                    elif obj[JsonConstants.TYPE] == ObjectConstants.SEAT_BELT:
                        TestDefinitionJSON.__assert_objects(obj, logger, code_type)
                    elif obj[JsonConstants.TYPE] == ObjectConstants.ENERGY_PART:
                        TestDefinitionJSON.__assert_objects(obj, logger, code_type)
                    elif obj[JsonConstants.TYPE] == ObjectConstants.ENERGY_GLOBAL:
                        TestDefinitionJSON.__assert_objects(obj, logger, code_type)
                    elif obj[JsonConstants.TYPE] == ObjectConstants.CROSS_SECTION:
                        TestDefinitionJSON.__assert_objects(obj, logger, code_type)
                    elif obj[JsonConstants.TYPE] == ObjectConstants.OBJECT:
                        if JsonConstants.ID not in obj and "id_range" not in obj:
                           raise IOError("Not defined")
                        if "id_range" in obj:
                            assert len(obj["id_range"]) > 1
                            #assert obj["id_range"][0] < obj["id_range"][1]
                    else:
                        logger.emit(LOGConstants.ERROR[0], "Not valid type: >>> " + obj[JsonConstants.TYPE] + " <<<")
                        time.sleep(0.5)
                        exit("Exiting")

            elif DefinitionConstants.DATA_VIS in j_obj:
                assert isinstance(j_obj[DefinitionConstants.DATA_VIS], list)
                for def_vis in j_obj[DefinitionConstants.DATA_VIS]:
                    assert JsonConstants.NAME in def_vis,  logger.emit(LOGConstants.ERROR[0], LD.NAME_MISSING)
                    assert isinstance(def_vis[JsonConstants.NAME], string_types), \
                        logger.emit(LOGConstants.ERROR[0], LD.STRING_TYPE)
                    assert JsonConstants.DEFINITION in def_vis
                    assert JsonConstants.PART_OF in def_vis[JsonConstants.DEFINITION], \
                        logger.emit(LOGConstants.ERROR[0],
                                    LD.PART_OF_MISSING + def_vis[JsonConstants.NAME])

            elif DefinitionConstants.CRITERIA in j_obj:
                assert isinstance(j_obj[DefinitionConstants.CRITERIA], list),  \
                    logger.emit(LOGConstants.ERROR[0], DefinitionConstants.CRITERIA + LD.LIST_TYPE)
                for criteria in j_obj[DefinitionConstants.CRITERIA]:
                    assert JsonConstants.NAME in criteria, \
                        logger.emit(LOGConstants.ERROR[0], JsonConstants.CRITERIA + LD.NAME_MISSING)
                    assert JsonConstants.DEFINITIONS in criteria, \
                        logger.emit(LOGConstants.ERROR[0], LD.CRITERIA_DEFINITION_MISSING)
                    assert JsonConstants.TYPE_OF_CRTITERIA in criteria[JsonConstants.DEFINITIONS], \
                        logger.emit(LOGConstants.ERROR[0], LD.CRITERIA_TYPE_OF_MISSING + criteria[JsonConstants.NAME])
                    assert JsonConstants.PART_OF in criteria[JsonConstants.DEFINITIONS], \
                        logger.emit(LOGConstants.ERROR[0], LD.PART_OF_MISSING + criteria[JsonConstants.NAME])
                    assert JsonConstants.FUNCTION in criteria[JsonConstants.DEFINITIONS], \
                        logger.emit(LOGConstants.ERROR[0], LD.CRITERIA_FUNCTION_MISSING + criteria[JsonConstants.NAME])
                    value_of_crit = criteria[JsonConstants.DEFINITIONS][JsonConstants.TYPE_OF_CRTITERIA]
                    if value_of_crit != JsonConstants.CRITERIA_KINEMATIC and value_of_crit != JsonConstants.CRITERIA_LOAD \
                            and value_of_crit != JsonConstants.CRITERIA_INJURY and value_of_crit != JsonConstants.CRITERIA_ENERGY \
                            and value_of_crit != JsonConstants.CRITERIA_METADATA and value_of_crit != JsonConstants.CRITERIA_RISK \
                            and value_of_crit != JsonConstants.CRITERIA_TIME:
                        assert False, \
                            logger.emit(LOGConstants.ERROR[0], LD.CRITERIA_TYPE_INCORRECT + value_of_crit)

            elif DefinitionConstants.CODE in j_obj:
                assert "MADYMO" == j_obj[DefinitionConstants.CODE] or "BINOUT" == j_obj[DefinitionConstants.CODE]
            else:
                assert False, logger.emit(LOGConstants.ERROR[0], LD.NOT_VALID_DEFINITION + str(j_obj) + LD.GITLAB_REFERENCE )
                #time.sleep(0.5)
                #exit("Exiting")
                ###