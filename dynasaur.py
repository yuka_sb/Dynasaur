"""
    Dynasaur,
    a post processing tool for the analysis of FE output files.

    Copyright (C) 2018  Graz University of Technology
"""

import os
import sys
import tkinter as tk
import tkinter.filedialog
import traceback
from lasso.dyna import Binout


import dynasaur_gui
from config_store import ConfigStore
from constants import LOGConstants, GUIConstants, DefFileConstants, ScriptCommands, CodeType
from Plugins.Analysis.universal_controller import UniversalController
from Plugins.Injury.crosssection_controller import CrosssectionController
from Plugins.Injury.csdm_controller import CSDMController
from Plugins.Injury.rib_controller import RibController
from Plugins.Injury.criteria_controller import CriteriaController
from Plugins.Data.data_plugin_controller import DataPluginController
from plugin import ScriptPluginRegistry
from dynasaur_definitions import DynasaurDefinitions
from script import Script
from Data.DataContainer import DataContainer
from logger import ConsoleLogger, WidgetLogger
import h5py

version = "v1.0"
projectname = "Dynasaur"


class Controller:
    def __show_universal_interface(self, event=None):
        risk_functions = self._dynasaur_definitions.get_defined_risk_functions(DefFileConstants.FUNC_DATA_VISUALISATION)
        units = self._dynasaur_definitions.get_units()
        code_type = self._dynasaur_definitions.get_code()
        UniversalController(root=self.__root, logger=self.__logger, risk_functions=risk_functions,
                            units=units, code_type=code_type, name=DefFileConstants.FUNC_UNIVERSAL)

    def __show_crosssection_interface(self, event=None):
        risk_functions = self._dynasaur_definitions.get_defined_risk_functions(DefFileConstants.FUNC_CROSSSECTION)
        units = self._dynasaur_definitions.get_units()
        code_type = self._dynasaur_definitions.get_code()
        if len(risk_functions) == 0:
            self.__logger.emit(LOGConstants.ERROR[0], "Risk function for crosssection not properly defined")
            return
        CrosssectionController(root=self.__root, logger=self.__logger, risk_functions=risk_functions, units=units,
                               code_type=code_type, name=DefFileConstants.FUNC_CROSSSECTION)

    def __show_csdm_interface(self, event=None):
        risk_functions = self._dynasaur_definitions.get_defined_risk_functions(DefFileConstants.FUNC_CSDM)
        units = self._dynasaur_definitions.get_units()
        code_type = self._dynasaur_definitions.get_code()
        if len(risk_functions) == 0:
            self.__logger.emit(LOGConstants.ERROR[0], "No Risk function defined. Check your definition file")
            return
        CSDMController(root=self.__root, logger=self.__logger, risk_functions=risk_functions, code_type=code_type,
                       units=units, name=DefFileConstants.FUNC_CSDM)

    def __show_kinematic_criteria(self, event=None):
        risk_functions = self._dynasaur_definitions.get_defined_risk_functions(DefFileConstants.FUNC_KINEMATIC_CRITERIA)
        units = self._dynasaur_definitions.get_units()
        code_type = self._dynasaur_definitions.get_code()
        #TODO: Function get_kinematic_criteria need a key to be called, check this, NO ERROR Handling
        CriteriaController(self._dynasaur_definitions.get_criteria(), risk_functions=risk_functions,
                           units=units, name=DefFileConstants.FUNC_KINEMATIC_CRITERIA)

    def __show_rib_fracture_interface(self, event=None):
        risk_functions = self._dynasaur_definitions.get_defined_risk_functions(DefFileConstants.FUNC_RIB)
        units = self._dynasaur_definitions.get_units()
        code_type = self._dynasaur_definitions.get_code()
        if len(risk_functions) == 0:
            self.__logger.emit(LOGConstants.ERROR[0], "No Risk function defined. Check your definition file")
            return
        RibController(root=self.__root, logger=self.__logger, risk_functions=risk_functions, units=units,
                      code_type=code_type, name=DefFileConstants.FUNC_RIB)

    def __show_data_object_interface(self, event=None):
        self.__logger.emit(LOGConstants.DATA_PLUGIN[0], "object interface")
        units = self._dynasaur_definitions.get_units()
        code_type = self._dynasaur_definitions.get_code()
        risk_functions = self._dynasaur_definitions.get_defined_risk_functions(DefFileConstants.FUNC_DATA_VISUALISATION)
        #TODO: MADYMO!
        DataPluginController(code_type=code_type, root=self.__root, logger=self.__logger, data_requirements=['ELEMENT'],
                             risk_functions=risk_functions, units=units, name=DefFileConstants.FUNC_DATA_VISUALISATION)

    def __select_def_file(self, event=None):
        dir = self.__config_store.get_dynasaur_def()
        def_file = tk.filedialog.askopenfilename(initialdir=dir, filetypes=[("All files", "*.def")])
        self.__config_store.store_dyna_def(def_file)
        self.__view.def_var.set(def_file)

    def __select_bin_dir(self, event=None):
        initdir = self.__config_store.get_binout_dir()
        binout_dir = os.path.dirname(
            tk.filedialog.askopenfilename(initialdir=initdir, filetypes=[("binout", "binout*")]))
        self.__config_store.store_binout_dir(binout_dir)
        self.__view.binout_dir.set(binout_dir)

    def __select_madymo_dir(self, event=None):
        initdir = self.__config_store.get_madymo_file()
        madymo_file = tk.filedialog.askopenfilename(initialdir=initdir, filetypes=[("All files", "*.h5")])
        self.__config_store.store_madymo_file(madymo_file)
        self.__view.madymo_file.set(madymo_file)

    def __toggle_menubar(self, from_where):
        self.__view.menu_bar.analysismenu.entryconfig(GUIConstants.UNIVERSAL, state="normal")
        self.__view.menu_bar.analysismenu.entryconfig(GUIConstants.DATA, state="normal")

        self.__view.menu_bar.analysismenu.entryconfig(GUIConstants.NODOUT, state="normal")

        self.__root.bind("<Key-F4>", self.__show_kinematic_criteria)
        self.__root.bind("<Key-F3>", self.__show_universal_interface)

        if isinstance(from_where, DynasaurDefinitions):
            self.__view.menu_bar.injurymenu.entryconfig(GUIConstants.CROSSSECTION, state="normal")
            self.__root.bind("<Key-F5>", self.__show_crosssection_interface)

            self.__view.menu_bar.injurymenu.entryconfig(GUIConstants.CSDM, state="normal")
            self.__root.bind("<Key-F6>", self.__show_csdm_interface)

            self.__view.menu_bar.injurymenu.entryconfig(GUIConstants.RIB, state="normal")
            self.__root.bind("<Key-F7>", self.__show_rib_fracture_interface)

    def __read_dyna_def(self, event=None):
        self._dynasaur_definitions.reset_definition()
        self._dynasaur_definitions.read_def(self.__config_store.get_dynasaur_def())
        if self.__root != None:
            self.__toggle_menubar(self._dynasaur_definitions)

    def __read_madymo_file(self, event=None):
        try:
            self._madymo_file = h5py.File(self.__config_store.get_madymo_file(), "r")

            madymo_keys = self._madymo_file["MODEL_0"].keys()

            self.__logger.emit(LOGConstants.READ_MADYMO[0], "available madymo data: ")
            self.__logger.emit(LOGConstants.READ_MADYMO[0], ' '.join(madymo_keys))
            self.__logger.emit(LOGConstants.READ_MADYMO[0], "madymo initialized!")

            DataContainer.init_data_source_nodout(self._madymo_file, self.__logger, self._dynasaur_definitions)

            return True
        except:
            tk.messagebox.showerror("Open Source File", "Failed to read file\n")
            return False
            exit("Problem occur while loading H5 file!")



    def __read_binout(self, event=None):

        # get binout files from directory! (extract all files which start with binout!)
        binout_filenames = [os.path.join(self.__config_store.get_binout_dir(), f) for f in
                            os.listdir(self.__config_store.get_binout_dir()) if f.startswith("binout")]

        if self.__root is not None:
            self.__root.tk.splitlist(binout_filenames)

        if len(binout_filenames) == 0:
            self.__logger.emit(LOGConstants.ERROR[0], "No binout data available in directory : " + self.__config_store.get_binout_dir())
            return False

        try:
            self.__binout = Binout(self.__config_store.get_binout_dir() + "/binout*")
            bin_ = Binout(self.__config_store.get_binout_dir() + "/binout*").read()
            binout_keys = self.__binout.read()

            self.__logger.emit(LOGConstants.READ_BINOUT[0], "available binout data: ")
            self.__logger.emit(LOGConstants.READ_BINOUT[0], ' '.join(binout_keys))
            self.__logger.emit(LOGConstants.READ_BINOUT[0], "binout initialized!")

            DataContainer.init_all_data_sources(self.__binout, self.__logger, self._dynasaur_definitions, self.__config_store.get_binout_dir())

            # 2) update what should be shown in the GUI if data is available!
            if self.__root is not None:
                self.__toggle_menubar(self.__binout)
            return True

        except:
            print((traceback.format_exc()))
            tk.messagebox.showerror("Open Source File", "Failed to read file\n'%s'" % binout_filenames)
            return False

    def __execute_command(self, command):
        if command[0] == ScriptCommands.DEF_FILE:
            self.__config_store.store_dyna_def(command[1])
            self.__read_dyna_def()

        elif command[0] == ScriptCommands.BINOUT_FILE:
            self.__config_store.store_binout_dir(command[1])
            if not self.__read_binout():
                return False
            self.__new_binout = True

        elif command[0] == ScriptCommands.MADYMO_FILE:
            self.__config_store.store_madymo_file(command[1])
            if self.__read_madymo_file():
                self.__new_madymo_file = True

        elif command[0] in [ScriptCommands.PLUGIN_CROSSSECTION, ScriptCommands.PLUGIN_UNIVERSAL, ScriptCommands.PLUGIN_CSDM,
                            ScriptCommands.PLUGIN_RIB, ScriptCommands.PLUGIN_CRITERIA, ScriptCommands.PLUGIN_DATA_VISUALISATION]:
            if self._plugin_registry.get_plugin_controller(command[0]) is None:
                self._plugin_registry.register(command[0])

            self._controller_script = self._plugin_registry.get_plugin_controller(command[0])
            self._controller_script.init_plugin_data(update=self.__new_binout)
            self.__new_binout = False

            param_dict = self._dynasaur_definitions.get_param_dict_from_script_command(command)

            if not self._controller_script.check_if_params_are_valid(param_dict=param_dict):
                self.__logger.emit(LOGConstants.ERROR[0], "Parameter for calculation of " + command[0] + " are not valid!")

            self._controller_script.calculate_and_store_results(param_dict=param_dict)

        elif command[0] == ScriptCommands.PDF_REPORT:
            self._controller_script.write_PDF(pdf_file_dir=command[1])

        elif command[0] == ScriptCommands.CSV_REPORT:
            self._controller_script.write_CSV(csv_file_dir=command[1])
        elif command[0] == ScriptCommands.ISOMME_REPORT:
            self._controller_script.write_ISOMME(self._dynasaur_definitions)
        elif command[0] == ScriptCommands.CLEAR:
            self._controller_script = None

        return True


    def __show_manual(self, event=None):
        import webbrowser
        url_wiki = "https://gitlab.com/VSI-TUGraz/dynasaur/wikis/home"
        webbrowser.open_new(url_wiki)

    def run(self):
        # open GUI
        if self.__script_filename is None:
            self.__root.title("Dynasaur " + version)
            self.__root.resizable(False, False)
            self.__root.deiconify()
            self.__root.mainloop()
        else:
            execution_ok = True
            while execution_ok:
                command = self.__script.get_next_command()
                if command is None:
                    return
                execution_ok &= self.__execute_command(command)

    def __init__(self, script_filename):
        self.__script_filename = script_filename
        self.__root = None
        if self.__script_filename is not None:
            self.__config_store = ConfigStore(projectname)
            self.__logger = ConsoleLogger()
            self._dynasaur_definitions = DynasaurDefinitions(self.__logger)
            self.__new_binout = False
            self.__script = Script(script_filename, self.__logger)
            self._plugin_registry = ScriptPluginRegistry(logger=self.__logger, dyna_def=self._dynasaur_definitions)
            self._controller_script = None
        else:
            self.__root = tk.Tk()
            #self.__root.iconbitmap('img/TUG.svg')
            self.__config_store = ConfigStore(projectname)

            self.__view = dynasaur_gui.View(self.__root, self.__config_store.get_binout_dir(),
                                            self.__config_store.get_dynasaur_def(), self.__config_store.get_madymo_file(),
                                            self.__read_binout, self.__read_madymo_file, self.__read_dyna_def)
            self.__logger = WidgetLogger(self.__view.log_text, self.__root)
            self._dynasaur_definitions = DynasaurDefinitions(self.__logger)

            # add GUI functionality
            self.__view.bin_sel_btn.bind("<Button>", self.__select_bin_dir)
            self.__view.mad_sel_btn.bind("<Button>", self.__select_madymo_dir)
            self.__view.def_sel_btn.bind("<Button>", self.__select_def_file)

            self.__root.bind("<Key-F1>", self.__show_manual)
            # analysis menu
            self.__view.menu_bar.analysismenu.entryconfig(GUIConstants.UNIVERSAL, state="disabled",
                                                          command=self.__show_universal_interface, accelerator='F3')
            self.__view.menu_bar.analysismenu.entryconfig(GUIConstants.NODOUT, state="disabled",
                                                          command=self.__show_kinematic_criteria, accelerator='F4')
            self.__view.menu_bar.analysismenu.entryconfig(GUIConstants.DATA, state="disabled",
                                                          command=self.__show_data_object_interface)

            # hbm menu
            self.__view.menu_bar.injurymenu.entryconfig(GUIConstants.CROSSSECTION, state="disabled",
                                                        command=self.__show_crosssection_interface, accelerator='F5')
            self.__view.menu_bar.injurymenu.entryconfig(GUIConstants.CSDM, state="disabled",
                                                        command=self.__show_csdm_interface, accelerator='F6')
            self.__view.menu_bar.injurymenu.entryconfig(GUIConstants.RIB, state="disabled",
                                                        command=self.__show_rib_fracture_interface, accelerator='F7')

            # help/? menu
            self.__view.menu_bar.helpmenu.entryconfig(GUIConstants.MANUAL, state="normal", command=self.__show_manual,
                                                      accelerator='F1')


if __name__ == '__main__':
    filename = None

    if len(sys.argv) > 1:
        filename = sys.argv[1]
        if not os.path.isfile(filename):
            print("[CALCULATION ABORTED]: Script File does not exist!")

    c = Controller(script_filename=filename)
    c.run()
