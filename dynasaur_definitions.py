from ls_object import LSObject
from constants import LOGConstants, DefFileConstants, ScriptCommands, UnitsConstants, DefinitionConstants, ObjectConstants, JsonConstants, LoggerERROR, LoggerWARNING, CodeType
from constants import LoggerReadDynasaurDefinitionFile as LD
from def_file_validator import TestDefinitionJSON
from unit import Units
import time
import datetime


class DynasaurDefinitions:
    # create the object container
    def __init__(self, logger):
        self._title = ''
        self._objects = {}
        self._logger = logger
        self._code = CodeType.BINOUT
        self._info = []

        # reset section, function and units dictionaries
        self._reset_sec_func_units()

    def get_param_dict_from_script_command(self, script_command):
        if script_command[0] in [ScriptCommands.PLUGIN_CROSSSECTION, ScriptCommands.PLUGIN_UNIVERSAL, ScriptCommands.PLUGIN_CSDM, ScriptCommands.PLUGIN_RIB]:
            return script_command[1]

        elif script_command[0] == ScriptCommands.PLUGIN_DATA_VISUALISATION:
            d = script_command[1]
            d[ScriptCommands.DYNASAUR_JSON] = self.get_data_vis(script_command[1][ScriptCommands.DIAGRAM_NAME])
            return d

        elif script_command[0] == ScriptCommands.PLUGIN_CRITERIA:
            d = script_command[1]
            d[ScriptCommands.DYNASAUR_JSON] = self.get_criteria(script_command[1][ScriptCommands.CRITERIA])
            return d

    def _reset_sec_func_units(self):
        self._cross_sections = {}
        self._node = {}
        self._contact = {}
        self._discrete = {}
        self._element = {}
        self._seatbelt = {}
        self._energy_part = {}
        self._energy_global = {}
        self._riskfunction = {DefFileConstants.FUNC_CROSSSECTION: {}, DefFileConstants.FUNC_CSDM: {},
                              DefFileConstants.FUNC_RIB: {}, DefFileConstants.FUNC_UNIVERSAL: {},
                              DefFileConstants.FUNC_KINEMATIC_CRITERIA: {},
                              DefFileConstants.FUNC_DATA_VISUALISATION: {}}

        self._criteria = {}
        self._data_vis = {}
        self._units = Units()

    def reset_definition(self):
        d = {}
        name = "Dynasaur Everything"
        if name in self._objects.keys():
            d[name] = self._objects[name]
        self._objects = d

        self._reset_sec_func_units()

    def get_units(self):
        return self._units

    def get_criteria(self, key):
        if key not in self._criteria.keys() or len(self._criteria) == 0:
            self._logger.emit(LOGConstants.ERROR[0], LoggerERROR.print_statements[6] + "\"" + key + "\"")
            #TODO: If Dynasaur used with script file exit() requested
            #time.sleep(.5)
            exit("Exiting, check your def file")
            return
        return self._criteria[key]

    def get_data_vis(self, key):
        if key not in self._data_vis.keys() or len(self._data_vis) == 0:
            self._logger.emit(LOGConstants.ERROR[0], LoggerERROR.print_statements[7] + "\"" + key + "\"")
            exit("Exiting, check your def file")
        return self._data_vis[key]

    def get_code(self):
        return self._code

    def get_ids_from_name(self, name, data_container, plugin_name):

        d = {}
        if data_container == "nodout":
            d = self._node
        elif data_container == "rcforc":
            d = self._contact
        elif data_container == "deforc":
            d = self._discrete
        elif data_container == "elout":
            d = self._element
        elif data_container == ObjectConstants.SECFORC:
            d = self._cross_sections
        elif data_container == "sbtout":
            d = self._seatbelt
        elif data_container == "matsum":
            d = self._energy_part
        elif data_container == "glstat":
            d = self._energy_global

        if name not in d.keys() or len(d) == 0:
            self._logger.emit(LOGConstants.ERROR[0], "In " + plugin_name + LoggerERROR.print_statements[5] + "\"" + name + "\"")
            time.sleep(.5)
            exit("Exiting, check your def file")

            return

        return d[name]

    def get_section_names_containing_ids(self, secforc_ids):
        sections = []
        for key, ids in self._cross_sections.items():
            for id_temp in secforc_ids:
                if id_temp in ids:
                    sections.append(key)
                    break
        return sections

    def get_defined_sections_containing_ids(self, secforc_ids):
        sections = {}
        for key, ids in self._cross_sections.items():
            for id_temp in secforc_ids:
                if id_temp in ids:
                    if key not in sections:
                        sections[key] = []
                    sections[key].append(id_temp)
        return sections

    def get_defined_objects_containing_parts(self, part_ids):
        list_defined_objects = []
        for tmp_object_name in sorted(self._objects):
            tmp_part_ids = list(self._objects[tmp_object_name].get_parts())
            intersection = [val for val in tmp_part_ids if val in part_ids]
            if len(intersection) != 0:
                list_defined_objects.append(tmp_object_name)

        if len(list_defined_objects) == 0:
            list_defined_objects.append("")

        return list_defined_objects

    def get_defined_risk_functions(self, plugin_name):
        d = self._riskfunction[plugin_name]
        return {key: d[key][JsonConstants.FUNCTION] for key in d.keys() if d[key] != None}

    def get_defined_limits(self, plugin_name):
        d = self._riskfunction[plugin_name]
        return {key: d[key][JsonConstants.LIMITS] for key in d.keys() if d[key] != None and JsonConstants.LIMITS in d[key]}

    def get_defined_type_of_criteria(self, plugin_name):
        d = self._riskfunction[plugin_name]
        return {key: d[key][JsonConstants.TYPE_OF_CRTITERIA] for key in d.keys() if d[key] != None
                and JsonConstants.TYPE_OF_CRTITERIA in d[key]}

    def get_defined_objects_containing_all_parts(self, part_ids):
        list_defined_objects = []
        for tmp_object_name in sorted(self._objects):
            tmp_part_ids = list(self._objects[tmp_object_name].get_parts())
            intersection = [val for val in tmp_part_ids if val in part_ids]
            if len(tmp_part_ids) == len(intersection):
                list_defined_objects.append(tmp_object_name)

        if len(list_defined_objects) == 0:
            list_defined_objects.append("")

        return list_defined_objects

    def _get_all_data_types_from_json(self, d, ls):
        for key in d.keys():
            if key is None:
                continue
            if isinstance(d[key], dict):
                data_type = self._get_all_data_types_from_json(d[key], ls)
                if data_type is not None and data_type not in ls:
                    ls.append(data_type)
            elif JsonConstants.TYPE in d.keys():
                return d[JsonConstants.TYPE]

    def get_required_datatypes_for_injury_risks(self):
        ls = []
        self._get_all_data_types_from_json(self._criteria, ls)
        return ls

    def get_required_datatypes_for_data_visualisation(self):
        ls = []
        self._get_all_data_types_from_json(self._data_vis, ls)
        return ls


    def get_parts_by_object_containing_part_ids(self, tmp_object, part_ids):
        return [val for val in part_ids if val in self._objects[tmp_object].get_parts()]

    def get_info(self):
        return self._info

    # param is a list of part_ids
    def define_dynasaur_everything(self, part_ids):
        # create an Everything object

        name = "Dynasaur Everything"
        self._objects[name] = LSObject(name)

        # only store ids not the entire dictonary would cause side effects
        #self._objects[name].parts = part_ids
        self._objects[name].set_parts(part_ids)

    def __set_contact(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._contact[name] = json_object[JsonConstants.ID]

    def __set_seatbelt(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._seatbelt[name] = json_object[JsonConstants.ID]

    def __set_discrete(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._discrete[name] = json_object[JsonConstants.ID]

    def __set_element(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._element[name] = json_object[JsonConstants.ID]

    def __set_node(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._node[name] = json_object[JsonConstants.ID]

    def __set_energy_part(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._energy_part[name] = json_object[JsonConstants.ID]

    def __set_energy_global(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._energy_global[name] = json_object[JsonConstants.ID]

    def __parse_objects(self, json_objects):
        for json_object in json_objects[DefinitionConstants.OBJECTS]:
            if json_object[JsonConstants.TYPE] == ObjectConstants.OBJECT:
                self.__set_object(json_object)
            elif json_object[JsonConstants.TYPE] == ObjectConstants.NODE:
                self.__set_node(json_object)
            elif json_object[JsonConstants.TYPE] == ObjectConstants.ELEMENT:
                self.__set_element(json_object)
            elif json_object[JsonConstants.TYPE] == ObjectConstants.CONTACT:
                self.__set_contact(json_object)
            elif json_object[JsonConstants.TYPE] == ObjectConstants.DISCRETE:
                self.__set_discrete(json_object)
            elif json_object[JsonConstants.TYPE] == ObjectConstants.SEAT_BELT:
                self.__set_seatbelt(json_object)
            elif json_object[JsonConstants.TYPE] == ObjectConstants.CROSS_SECTION:
                self.__set_sections(json_object)
            elif json_object[JsonConstants.TYPE] == ObjectConstants.ENERGY_PART:
                self.__set_energy_part(json_object)
            elif json_object[JsonConstants.TYPE] == ObjectConstants.ENERGY_GLOBAL:
                self.__set_energy_global(json_object)
            else:
                self._logger.emit(LOGConstants.WARNING[0], json_object[JsonConstants.TYPE] + LoggerWARNING.print_statements[1])

    def __set_object(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._objects[name] = LSObject(name)
        if JsonConstants.ID in json_object:
            for part in json_object[JsonConstants.ID]:
                self._objects[name].add_part(part)

        if JsonConstants.ID_RANGE in json_object:
            for i in range(json_object[JsonConstants.ID_RANGE][0], json_object[JsonConstants.ID_RANGE][-1]):
                self._objects[name].add_part(i)

        #TODO: Check if we need elem?
        elif "elem" in json_object:
            for elem in json_object["elem"]:
                self._objects[name].add_element(elem)

        elif "elem_range" in json_object:
            for i in range(json_object["elem_range"][0], json_object["elem_range"][1]):
                self._objects[name].add_element(i)

    def __set_riskfunctions(self, json_object):
        for section in json_object[DefinitionConstants.RISK_FUNCTION]:
            name = section[JsonConstants.NAME]
            plugin = section[JsonConstants.PLUGIN]
            self._riskfunction[plugin][name] = {}
            if JsonConstants.DCDF in section:
                self._riskfunction[plugin][name][JsonConstants.FUNCTION] = section[JsonConstants.DCDF]
            elif JsonConstants.CCDF in section:
                self._riskfunction[plugin][name][JsonConstants.FUNCTION] = section[JsonConstants.CCDF]
            if JsonConstants.LIMITS in section:
                self._riskfunction[plugin][name][JsonConstants.LIMITS] = str(section[JsonConstants.LIMITS])
            if JsonConstants.TYPE_OF_CRTITERIA in section:
                self._riskfunction[plugin][name][JsonConstants.TYPE_OF_CRTITERIA] = section[JsonConstants.TYPE_OF_CRTITERIA]

    def __set_sections(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._cross_sections[name] = json_object[JsonConstants.ID]

    def __set_madymo(self, json_object):
        name = json_object[JsonConstants.NAME]
        self._madymo[name] = json_object[JsonConstants.ID]

    def __set_title(self, json_object):
        self._title = json_object[DefinitionConstants.TITLE]

    def __set_unit(self, json_object):
        for key_name in json_object[DefinitionConstants.UNIT]:
            if key_name == UnitsConstants.TIME:
                if json_object[DefinitionConstants.UNIT][key_name] == UnitsConstants.SECOND:
                    self._units.set_time(UnitsConstants.ONE)
                elif json_object[DefinitionConstants.UNIT][key_name] == UnitsConstants.MILLISECOND:
                    self._units.set_time(UnitsConstants.THOUSAND)
            elif key_name == UnitsConstants.LENGTH:
                if json_object[DefinitionConstants.UNIT][key_name] == UnitsConstants.METER:
                    self._units.set_length(UnitsConstants.ONE)
                elif json_object[DefinitionConstants.UNIT][key_name] == UnitsConstants.MILLIMETER:
                    self._units.set_length(UnitsConstants.THOUSAND)
            elif key_name == UnitsConstants.WEIGHT:
                if json_object[DefinitionConstants.UNIT][key_name] == UnitsConstants.KILOGRAM:
                    self._units.set_weight(UnitsConstants.ONE)
                elif json_object[DefinitionConstants.UNIT][key_name] == UnitsConstants.TON:
                    self._units.set_weight(UnitsConstants.WEIGHT_DEFAULT)

    def __set_criteria(self, json_object):
        for criteria in json_object[DefinitionConstants.CRITERIA]:
            body_part = criteria[JsonConstants.DEFINITIONS][JsonConstants.PART_OF]
            self._criteria[body_part + "_" +criteria[JsonConstants.NAME]] = criteria[JsonConstants.DEFINITIONS]

    def __set_diagram_visualisation(self, json_object):
        for definition in json_object[DefinitionConstants.DATA_VIS]:
            body_part = definition[JsonConstants.DEFINITION][JsonConstants.PART_OF]
            self._data_vis[body_part + "_" + definition[JsonConstants.NAME]] = definition[JsonConstants.DEFINITION]

    def __set_code(self, json_object):
        self._code = json_object[DefinitionConstants.CODE]

    def __set_info(self):
        info = []
        for key in self._data_vis.keys():
            if JsonConstants.INFO in self._data_vis[key].keys():
                if len(self._data_vis[key][JsonConstants.INFO].split(":")) == 3:
                    info.append(self._data_vis[key][JsonConstants.INFO])
                elif len(self._data_vis[key][JsonConstants.INFO].split(":")) == 2:
                    self._data_vis[key][JsonConstants.INFO].append(":")
                    info.append(self._data_vis[key][JsonConstants.INFO])
                elif len(self._data_vis[key][JsonConstants.INFO].split(":")) > 3:
                    self._data_vis[key][JsonConstants.INFO] = "::"
                    info.append(self._data_vis[key][JsonConstants.INFO])
                else:
                    self._data_vis[key][JsonConstants.INFO].append(":")
                    info.append(self._data_vis[key][JsonConstants.INFO])
            else:
                info.append(None)
        self._info = info

    def read_def(self, fn):
        # functions here!
        import json
        self._logger.emit(LOGConstants.READ_DYNASAUR_DEF[0], LD.READ % fn)

        with open(fn) as fd:
            try:
                json_objects = json.load(fd)
            except ValueError as e:
                self._logger.emit(LOGConstants.ERROR[0], LoggerERROR.print_statements[4] % e)
                exit("Exiting - Invalid JSON file") #Because Mutant, if GUI "exit()" comment
                return

        TestDefinitionJSON.test_def_json(json_objects, self._logger)

        for j_object in json_objects:
            if DefinitionConstants.TITLE in j_object:
                self.__set_title(j_object)
            elif DefinitionConstants.UNIT in j_object:
                self.__set_unit(j_object)
            elif DefinitionConstants.CRITERIA in j_object:
                self.__set_criteria(j_object)
            elif DefinitionConstants.DATA_VIS in j_object:
                self.__set_diagram_visualisation(j_object)
                self.__set_info()
            elif DefinitionConstants.RISK_FUNCTION in j_object:
                self.__set_riskfunctions(j_object)
            elif DefinitionConstants.OBJECTS in j_object:
                self.__parse_objects(j_object)
            elif DefinitionConstants.CODE in j_object:
                self.__set_code(j_object)
            else:
                self._logger.emit(LOGConstants.WARNING[0], j_object[DefinitionConstants.DEFINITION] + LoggerWARNING.print_statements[1])
                return

        self._logger.emit(LOGConstants.READ_DYNASAUR_DEF[0], LD.DONE)
