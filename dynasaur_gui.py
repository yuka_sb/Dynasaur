import tkinter as tk
import os

from constants import GUIConstants, LOGConstants, CodeType


class CustomText(tk.Text):
    '''A text widget with a new method, highlight_pattern()

    example:

    text = CustomText()
    text.tag_configure("red", foreground="#ff0000")
    text.highlight_pattern("this should be red", "red")

    The highlight_pattern method is a simplified python
    version of the tcl code at http://wiki.tcl.tk/3246
    '''

    def __init__(self, *args, **kwargs):
        tk.Text.__init__(self, *args, **kwargs)

    def highlight_pattern(self, pattern, tag, start="1.0", end="end",
                          regexp=False):
        '''
        Apply the given tag to all text that matches the given pattern

        If 'regexp' is set to True, pattern will be treated as a regular
        expression according to Tcl's regular expression syntax.
        '''
        start = self.index(start)
        end = self.index(end)
        self.mark_set("matchStart", start)
        self.mark_set("matchEnd", start)
        self.mark_set("searchLimit", end)

        count = tk.IntVar()
        while True:
            index = self.search(pattern, "matchEnd", "searchLimit", count=count, regexp=regexp)
            if index == "": break
            if count.get() == 0: break  # degenerate pattern which matches zero-length strings
            self.mark_set("matchStart", index)
            self.mark_set("matchEnd", "%s+%sc" % (index, count.get()))
            self.tag_add(tag, "matchStart", "matchEnd")


class MenuBar():

    def __init__(self, root):
        self.mainmenu = tk.Menu(root)
        root.config(menu=self.mainmenu)

        self.datamenu = tk.Menu(self.mainmenu)

        self.analysismenu = tk.Menu(self.mainmenu)
        self.analysismenu.add_command(label=GUIConstants.UNIVERSAL)
        self.analysismenu.add_command(label=GUIConstants.NODOUT)
        self.analysismenu.add_command(label=GUIConstants.DATA)
        self.mainmenu.add_cascade(label=GUIConstants.ANALYSISMENU, menu=self.analysismenu)

        self.injurymenu = tk.Menu(self.mainmenu)
        self.injurymenu.add_command(label=GUIConstants.CROSSSECTION)
        self.injurymenu.add_command(label=GUIConstants.CSDM)
        self.injurymenu.add_command(label=GUIConstants.RIB)
        self.mainmenu.add_cascade(label=GUIConstants.INJURYMENU, menu=self.injurymenu)

        self.helpmenu = tk.Menu(self.mainmenu)
        self.helpmenu.add_command(label=GUIConstants.MANUAL)
        self.mainmenu.add_cascade(label=GUIConstants.HELPMENU, menu=self.helpmenu)


class View():
    def __build_gui(self, root, read_binout_callback, read_dyna_def_callback, read_madymo_callback, binout_dir, madymo_file, dyna_file):
        self.menu_bar = MenuBar(root)
        frame = tk.Frame(root, width=sum(self.__width_definition[0]))
        frame.grid(row=0, column=0, padx=(10, 10), pady=(10, 10))

        ################################################################################################################
        ### shell for logging
        row = 0
        frame_shell = tk.ttk.Labelframe(frame, text="Log", width=sum(self.__width_definition[0]))
        frame_shell.grid(row=row, column=0)

        self.log_text = CustomText(frame_shell, borderwidth=2, relief="groove", state='disabled',
                                   width=(sum(self.__width_definition[0])))
        self.log_text.grid(row=0, column=0, padx=(5, 5), pady=(5, 5))

        self.log_text.tag_configure(LOGConstants.ERROR[0], foreground=LOGConstants.ERROR[1])
        self.log_text.tag_configure(LOGConstants.READ_BINOUT[0], foreground=LOGConstants.READ_BINOUT[1])
        self.log_text.tag_configure(LOGConstants.READ_DYNASAUR_DEF[0], foreground=LOGConstants.READ_DYNASAUR_DEF[1])
        self.log_text.tag_configure(LOGConstants.READ_VOLUME[0], foreground=LOGConstants.READ_VOLUME[1])
        self.log_text.tag_configure(LOGConstants.DATA_PLUGIN[0], foreground=LOGConstants.DATA_PLUGIN[1])
        self.log_text.tag_configure(LOGConstants.WARNING[0], foreground=LOGConstants.WARNING[1])

        ################################################################################################################
        # DATA section
        row += 1
        frame_setup = tk.ttk.Labelframe(frame, text="Data", width=sum(self.__width_definition[0]))
        frame_setup.grid(row=row, column=0, padx=(10, 10), pady=(10, 10))


        row_data_frame = 0
        self.code_type = tk.StringVar(root)
        choices = [CodeType.MADYMO, CodeType.BINOUT]
        self.code_type.set(CodeType.BINOUT)
        tk.ttk.Label(frame_setup, justify='right', text="Chose a input code :", anchor="e",
                     width=20).grid(row=row_data_frame, column=0, padx=self.__pad_x[0], pady=self.__pad_y[0])
        self.popup_menu = tk.ttk.OptionMenu(frame_setup, self.code_type, choices[0], *choices)
        self.popup_menu.grid(row=row_data_frame, column=1)

        row_data_frame += 1

        self.madymo_file = tk.StringVar()

        tk.ttk.Label(frame_setup, justify='right', text="Madymo directory : ", anchor="e",
                                            width=20).grid(
            row=row_data_frame, column=0, padx=self.__pad_x[0], pady=self.__pad_y[0])
        self.__madymo_entry = tk.ttk.Entry(frame_setup, textvar=self.madymo_file, justify='left', width=70)
        self.__madymo_entry.grid(row=row_data_frame, column=1, padx=self.__pad_x[1])
        self.mad_sel_btn = tk.ttk.Button(frame_setup, text="...", width=3)
        self.mad_sel_btn.grid(row=row_data_frame, column=2, padx=(0, 10), pady=(5, 5))
        self.mad_read_btn = tk.ttk.Button(frame_setup, command=read_madymo_callback, text="read", width=12)
        self.mad_read_btn.grid(row=row_data_frame, column=3, pady=(5, 5))
        self.madymo_file.set(madymo_file)

        row_data_frame += 1

        self.binout_dir = tk.StringVar()

        self.label_data_type = tk.ttk.Label(frame_setup, justify='right', text="Binout directory : ", anchor="e", width=20).grid(
            row=row_data_frame, column=0, padx=self.__pad_x[0], pady=self.__pad_y[0])
        self.__binout_entry = tk.ttk.Entry(frame_setup, textvar=self.binout_dir, justify='left', width=70)
        self.__binout_entry.grid(row=row_data_frame, column=1, padx=self.__pad_x[1])
        self.bin_sel_btn = tk.ttk.Button(frame_setup, text="...", width=3)
        self.bin_sel_btn.grid(row=row_data_frame, column=2, padx=(0, 10), pady=(5, 5))
        self.bin_read_btn = tk.ttk.Button(frame_setup, command=read_binout_callback, text="read", width=12)
        self.bin_read_btn.grid(row=row_data_frame, column=3, pady=(5, 5))
        self.binout_dir.set(binout_dir)

        row_data_frame += 1

        self.def_var = tk.StringVar()
        tk.ttk.Label(frame_setup, anchor='e', text="dynasaur.def : ", width=20).grid(row=row_data_frame, column=0,
                                                                                     padx=self.__pad_x[0],
                                                                                     pady=self.__pad_y[0])
        self.__dyna_entry = tk.ttk.Entry(frame_setup, textvar=self.def_var, justify='left', width=70)
        self.__dyna_entry.grid(row=row_data_frame, column=1, padx=self.__pad_x[1])
        self.def_sel_btn = tk.ttk.Button(frame_setup, text="...", width=3)
        self.def_sel_btn.grid(row=row_data_frame, column=2, padx=(0, 10), pady=(5, 5))
        self.def_read_btn = tk.ttk.Button(frame_setup, command=read_dyna_def_callback, text="read", width=12)
        self.def_read_btn.grid(row=row_data_frame, column=3, pady=(5, 5))
        self.def_var.set(dyna_file)

        self.binout_dir.trace("w", self.__toggle_buttons)
        self.madymo_file.trace("w", self.__toggle_buttons)
        self.def_var.trace("w", self.__toggle_buttons)
        self.code_type.trace("w", self.__toggle_buttons)
        self.__toggle_buttons()

    def __toggle_buttons(self, *args):
        self.__binout_entry.config(state='disabled')
        self.__madymo_entry.config(state='disabled')

        state = "normal" if os.path.isdir(self.binout_dir.get()) and self.code_type.get() == CodeType.BINOUT else "disabled"
        self.bin_sel_btn.config(state=state)
        self.bin_read_btn.config(state=state)
        self.__binout_entry.config(state=state)
        state = "normal" if os.path.isfile(self.madymo_file.get()) and self.code_type.get() == CodeType.MADYMO else "disabled"
        self.mad_sel_btn.config(state=state)
        self.mad_read_btn.config(state=state)
        self.__madymo_entry.config(state=state)
        state = "normal" if os.path.isfile(self.def_var.get()) else "disabled"
        self.def_read_btn.config(state=state)


    def __init__(self, root, binout_dir, dyna_file, madymo_file, read_binout_callback, read_madymo_callback, read_dyna_def_callback):
        self.__width_definition = [[30, 53], [30, 23]]
        self.__pad_x = [(5, 5), (5, 5)]
        self.__pad_y = [(3, 3)]

        self.__build_gui(root, read_binout_callback, read_dyna_def_callback, read_madymo_callback, binout_dir, madymo_file, dyna_file)
