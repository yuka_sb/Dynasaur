# Class to write Hyperworks files
import os.path


class ExportSet(object):

    def __init__(self):
        self.fh = None

    def open(self, fn):
        self.fh = open(fn, "w")
        self.fh.write("*KEYWORD\n")
        return self.fh

    def set_solid(self, solid, sid, name):
        self.fh.write("*SET_SOLID\n")
        self.fh.write("$HMSET %d SETS %s\n" % (sid, name))
        self.fh.write("%10d\n" % sid)
        c = 0
        for id in solid:
            c = c + 1
            self.fh.write("%10s" % id)
            if c % 8 == 0: self.fh.write("\n")
        self.fh.write("\n")

    def set_shell(self, shell, sid, name):
        self.fh.write("*SET_SHELL_LIST\n")
        self.fh.write("$HMSET %d SETS %s\n" % (sid, name))
        self.fh.write("%10d\n" % sid)
        c = 0
        for id in shell:
            c = c + 1
            self.fh.write("%10s" % id)
            if c % 8 == 0: self.fh.write("\n")
        self.fh.write("\n")

    def close(self):
        self.fh.write("*END\n")
        self.fh.close()
