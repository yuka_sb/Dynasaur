#!/bin/bash



# check prerequists
# git installation
# >python3.6 installation
command -v git >/dev/null 2>&1 || { echo >&2 "Installation requires git but it's not installed.  Aborting."; exit 1; }
command -v python3 >/dev/null 2>&1 || { echo >&2 "Installation requires python3 but it's not installed.  Aborting."; exit 1; }


ret=`python3 -c 'import sys; print(6 <= sys.version_info[1])'`
if [ $ret != 'True' ]; then
    echo "Dynasaur requires python version >= 3.6. Aborting"
    exit 1
fi



# install other prerequisites
pip3 install -r requirements.txt --user
apt-get install python3-tk
