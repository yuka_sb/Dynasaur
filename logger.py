import logging
import colorama
import tkinter as tk
from termcolor import colored

from constants import LOGConstants


class WidgetLogger(logging.Handler):

    def __init__(self, widget, root):
        self.__root = root
        logging.Handler.__init__(self)
        self.setLevel(logging.CRITICAL)
        self.__widget = widget
        self.__widget.config(state='disabled')

    def clear(self):
        self.__widget.config(state='normal')
        self.__widget.delete('1.0', tk.END)
        self.__widget.config(state='disabled')

    def emit(self, label, record):
        string = label + "\t" + record
        self.__widget.config(state='normal')
        # Append message (record) to the widget
        self.__widget.insert(tk.END, string + '\n')
        self.__widget.see(tk.END)  # Scroll to the bottom
        self.__widget.highlight_pattern(label, label)
        self.__widget.config(state='disabled')
        self.__root.update_idletasks()


class ConsoleLogger(object):
    def __init__(self):
        colorama.init()
        return

    def __get_color_for_label(self, label):
        log_attributes = [a for a in dir(LOGConstants) if not a.startswith("__")]
        attribute_values = [[LOGConstants.__dict__[key]] for key in LOGConstants.__dict__.keys() if
                            key in log_attributes]
        color = [i[0][1] for i in attribute_values if i[0][0] == label][0]
        return color

    def emit(self, label, record):
        color = self.__get_color_for_label(label)
        print(colored(label, color) + "\t" + record)
