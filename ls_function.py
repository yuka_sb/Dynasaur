"""
    Dynasaur,
    a post processing tool for the analysis of FE output files.

    Copyright (C) 2018  Graz University of Technology
"""

import math
import numpy as np
from cfc import CFC
from scipy.integrate import cumtrapz
from scipy.stats import norm 

from constants import LOGConstants


class StandardFunction:

    @staticmethod
    def HIC15_AIS3(param_dict, units):
        injuryvalue = param_dict["injuryvalue"]
        offset = param_dict["offset"]
        divide = param_dict["divide"]
        risk = 0
        risk = norm.cdf((math.log(injuryvalue[0]) - offset) / divide)

        return risk
        
    @staticmethod
    def BRIC_AIS3(param_dict, units):
        injuryvalue = param_dict["injuryvalue"]  
        divide = param_dict["divide"]  
        power = param_dict["power"]
        risk = 0
        risk = 1 - (math.exp(-((injuryvalue) / divide)** power))
         
        return risk

    @staticmethod
    def SIGMOID_RISK_1(param_dict, units):
        injuryvalue = param_dict["injuryvalue"]
        offset = param_dict["offset"]
        divide = param_dict["divide"]  
        scaling = param_dict["scaling"]  
        power = param_dict["power"]
        risk = 0
        risk = 1 / (1 + math.exp(offset - scaling * ((np.abs(injuryvalue[0])) ** power)))

        return risk

    @staticmethod
    def res(param_dict):
        return np.linalg.norm(param_dict["vector"], axis=1).reshape(-1, 1)

    @staticmethod
    def max(param_dict, absolute=False):
        data_vector = np.abs(param_dict['data_vector']) if absolute else param_dict['data_vector']
        maximum = np.max(data_vector)
        index_of_maximum = np.argmax(data_vector)
        index_of_maximum = int(index_of_maximum / data_vector.shape[1])
        time_of_maximum = param_dict['time'][index_of_maximum]
        return (maximum, time_of_maximum[0])

    @staticmethod
    def minimum(param_dict):
        data_vector = param_dict['data_vector']
        minimum = np.min(data_vector)
        index_of_minimum = np.argmin(data_vector)

        if len(data_vector.shape) != 1:
            index_of_minimum = int(index_of_minimum / data_vector.shape[1])

        time_of_minimum = param_dict['time'][index_of_minimum]

        return (minimum, time_of_minimum[0])

    @staticmethod
    def max_and_time(param_dict):
        maximum = np.max(param_dict['data_vector'])
        time_of_maximum = np.argmax(param_dict['data_vector'])
        param_dict['time'][time_of_maximum]
        return (maximum, time_of_maximum)

    @staticmethod
    def multiplication(param_dict):
        arr1 = param_dict["arr1"]
        arr2 = param_dict["arr2"]
        return np.multiply(arr1, arr2)

    @staticmethod
    def abs(param_dict):
        data_vec = param_dict['data_vector']
        return np.abs(data_vec)

    @staticmethod
    def transform_2_origin(param_dict):
        data_vec = param_dict['data_vector']
        return data_vec - data_vec[0]

    @staticmethod
    def cfc(param_dict, units):
        assert('time' in param_dict)
        assert(len(param_dict['time']) >= 2)

        Time = (param_dict['time'][1] - param_dict['time'][0])[0] / units.second()
        cfc = CFC(cfc=param_dict['cfc'], T=Time)
        nr_sampled_array_data_series = param_dict['sampled_array'].shape[1]
        return np.transpose(cfc.filter(sampled_array=np.transpose(param_dict['sampled_array'].reshape(-1, nr_sampled_array_data_series)), time=param_dict['time'], time_to_seconds_factor=units.second()))

    @staticmethod
    def tibia_index(param_dict, units):

        mx = param_dict['Mx'] / ((units.meter() ** 2) / (units.second() ** 2))
        my = param_dict['My'] / ((units.meter() ** 2) / (units.second() ** 2))
        fz = param_dict['Fz'] / (units.meter() / (units.second() ** 2))
        critical_bending_moment = param_dict["critical_bending_moment"]  # Input def: Nm
        critical_compression_force = param_dict["critical_compression_force"]  # Input def: N

        mr = np.linalg.norm(np.stack([mx, my], axis=1)[:, :, 0], axis=1)
        ti = np.abs(mr / critical_bending_moment) + np.abs(fz / critical_compression_force)

        return ti

    @staticmethod
    def NIC(param_dict, units):
        a_t1 = param_dict['a_T1'].flatten()
        a_head = param_dict['a_head'].flatten()
        a_t1 /= units.meter() / (units.second() ** 2) # a_t1 in [m/s^2]
        a_head /= units.meter() / (units.second() ** 2) # a_head in [m/s^2]
        a_rel = a_t1 - a_head

        t = param_dict['time'].flatten() / units.second() # t in [s]
        v_rel = cumtrapz(a_rel, t, initial=0) #integral

        return_value_nic = 0.2 * a_rel + v_rel ** 2

        return np.array(return_value_nic).reshape(-1, 1)

    '''
      time vector in ms
      a_res in mm/s²

      make that consistent and use definitions here!
      use more outputs here!
      possible to return HIC for each time step
    '''
    @staticmethod
    def HIC_15(param_dict, units, flag=True):
        t = param_dict['time'].flatten() / units.second()  # t in [s]
        a_res = param_dict['a_res'].flatten()
        a_res /= units.meter() / (units.second() ** 2)  # a_res in [m/s^2]
        a_res /= units.grav_const  # a_res in [g]

        vel = cumtrapz(a_res, t, initial=0)
        hic15 = []
        time2 = []
        dt = (t[1] - t[0])
        if flag:
            nidx = int(round(0.015 / dt))
        else:
            nidx = int(round(0.036 / dt))


        # TODO
        # RuntimeWarning: invalid value encountered in double_scalars temp.append((tdiff) * (((vel[jt] - vel[ut]) / (tdiff)) ** 2.5))
        # problem None values
        
        for ut in range(len(t) - 1):
            temp = []
            for jt in range(ut + 1, min([len(t), ut + nidx])):
                tdiff = t[jt] - t[ut]
                temp.append(tdiff * (((vel[jt] - vel[ut]) / tdiff) ** 2.5))
                
            m = np.nanmax(temp)
            hic15.append(m)
            time2.append(t[ut])
      
        hic15.append(hic15[-1])
        return np.array(hic15).reshape(-1, 1)

    '''
    only one value possible to return
    maybe some visualisation possible?!?
    '''

    @staticmethod
    def central_differences(param_dict, units):
        v = param_dict['arr1'].flatten()
        time = param_dict['arr2']
        
        a = []  
        a = np.gradient(v,np.squeeze(time))
        
        return np.array(a).reshape(-1, 1)


    @staticmethod
    def bric(param_dict, units):
        r_vel = param_dict["r_vel"] * units.second()
        crit_rx_velocity = param_dict["crit_rx_velocity"] # [rad/s]
        crit_ry_velocity = param_dict["crit_ry_velocity"] # [rad/s]
        crit_rz_velocity = param_dict["crit_rz_velocity"] # [rad/s]
        bric = np.linalg.norm([np.max(np.abs(r_vel[:, 0])) / crit_rx_velocity,
                               np.max(np.abs(r_vel[:, 1])) / crit_ry_velocity,
                               np.max(np.abs(r_vel[:, 2])) / crit_rz_velocity])

        return bric


    @staticmethod
    def uBRIC(param_dict, units):
    
        r_vel = param_dict["r_vel"] * units.second()
        r_acc = param_dict["r_acc"] * (units.second() ** 2)
        
        a = np.max(np.abs(r_vel[:, 0]))
        
        crit_rx_velocity = param_dict["crit_rx_velocity"] # [rad/s]
        crit_ry_velocity = param_dict["crit_ry_velocity"] # [rad/s]
        crit_rz_velocity = param_dict["crit_rz_velocity"] # [rad/s]        
        
        crit_rx_acceleration = param_dict["crit_rx_acceleration"] # [rad/s^2]
        crit_ry_acceleration = param_dict["crit_ry_acceleration"] # [rad/s^2]        
        crit_rz_acceleration = param_dict["crit_rz_acceleration"] # [rad/s^2]
        
        # absolute max. of r_vel and r_acc normalized
        
        wx = np.max(np.abs(r_vel[:, 0])) / crit_rx_velocity
        wy = np.max(np.abs(r_vel[:, 1])) / crit_ry_velocity
        wz = np.max(np.abs(r_vel[:, 2])) / crit_rz_velocity
        ax = np.max(np.abs(r_acc[:, 0])) / crit_rx_acceleration
        ay = np.max(np.abs(r_acc[:, 1])) / crit_ry_acceleration
        az = np.max(np.abs(r_acc[:, 2])) / crit_rz_acceleration
        
        ubric = ( ( wx + ( ax - wx ) * math.exp( -( ax / wx ))) ** 2 + ( wy + ( ay - wy ) * math.exp(-( ay / wy ))) ** 2 + ( wz + ( az-wz ) * math.exp(-( az / wz ))) ** 2 ) ** (1/2)
        
        return ubric
    
    @staticmethod
    def vc(param_dict, units):

        scfa = param_dict['scaling_factor']
        defconst = param_dict['deformation_constant']
        y = param_dict['y'] / units.meter()
        t = param_dict['time'] / units.second()

        delta_t = t[1] - t[0]

        # vc derivativew
        deformation_velocity = np.zeros(len(t))
        for i in range(2, len(t) - 2):
            deformation_velocity[i] = (8 * (y[i + 1] - y[i - 1]) - (y[i + 2] - y[i - 2]))

        deformation_velocity[0] = (8 * (y[1] - y[0]) - (y[2] - y[0]))
        deformation_velocity[1] = (8 * (y[2] - y[0]) - (y[3] - y[0]))
        deformation_velocity[-2] = (8 * (y[-1] - y[-3]) - (y[-1] - y[-4]))
        deformation_velocity[-1] = (8 * (y[-1] - y[-2]) - (y[-1] - y[-3]))

        deformation_velocity /= (12 * delta_t)
        vc = np.multiply(scfa * (y / defconst), deformation_velocity.reshape(-1, 1))

        return vc

    @staticmethod
    def nij(param_dict, units):
        force_x = param_dict['force_x'] / (units.meter() / (units.second() ** 2))
        force_z = param_dict['force_z'] / (units.meter() / (units.second() ** 2))
        moment_y = param_dict['moment_y'] / ((units.meter()**2) / (units.second() ** 2))
        compression_tension_flexion_extension_data = np.zeros(shape=(len(force_z), 0))
        
        moc_d = param_dict['distance_occipital_condyle'] #  m
        nij_fzc_te = param_dict['nij_fzc_te']  # N
        nij_fzc_co = param_dict['nij_fzc_co']  # N
        nij_myc_fl = param_dict['nij_myc_fl']  # Nm
        nij_myc_ex = param_dict['nij_myc_ex']  # Nm

        nij = np.zeros(shape=(len(force_z)))
        temp_n_value = np.zeros(shape=(4,))

        mtot = moment_y - moc_d * force_x

        for o in range(len(force_z)):
            temp_n_value[0] = force_z[o] / nij_fzc_co + mtot[o] / nij_myc_fl if force_z[o] <= 0 and mtot[o] > 0 else 0    # NCF
            temp_n_value[1] = force_z[o] / nij_fzc_co + mtot[o] / nij_myc_ex if force_z[o] <= 0 and mtot[o] <= 0 else 0   # NCE
            temp_n_value[2] = force_z[o] / nij_fzc_te + mtot[o] / nij_myc_fl if force_z[o] > 0 and mtot[o] > 0 else 0     # NTF
            temp_n_value[3] = force_z[o] / nij_fzc_te + mtot[o] / nij_myc_ex if force_z[o] > 0 and mtot[o] <= 0 else 0    # NTE

            nij[o] = np.max(temp_n_value)
            temp_n_value = np.zeros(shape=(4,))

        return nij.reshape((-1, 1))

    @staticmethod
    def subtraction(param_dict):
        arr1 = param_dict["arr1"]
        arr2 = param_dict["arr2"]
        return np.subtract(arr1, arr2)

    @staticmethod
    def addition(param_dict):
        arr1 = param_dict["arr1"]
        arr2 = param_dict["arr2"]
        return np.add(arr1, arr2)

    @staticmethod
    def abs_subtraction(param_dict):
        arr1 = param_dict["arr1"]
        arr2 = param_dict["arr2"]
        return np.abs(np.subtract(arr1, arr2))

    @staticmethod
    def abs_addition(param_dict):
        arr1 = param_dict["arr1"]
        arr2 = param_dict["arr2"]
        return np.abs(np.add(arr1, arr2))

    @staticmethod
    def abs_subtraction_res(param_dict):
        arr1 = param_dict["arr1"]
        arr2 = param_dict["arr2"]

        rel_dist_m = []
        rel_dist = []

        rel_dist_m = np.subtract(arr1, arr2)

        for i in range(len(rel_dist_m)):
            rel_dist.append((rel_dist_m[i, 0] ** 2 + rel_dist_m[i, 1] ** 2 + rel_dist_m[i, 2] ** 2) ** (1 / 2))

        return np.abs(rel_dist)


    @staticmethod
    def a3ms(param_dict, units):
        time = param_dict['time'].flatten()  # t in [s]
        a_res = param_dict['a_res'].flatten()
        # TODO
        # implementation in a central point
        t = time / units.second()
        a_res /= units.meter() / (units.second() ** 2)
        a_res /= units.grav_const

        ls_ind = []
        last = 0  # possible because time is ascending!
        for i in range(len(t)):
            if last == len(t) - 1:
                ls_ind.append((i, last + 1))
                #continue

            for j in range(last, len(t)):
                if t[j] - t[i] > 0.003:  # found start stop indices
                    ls_ind.append((i, j))
                    last = j
                    break

        a3ms_values = np.array([np.min(a_res[ind_tuple[0]:ind_tuple[1]]) for ind_tuple in ls_ind])

        return a3ms_values.reshape((-1, 1))


class LSFunction:
    def __init__(self, functions, logger):
        self.__funcs = functions
        self._logger = logger
        # check if the step function is properly defined, if not reject it
        d = {}
        for key in functions.keys():
            risk_0 = self.calculate_function(key, 0.001)
            if 0.0 <= risk_0 < 0.1:
                d[key] = functions[key]
            else:
                logger.emit(LOGConstants.WARNING[0], "function: <" + key + "> is not a proper risk function: "
                                                                           "risk for x = 0 is: " + str(risk_0))

        self.__funcs = d

    def proper_defined_functions(self):
        return self.__funcs.keys()

    def get_x_where_risk_is_value(self, function_name, val=1):
        if isinstance(self.__funcs[function_name], list):
            prob = np.asarray(self.__funcs[function_name])[:, 0]
            x = np.asarray(self.__funcs[function_name])[:, 1]
            x[0] = -np.Inf
            ret_vals = x[-1] if len(np.where(prob > val)[0]) == 0 else x[np.where(prob > val)[0][0]]
            return ret_vals
        else:
            # binary search between 0 and 1000
            lower = 0
            upper = 0.1
            ind = []
            value = 0.00 # NOTE do not delte it!!
            eps = abs(eval(self.__funcs[function_name]))
            eps = max(eps, 10 ** (-5))
            while len(ind) == 0:
                x = np.linspace(lower, upper, num=100)
                risks = [eval(self.__funcs[function_name]) for value in x]
                ind = np.where((val - np.array(risks)) < eps)[0]
                upper += 0.1
                lower += 0.1

            return x[ind[0]]

    def calculate_function(self, function_name, values):
        try:
            if isinstance(self.__funcs[function_name], list):
                prob = np.asarray(self.__funcs[function_name])[:, 0]
                x = np.asarray(self.__funcs[function_name])[:, 1]
                x[0] = -np.Inf

                if type(values) is list:
                    return [prob[np.where(value > x)][-1] for value in values]
                else:
                    return prob[np.where(values > x)][-1]

            else:
                if type(values) is list:
                    return [min(eval(self.__funcs[function_name]), 1.0) for value in values]
                else:
                    value = values
                    return min(eval(self.__funcs[function_name]), 1.0)

        except KeyError:
            self._logger.emit(LOGConstants.WARNING[0], "Risk function: " + function_name + " is not defined! Please check your def file!")
            return np.NaN
        except:
            return np.NaN

    def get_function(self, function_name):
        return self.__funcs[function_name]

    def get_function_names(self):
        return self.__funcs.keys()
