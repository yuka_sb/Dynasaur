# Class for LS-Dyna object
#
# Author: Robert Greimel
# Version: 28.August.2014
#
# -----------------------------------------------------------------------


class LSObject(object):
    def __init__(self, oid):
        self.__oid = oid
        self.__parts = {}
        self.__elements = {}

    def set_parts(self, part):
        self.__parts = part

    def add_part(self, pid):
        self.__parts[pid] = pid

    def add_element(self, eid):
        self.__elements[eid] = eid

    def get_parts(self):
        return sorted(self.__parts)


    def get_element(self):
        return sorted(self.__elements)
