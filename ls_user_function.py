import math
import numpy as np
from cfc import CFC
from scipy.integrate import cumtrapz
from scipy.stats import norm 

from constants import LOGConstants

# use to define own functions
class UserFunction:

    #EXAMPLE 
    @staticmethod
    def define_your_function(param_dict, units):
        time = (param_dict['time'])
        value = (param_dict['value'])
        offset = (param_dict['offset'])

        time_of_maximum = time[0] - offset ** value

        return np.max(time_of_maximum)

    #describe in Wiki page