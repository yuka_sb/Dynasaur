import csv
import abc
import os.path
from io import StringIO
from matplotlib.backends.backend_pdf import PdfPages
from PIL import Image
from datetime import datetime
from tkinter import filedialog
from tkinter import ttk
import inspect
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import math
#from converter import Converter

from constants import DefFileConstants, StandardFunctionsDefinition, JsonConstants, ObjectConstantsForData, LoggerERROR, LoggerSCRIPT
from ls_user_function import UserFunction

from Data.elout import EloutIndex

import time
import numpy as np

from ls_function import LSFunction, StandardFunction
from constants import LOGConstants, FigureConstants, ScriptCommands, CodeType
from Data.DataContainer import DataContainer


class ScriptPluginRegistry:

    def __init__(self, dyna_def, logger):
        self._dyna_def = dyna_def
        self._logger = logger
        self._plugin_controller = {ScriptCommands.PLUGIN_UNIVERSAL: None,
                                   ScriptCommands.PLUGIN_DATA_VISUALISATION: None,
                                   ScriptCommands.PLUGIN_CRITERIA: None,
                                   ScriptCommands.PLUGIN_CROSSSECTION: None,
                                   ScriptCommands.PLUGIN_CSDM: None,
                                   ScriptCommands.PLUGIN_RIB: None}

    def register(self, name):
        from Plugins.Analysis.universal_controller import UniversalController
        from Plugins.Injury.crosssection_controller import CrosssectionController
        from Plugins.Injury.csdm_controller import CSDMController
        from Plugins.Injury.rib_controller import RibController
        from Plugins.Injury.criteria_controller import CriteriaController
        from Plugins.Data.data_plugin_controller import DataPluginController

        units = self._dyna_def.get_units()
        code_type = self._dyna_def.get_code()
        if name == ScriptCommands.PLUGIN_UNIVERSAL:
            risk_functions = self._dyna_def.get_defined_risk_functions(DefFileConstants.FUNC_UNIVERSAL)
            limits = self._dyna_def.get_defined_limits(DefFileConstants.FUNC_UNIVERSAL)
            self._plugin_controller[name] = UniversalController(root=None, logger=self._logger, risk_functions=risk_functions,
                                                                units=units, name=name, code_type=code_type, limits=limits)

        elif name == ScriptCommands.PLUGIN_CROSSSECTION:
            risk_functions = self._dyna_def.get_defined_risk_functions(DefFileConstants.FUNC_CROSSSECTION)
            limits = self._dyna_def.get_defined_limits(DefFileConstants.FUNC_CROSSSECTION)
            type_of_criteria = self._dyna_def.get_defined_type_of_criteria(DefFileConstants.FUNC_CROSSSECTION)
            self._plugin_controller[name] = CrosssectionController(root=None, logger=self._logger, risk_functions=risk_functions,
                                                                   units=units, name=name, code_type=code_type, limits=limits, type_of_criteria=type_of_criteria)

        elif name == ScriptCommands.PLUGIN_CSDM:
            risk_functions = self._dyna_def.get_defined_risk_functions(DefFileConstants.FUNC_CSDM)
            limits = self._dyna_def.get_defined_limits(DefFileConstants.FUNC_CSDM)
            type_of_criteria = self._dyna_def.get_defined_type_of_criteria(DefFileConstants.FUNC_CSDM)
            self._plugin_controller[name] = CSDMController(root=None, logger=self._logger, risk_functions=risk_functions,
                                                           units=units, name=name, code_type=code_type, limits=limits, type_of_criteria=type_of_criteria)

        elif name == ScriptCommands.PLUGIN_RIB:
            risk_functions = self._dyna_def.get_defined_risk_functions(DefFileConstants.FUNC_RIB)
            limits = self._dyna_def.get_defined_limits(DefFileConstants.FUNC_RIB)
            type_of_criteria = self._dyna_def.get_defined_type_of_criteria(DefFileConstants.FUNC_RIB)
            self._plugin_controller[name] = RibController(root=None, logger=self._logger, risk_functions=risk_functions,
                                                          units=units, name=name, code_type=code_type, limits=limits, type_of_criteria=type_of_criteria)

        elif name == ScriptCommands.PLUGIN_CRITERIA:
            risk_functions = self._dyna_def.get_defined_risk_functions(DefFileConstants.FUNC_KINEMATIC_CRITERIA)
            data_requirements = self._dyna_def.get_required_datatypes_for_injury_risks()
            self._plugin_controller[name] = CriteriaController(code_type=code_type, root=None, logger=self._logger, risk_functions=risk_functions,
                                                               units=units, data_requirements=data_requirements, name=name)

        elif name == ScriptCommands.PLUGIN_DATA_VISUALISATION:
            risk_functions = self._dyna_def.get_defined_risk_functions(DefFileConstants.FUNC_DATA_VISUALISATION)
            data_requirements = self._dyna_def.get_required_datatypes_for_data_visualisation()
            self._plugin_controller[name] = DataPluginController(code_type=code_type, root=None, data_requirements=data_requirements,
                                                                 logger=self._logger, risk_functions=risk_functions, units=units, name=name)

    def get_plugin_controller(self, name):
        return self._plugin_controller[name]


class Plugin(object):
    def __init__(self, root, logger, data_requirements, risk_functions, units, name, code_type):
        self.__root = root
        self.__script_calc_array = []
        self.__timestamp = datetime.today().strftime("%Y-%m-%d-%H-%M-%S")
        self._units = units
        self._logger = logger
        self._titles = []
        self._overall_plot_data = []
        self._data_requirements = data_requirements
        self._data = None
        self._name = name
        self._sample_types = []
        self._function = LSFunction(risk_functions, logger)
        self._code_type = code_type

        # ensure unique timestamps
        time.sleep(1)

    ###
    ### TODO
    ### data type should be returned
    ### problem with time start_time
    ### validate the results !!!
    def _get_data_from_dynasaur_JSON(self, json_object, data_offsets):
        if JsonConstants.FUNCTION in json_object.keys(): # expected name and params
            function_name = json_object[JsonConstants.FUNCTION][JsonConstants.NAME]
            parameter_def = json_object[JsonConstants.FUNCTION][JsonConstants.PARAM]

            param_dict = {}
            for key in parameter_def.keys():
                if type(parameter_def[key]) is dict: # step into recursion
                    param_dict[key] = self._get_data_from_dynasaur_JSON(parameter_def[key], data_offsets)
                else:
                    param_dict[key] = parameter_def[key]

            #START: Standard function
            if function_name == StandardFunctionsDefinition.HIC15:
                return StandardFunction.HIC_15(param_dict, self._units, True)
            elif function_name == StandardFunctionsDefinition.HIC36:
                return StandardFunction.HIC_15(param_dict, self._units, False)
            elif function_name == StandardFunctionsDefinition.BRIC:
                return StandardFunction.bric(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.A3MS:
                return StandardFunction.a3ms(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.NIJ:
                return StandardFunction.nij(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.VC:
                return StandardFunction.vc(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.TIBIA_INDEX:
                return StandardFunction.tibia_index(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.CFC:
                return StandardFunction.cfc(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.RES:
                return StandardFunction.res(param_dict)
            elif function_name == StandardFunctionsDefinition.ABS:
                return StandardFunction.abs(param_dict)
            elif function_name == StandardFunctionsDefinition.TRANSFORM_TO_ORIGIN:
                return StandardFunction.transform_2_origin(param_dict)
            elif function_name == StandardFunctionsDefinition.MAX:
                return StandardFunction.max(param_dict)
            elif function_name == StandardFunctionsDefinition.MIN:
                return StandardFunction.minimum(param_dict)
            elif function_name == StandardFunctionsDefinition.MULT:          
                return StandardFunction.multiplication(param_dict)
            elif function_name == StandardFunctionsDefinition.SUB:
                return StandardFunction.subtraction(param_dict)
            elif function_name == StandardFunctionsDefinition.ADD:
                return StandardFunction.addition(param_dict)
            elif function_name == StandardFunctionsDefinition.ABS_SUB:
                return StandardFunction.abs_subtraction(param_dict)
            elif function_name == StandardFunctionsDefinition.ABS_ADD:
                return StandardFunction.abs_addition(param_dict)
            elif function_name == StandardFunctionsDefinition.ABS_SUB_RES:
                return StandardFunction.abs_subtraction_res(param_dict)
            elif function_name == StandardFunctionsDefinition.ABS_MAX:
                return StandardFunction.max(param_dict, absolute=True)
            elif function_name == StandardFunctionsDefinition.NIC:
                return StandardFunction.NIC(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.uBRIC:
                return StandardFunction.uBRIC(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.ALL_HIC15_AIS3:
                return StandardFunction.HIC15_AIS3(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.ALL_BRIC_AIS3:
                return StandardFunction.BRIC_AIS3(param_dict, self._units)
            elif function_name == StandardFunctionsDefinition.SIGMOID_RISK_1:
                return StandardFunction.SIGMOID_RISK_1(param_dict, self._units)    
            elif function_name == StandardFunctionsDefinition.central_differences:
                return StandardFunction.central_differences(param_dict, self._units)
            #END: Standard functions
            elif self._check_if_user_function(function_name):
                function = getattr(UserFunction, function_name)
                return function(param_dict, self._units)
            else:
                self._logger.emit(LOGConstants.ERROR[0], function_name + StandardFunctionsDefinition.ERROR_NOT_STANDARD_FUNCTION)
                return

        else:  # data to obtain
            if JsonConstants.VALUE in json_object.keys():
                return json_object[JsonConstants.VALUE]
            data_type = json_object[JsonConstants.TYPE]
            ids = self._data[data_type].get_channels_ids_object_name(json_object[JsonConstants.ID_UPPER_CASE], self._name)
            array_definition = json_object[JsonConstants.ARRAY]

            # processing data array
            converted_tuples = [(int(tuple_string.split(',')[0].strip(' (')), tuple_string.split(',')[1].strip(' )'))
                                for tuple_string
                                in array_definition]

            data_array = None
            for tuple in converted_tuples:
                d = self._data[data_type].get_measurement_channel(id=ids[tuple[0]], channel_name=tuple[1])
                data_array = d if data_array is None else np.append(data_array, d, axis=1)

            for (t,offset, delta_t) in data_offsets:
                if t == data_type:
                    data_offset = offset
                    data_delta_t = delta_t

            return data_array[data_offset:data_delta_t]

    def _check_if_user_function(self, function_name):
        attributes = inspect.getmembers(UserFunction, predicate=inspect.isfunction)
        #if function_name in [att[0] for att in attributes if '_' not in att[0]]:
        if function_name in [att[0] for att in attributes]:
            return True
        return False


    def init_plugin_data(self, update):
        if self._data is None or update:
            self._data = {data: DataContainer.get_data(data) for data in self._data_requirements}
            for data_name in self._data.keys():
                if data_name is ObjectConstantsForData.VOLUME: # data dependency for volume ... elout is necessary fot initialisation
                    self._data[data_name].read_binout_data(elout=self._data[ObjectConstantsForData.ELEMENT], strain_stress_type=EloutIndex.STRAIN)
                    continue
                if self._code_type == CodeType.MADYMO:
                    self._data[data_name].set_madymo_data()
                    self._data[data_name].clean_channel_names()
                    continue
                if self._data[data_name] is None:
                    self._logger.emit(LOGConstants.ERROR[0], data_name + LoggerERROR.print_statements[1])
                if not self._data[data_name].read_binout_data():
                    self._logger.emit(LOGConstants.ERROR[0], data_name + LoggerERROR.print_statements[2])

    def _get_proper_defined_risk_functions(self):
        self._function.proper_defined_functions()
        return self._function.proper_defined_functions()

    # saveText
    def save_text(self, tmp_file=None):
        separator = ";" if tmp_file.name.endswith(".csv") else " "  # .txt file separator
        tmp_file.write(self.getOutputString(separator=separator))
        tmp_file.close()

    def get_upper_limit(self, curve):
        #  find value where 1 - risk(value) <10^-6
        upper_limit = self._function.get_x_where_risk_is_value(curve, 1)
        return upper_limit

    def calculate_risk(self, value, curve):
        risk = self._function.calculate_function(curve, value)
        return risk

    # copyText
    def copy_text(self):
        # copy to clipboard
        self.__root.clipboard_clear()
        self.__root.clipboard_append(self.getOutputString())

    def extract_script_calc_array_and_title(self, result_str, merge=True):
        title = ''
        if len(self.__script_calc_array) == 0:
            f = StringIO(result_str)
            reader1 = csv.reader(f, delimiter=';')
            for row in reader1:
                if len(row) == 1:
                    row.append('')

                self.__script_calc_array.append(row)

                if len(row) > 0:
                    if row[0] in ['Object:', 'Strain/Stress:', 'Tension/Compression:', 'Limit:']:
                        title += row[0] + ' ' + row[1] + ';'

        else:
            f = StringIO(result_str)
            reader1 = csv.reader(f, delimiter=';')

            content = [row for row in reader1]
            if len(content) < len(self.__script_calc_array):
                for i in range(len(self.__script_calc_array) - len(content)):
                    content.append(['-'] * len(content[0]))

            if len(content)> len(self.__script_calc_array): # extend the __script_calc_array
                for i in range(len(content) - len(self.__script_calc_array)):
                    self.__script_calc_array.append(['-'] * len(self.__script_calc_array[0]))

            for row_index in range(len(self.__script_calc_array)):
                row = content[row_index]

                if len(row) == 0 and 0 == len(self.__script_calc_array[row_index]): # empty row
                    continue
                elif len(row) == 1:
                    self.__script_calc_array[row_index].append('')
                else:
                    entry_list = row[1:] if merge else row # merge into formats (Object:; Head  + Object:; Pelvis -> Object:;Head;Pelvis)
                    for r in entry_list:
                        self.__script_calc_array[row_index].append(r)

                if len(row) > 0:
                    if row[0] in ['Object:', 'Strain/Stress:', 'Tension/Compression:', 'Limit:']:
                        title += row[0] + ' ' + row[1] + ';'

        self._titles.append(title)

    # ******************************************************************************************************************
    # write csv to the given file directory
    #
    def write_CSV(self, csv_file_dir):
        csv_path = csv_file_dir.split("\\a_act50")[0]
        self._logger.emit(LOGConstants.SCRIPT[0], self._name + LoggerSCRIPT.print_statements[1] + csv_path)
        path = os.path.join(csv_path, self._name + "_" + self.__timestamp + ".csv")

        with open(path, 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=';', lineterminator='\n')
            writer.writerows(self.__script_calc_array)

    def write_ISOMME(self, _dynasaur_definitions):
        converter = Converter()
        converter.write_ISOMME(self.__script_calc_array, _dynasaur_definitions, self._logger)


    # ******************************************************************************************************************
    # PDF contains:
    # 1) the calculated data
    #    - taken from the self.script_calc_array
    #
    # 2) the histogram
    #    - plot function
    #
    # 3) time plot
    #    - better visualization in x axes
    def write_PDF(self, pdf_file_dir):
        self._logger.emit(LOGConstants.SCRIPT[0], LoggerSCRIPT.print_statements[2] + pdf_file_dir)
        path = pdf_file_dir + "\\Report" + self.__timestamp + ".pdf"

        # get the plots
        pp = PdfPages(path)
        self.plotCalculationsToPDF(pp=pp)
        pp.close()

    def brand_plot_TU(self, ax, logo_width=0.2):
        im = Image.open('./img/TU_Graz_svg.png')
        logo_height = logo_width / im.width * im.height
        x0 = ax.get_position().x0
        y0 = ax.get_position().y0
        width = ax.get_position().width
        height = ax.get_position().height

        newax = ax.figure.add_axes([x0 + width - logo_width, y0 + height, logo_width, logo_height], anchor='NE',
                                   zorder=10)
        newax.imshow(im)

        newax.text(-0.2, height / 2, "Analysed with Dynasaur developed by Graz University of Technology",
                   verticalalignment='bottom', horizontalalignment='right',
                   transform=newax.transAxes, color='black', fontsize=8)

        newax.axis('off')

    # ******************************************************************************************************************
    # brand Plot
    #
    def brand_plot(self, ax, position, logo_width=0.2):
        im = Image.open('./img/TU_Graz_svg.png')
        logo_height = logo_width / im.width * im.height
        x0 = ax.get_position().x0
        y0 = ax.get_position().y0
        width = ax.get_position().width
        height = ax.get_position().height

        if position == FigureConstants.LEFT_LOWER_CORNER:
            newax = ax.figure.add_axes([x0, y0, logo_width, logo_height], anchor='SW', zorder=10)
        elif position == FigureConstants.LEFT_UPPER_CORNER:
            newax = ax.figure.add_axes([x0, y0 + height - logo_height, logo_width, logo_height], anchor='SW', zorder=10)
        elif position == FigureConstants.RIGHT_LOWER_CORNER:
            newax = ax.figure.add_axes([x0 + width - logo_width, y0, logo_width, logo_height], anchor='SE', zorder=10)
        else:
            newax = ax.figure.add_axes([x0 + width - logo_width, y0 + height - logo_height, logo_width, logo_height],
                                       anchor='SE', zorder=10)

        newax.imshow(im)
        newax.axis('off')

    def _reduce_sample_offset(self, json_object, sample_offsets):
        self._get_data_from_json_reduce_samples(json_object)
        self._sample_types = list(set(self._sample_types))
        return_sample = []
        for sample in sample_offsets:
            if sample[0] in self._sample_types:
                return_sample.append(sample)
        self._sample_types.clear()
        return return_sample

    def _get_data_from_json_reduce_samples(self, json_object):
        if JsonConstants.FUNCTION in json_object.keys():  # expected name and params
            parameter_def = json_object[JsonConstants.FUNCTION][JsonConstants.PARAM]

            param_dict = {}
            for key in parameter_def.keys():
                if type(parameter_def[key]) is dict:  # step into recursion
                    param_dict[key] = self._get_data_from_json_reduce_samples(parameter_def[key])
                else:
                    param_dict[key] = parameter_def[key]
        else:  # data to obtain
            if JsonConstants.VALUE in json_object.keys():
                return json_object[JsonConstants.VALUE]

            self._sample_types.append(json_object[JsonConstants.TYPE])
            return json_object[JsonConstants.ARRAY]

class GUI(object):
    def __init__(self, root, controller, width_definition, pad_x, pad_y):
        self._root = root
        self._controller = controller
        self.label_list_calc = []

        self._width_definition = width_definition
        self._pad_x = pad_x
        self._pad_y = pad_y

    def _gui_save_text(self, event=None):
        file = filedialog.asksaveasfile(mode='w', defaultextension=".csv", filetypes=("CSV file", "*.csv"))
        if file is None:
            self._logger.emit(LOGConstants.ERROR[0], LoggerERROR.print_statements[3])
            return

        self._controller.save_text(file)
        file.close()

    def _gui_copy_text(self, event=None):
        self._controller.copy_text()

    def _create_label(self, frame, name, row, column, width_definition_index):
        anchor = "w" if column == 1 else "e"
        self.label_list_calc[-1].append(ttk.Label(frame, text=name, anchor=anchor,
                                                  width=self._width_definition[width_definition_index][
                                                      column]))
        self.label_list_calc[-1][-1].grid(row=row, column=column, padx=self._pad_x[column])


#TODO: There is no usages of this function, do we need them?
class GUIInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def _build_GUI(self):
        """Save the data object to the output."""

    @abc.abstractmethod
    def _gui_update(self):
        """"""

    @abc.abstractmethod
    def _gui_plot(self):
        """"""


class PluginInterface(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_output_string(self):
        """Save the data object to the output."""

    @abc.abstractmethod
    def check_if_params_are_valid(self, param_dict):
        """"""

    @abc.abstractmethod
    def calculate_and_store_results(self, param_dict):
        """"""

    @abc.abstractmethod
    def plot_calculations_to_pdf(self):
        """"""
