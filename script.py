import re
import copy
import os.path
from constants import ScriptCommands
from constants import ParsingReturns, LOGConstants


class Script(object):

    def __init__(self, script_file_name, logger):
        self.__script_file_name = script_file_name
        self.__logger = logger
        self.__command_pointer = -1

        self.madymo_file = None

        self.__parameters = {}
        self.__dicts = []

        self.__binouts = []
        self.__commands = []
        self.__command_stack = []

        # parsing the given .dss file
        with open(self.__script_file_name) as f:

            # parse each line and check the return value
            for line in f:
                # print(line)
                parsed = self.__parse_line(line)
                if parsed == ParsingReturns.MADYMO_FILE_DOES_NOT_EXIST:
                    self.__logger.emit(LOGConstants.ERROR[0], "Madymo file does not exist : " + line)
                    self.__commands = [None]
                    break
                if parsed == ParsingReturns.DEF_FILE_DOES_NOT_EXIST:
                    self.__logger.emit(LOGConstants.ERROR[0], "Def file does not exist : " + line)
                    self.__commands = [None]
                    break
                if parsed == ParsingReturns.NOT_ALL_BINOUTS_EXIST:
                    self.__logger.emit(LOGConstants.ERROR[0], "Not all binout directories exist : " + line)
                    self.__commands = [None]
                    break
                if parsed == ParsingReturns.PARM_MERGE_ERROR:
                    self.__logger.emit(LOGConstants.ERROR[0],
                                       "Merging does not work, due to different lengths : " + line)
                    self.__commands = [None]
                    break
                if parsed == ParsingReturns.DEPENDENCIES_ERROR:
                    self.__logger.emit(LOGConstants.ERROR[0], "Some dependencies are not fulfilled : " + line)
                    self.__commands = [None]
                    break
                if parsed == ParsingReturns.COMMAND_NOT_UNDERSTOOD:
                    self.__logger.emit(LOGConstants.ERROR[0], "Command not understood : " + line)
                    self.__commands = [None]
                    break
            self.__extend_command_stack()
            self.__command_stack

        f.close()

        return

    ######################################################################################################################
    #
    # check if the given string is a float
    #
    def __is_float(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    ######################################################################################################################
    #
    # return :
    #  key  : parameter name for calling the calculation function
    #  value: parameters value (can be a list)
    def __get_params_to_vary(self, list_string):
        list_string = "".join(list_string.split())

        stripped = (list_string).strip('()')
        split_list = stripped.split(':')

        list_string = stripped[stripped.find(':')+1:]
        params_to_vary = list_string[1:-1]
        params_to_vary = params_to_vary.split(",")
        return split_list[0], params_to_vary

    ######################################################################################################################
    #
    # called for command PARAMS_MERGE
    # creates a dictionary with flat entries
    #
    def __add_params_to_dictionary_flat(self, list_string):

        # if self.__parameters == {}:
        #     return ParsingReturns.DEPENDENCIES_ERROR

        (dict_key_word, params_to_vary) = self.__get_params_to_vary(list_string)
        if (len(self.__dicts) == 0):
            d = copy.deepcopy(self.__parameters)
            self.__dicts.append(d)

        if (len(self.__dicts) != len(params_to_vary)):
            return ParsingReturns.PARM_MERGE_ERROR

        dicts = []
        for (d, i) in zip(self.__dicts, params_to_vary):
            d_copy = copy.deepcopy(d)
            d_copy[dict_key_word] = float(i) if self.__is_float(i) else i
            dicts.append(d_copy)

        self.__dicts = dicts

        return ParsingReturns.COMMAND_OK

    ######################################################################################################################
    #
    # called for command PARAMS_VAR
    # create a dictionary with nested entries
    #
    def __add_params_to_dictionary_nested(self, list_string):

        # if self.__parameters == {}:
        #     return ParsingReturns.DEPENDENCIES_ERROR

        (dict_key_word, params_to_vary) = self.__get_params_to_vary(list_string)
        if (len(self.__dicts) == 0):
            d = copy.deepcopy(self.__parameters)
            self.__dicts.append(d)

        dicts = []

        for d in self.__dicts:
            for i in params_to_vary:
                d_copy = copy.deepcopy(d)
                d_copy[dict_key_word] = float(i) if self.__is_float(i) else i
                dicts.append(d_copy)

        self.__dicts = dicts

        return ParsingReturns.COMMAND_OK

    ######################################################################################################################
    #
    # called for command PARAMS_DEF
    #
    def __init_dict(self, string_dict):

        stripped_dict = string_dict.strip('{}').split(",")
        initial_definition_elements = [(i.strip().strip('()').split(':')) for i in stripped_dict]

        # insert elements into dictionary
        for element in initial_definition_elements:
            #
            self.__parameters[element[0].strip()] = float(element[1].strip()) if self.__is_float(element[1].strip()) else \
                element[1].strip()

        return ParsingReturns.COMMAND_OK

    def __extend_command_stack(self):

        self.__command_stack.append([ScriptCommands.DEF_FILE, self.def_file])
        if self.madymo_file is not None:
            self.__command_stack.append(["MADYMO_FILE", self.madymo_file])
            for com in self.__commands:
                if com[0] == ScriptCommands.PDF_REPORT:
                    self.__command_stack.append([ScriptCommands.PDF_REPORT, self.madymo_file])
                elif com[0] == ScriptCommands.CSV_REPORT:
                    self.__command_stack.append([ScriptCommands.CSV_REPORT, self.madymo_file])
                else:
                    self.__command_stack.append(com)
            self.__command_stack.append(['CLEAR'])


        for binout in self.__binouts:
            self.__command_stack.append([ScriptCommands.BINOUT_FILE, binout])
            for com in self.__commands:
                if com[0] == ScriptCommands.PDF_REPORT:
                    self.__command_stack.append([ScriptCommands.PDF_REPORT, binout])
                elif com[0] == ScriptCommands.CSV_REPORT:
                    self.__command_stack.append([ScriptCommands.CSV_REPORT, binout])
                elif com[0] == ScriptCommands.ISOMME_REPORT:
                    self.__command_stack.append([ScriptCommands.ISOMME_REPORT, binout])
                else:
                    self.__command_stack.append(com)
            self.__command_stack.append(['CLEAR'])

    ######################################################################################################################
    #
    # creates the commandstack that is executed
    #
    def __convert_dicts_to_command(self, what, write_as):

        if len(self.__dicts) == 0:
            self.__dicts = [self.__parameters]

        # 1) read def_file
        # self.__commands.append(["DEF_FILE", self.def_file])

        #calculation_command = what.replace("PLUGIN_", "")

        for d in self.__dicts:
            self.__commands.append([what, d])
        if ScriptCommands.PDF_REPORT in write_as:
            self.__commands.append([ScriptCommands.PDF_REPORT, ""])
        if ScriptCommands.CSV_REPORT in write_as:
            self.__commands.append([ScriptCommands.CSV_REPORT, ""])
        if ScriptCommands.ISOMME_REPORT in write_as:
            self.__commands.append([ScriptCommands.ISOMME_REPORT, ""])

    ######################################################################################################################
    #
    # check if all binouts of the list exist
    #
    def __parse_binout_files(self, binout_list):
        binout_list_string_without_spaces = "".join(binout_list.split())
        self.__binouts = binout_list_string_without_spaces.strip('[]').split(",")
        if len(self.__binouts) == sum([1 if os.path.exists(i) else 0 for i in self.__binouts]):
            return ParsingReturns.COMMAND_OK
        return ParsingReturns.NOT_ALL_BINOUTS_EXIST

    ######################################################################################################################
    #
    # parsing 1 line of the script
    # stores the data, which are necessary to create the command stack
    #
    def __parse_line(self, line):
        line = line.strip()
        if line.startswith("#") or len(line) == 0:
            return ParsingReturns.COMMENT

        path_regex = "[A-Z]:((\\\\)[A-Za-z0-9_\-]+)+"
        tuple_rgex = "\([ \t]*[A-z][A-z0-9]*[ \t]*:[ \t]*[A-z0-9\._\+\*\^]+[ \t]*\)"
        tuple_var_regex = "[A-z][A-z0-9]*[ \t]*:[ \t]*\[[0-9A-z][A-z0-9\:\._\+\*\^]*[ \t]*([ \t]*,[ \t]*[0-9A-z][A-z0-9\:\._\+\*\^]*)*[ \t]*\]"
        write_out_options = "(" + ScriptCommands.CSV_REPORT + "|" + ScriptCommands.PDF_REPORT + "|" + ScriptCommands.ISOMME_REPORT + ")"

        # FILE Command (BINOUT_FILE) ...
        m = re.search(
            "(" + ScriptCommands.BINOUT_FILE + ")[ \t]+\[(" + path_regex + ")([ \t]*[,][ \t]* " + path_regex + ")*\]",
            line)
        if m != None:
            self.__logger.emit(LOGConstants.SCRIPT[0], "parsing binout files")
            command = m.group(0)
            ret_val = self.__parse_binout_files(command[command.find("["):])
            return ret_val

        m = re.search("(" + ScriptCommands.DEF_FILE + ")[ \t]+(" + path_regex + "(\.def))[ \t]*", line)
        if m != None:
            self.__logger.emit(LOGConstants.SCRIPT[0], "parsing def file")
            command = m.group(0)
            self.def_file = command.replace(ScriptCommands.DEF_FILE, "", 1).strip()
            if os.path.isfile(self.def_file):
                return ParsingReturns.COMMAND_OK
            return ParsingReturns.DEF_FILE_DOES_NOT_EXIST

        m = re.search("(" + ScriptCommands.MADYMO_FILE + ")[ \t]+(" + path_regex + "(\.h5))[ \t]*", line)
        if m != None:
            self.__logger.emit(LOGConstants.SCRIPT[0], "parsing madymo file")
            command = m.group(0)
            self.madymo_file = command.replace(ScriptCommands.MADYMO_FILE, "", 1).strip()
            if os.path.isfile(self.madymo_file):
                return ParsingReturns.COMMAND_OK
            return ParsingReturns.MADYMO_FILE_DOES_NOT_EXIST

        m = re.search("(" + ScriptCommands.PARAMS_DEF + ")[ \t]+\{(" + tuple_rgex + ")*([ \t]*[,][ \t]* " + tuple_rgex + ")*\}", line)
        if m != None:
            self.__logger.emit(LOGConstants.SCRIPT[0], "parsing parameter definition")
            command = m.group(0)
            ret_val = self.__init_dict(command[command.find("{"):])
            return ret_val

        m = re.search("(" + ScriptCommands.PARAMS_VAR + ")[ \t]+(\(" + tuple_var_regex + ")*\)", line)
        if m != None:
            self.__logger.emit(LOGConstants.SCRIPT[0], "parsing parameter variation")
            command = m.group(0)
            ret_val = self.__add_params_to_dictionary_nested(command[command.find("("):])
            return ret_val

        m = re.search("(" + ScriptCommands.PARAMS_MERGE + ")[ \t]+(\(" + tuple_var_regex + ")\)", line)
        if m != None:
            self.__logger.emit(LOGConstants.SCRIPT[0], "parsing parameter merge")
            command = m.group(0)
            ret_val = self.__add_params_to_dictionary_flat(command[command.find("("):])
            return ret_val

        m = re.search("(" + ScriptCommands.PARAMS_CLEAR + ")", line)
        if m != None:
            self.__logger.emit(LOGConstants.SCRIPT[0], "parsing parameter clear")
            # reset the parameters
            # self.__commands.append(["CLEAR"])
            self.__parameters = {}
            self.__dicts = []
            return ParsingReturns.COMMAND_OK
        m = re.search(
            "(" + ScriptCommands.PLUGIN_UNIVERSAL + "|" + ScriptCommands.PLUGIN_CSDM + "|" + ScriptCommands.PLUGIN_CROSSSECTION + "|" + ScriptCommands.PLUGIN_DUMMY_EVAL + "|" +
            ScriptCommands.PLUGIN_RIB + "|" + ScriptCommands.PLUGIN_CRITERIA + "|" + ScriptCommands.PLUGIN_DATA_VISUALISATION + ")" +
            "[ \t]*(\[[ \t]*" + write_out_options + "[ \t]*([ \t]*,[ \t]*" + write_out_options + "[ \t]*)*\])",
            line)
        if m != None:
            plugin_name = m.group(0)[0:m.group(0).find('[')].strip()
            self.__logger.emit(LOGConstants.SCRIPT[0], "prepare command stack for " + plugin_name)
            command = m.group(0)
            report_list = [i.strip() for i in command.replace(plugin_name, "", 1).strip().strip("[]").split(",")]

            # create a dictionary from the parameters
            self.__convert_dicts_to_command(plugin_name, report_list)

            return ParsingReturns.COMMAND_OK

        return ParsingReturns.COMMAND_NOT_UNDERSTOOD

    ######################################################################################################################
    #
    # called from the main controller to fetch one command from the command stack
    #
    def get_next_command(self):
        self.__command_pointer += 1

        if self.__command_pointer == len(self.__command_stack):
            return None

        return self.__command_stack[self.__command_pointer]

    ######################################################################################################################
    #
    # only for testing purpose
    #
    def _test_parse_line(self):

        print("**********************************  TEST REGEX **********************************")
        self.__parse_line("") == 0
        self.__parse_line(" ") == 0
        assert (self.__parse_line(
            "BINOUT_FILE [C:\\Users\\martin_schachner\\PycharmProjects\\dynasaur\\Examples\\binout_HBM_example_04_Output_2ms_2steps_Rib\\test , C:\\a]") == 1)
        assert (self.__parse_line(
            "BINOUT_FILE [C:\\Users\\martin_schachner\\PycharmProjects\\dynasaur\\Examples\\binout_HBM_example_04_Output_2ms_2steps_Rib\\test , C:\\abc\\abhhbh\\jnaja      	, A:\\ana\\ahb]") == 1)
        assert (self.__parse_line(
            "BINOUT_FILE C:\\Users\\martin_schachner\\PycharmProjects\\dynasaur\\Examples\\binout_HBM_example_04_Output_2ms_2steps_Rib\\test , C:\\abc\\abhhbh\\jnaja      	, A:\\ana\\ahb") == -1)
        assert (self.__parse_line(
            "BINOUT_FILE C:\\Users\\martin_schachner\\PycharmProjects\\dynasaur\\Examples\\binout_HBM_example_04_Output_2ms_2steps_Rib\\test ") == -1)

        assert (self.__parse_line(
            "DEF_FILE [C:\\Users\\martin_schachner\\PycharmProjects\\dynasaur\\Examples\\binout_HBM_example_04_Output_2ms_2steps_Rib\\test\\dynsaur.def]") == -1)
        assert (self.__parse_line(
            "DEF_FILE C:\\Users\\martin_schachner\\PycharmProjects\\dynasaur\\Examples\\binout_HBM_example_04_Output_2ms_2steps_Rib\\test\\dynsaur.def") == 1)
        assert (self.__parse_line(
            "DEF_FILE        C:\\Users\\martin_schachner\\PycharmProjects\\dynasaur\\Examples\\binout_HBM_example_04_Output_2ms_2steps_Rib\\test\\dynsaur.def    ") == 1)
        assert (self.__parse_line(
            "DEF_FILE        C:\\Users\\martin_schachner\\PycharmProjects dynasaur\\Examples\\binout_HBM_example_04_Output_2ms_2steps_Rib\\test\\dynsaur.def") == -1)

        assert (self.__parse_line("PARAMS_DEF   {(selected_section: FemurL), (selected_sect: FemurL_01_Crs)}") == 1)
        assert (self.__parse_line(
            "PARAMS_DEF   {(selected_section: FemurL), (selected_sect: 12), (selected_secto: 120)}") == 1)
        assert (self.__parse_line(
            "PARAMS_DEF {(selected_time_step: All), (integration_point: Mean), (selection_tension_compression: Overall), (selection_strain_stress : Strain), (limit : 0.2), (percentile    : true)}") == 1)

        assert (self.__parse_line("PARAMS_VAR   (selected_section: [FemurL, FemurL_01_Crs])") == 1)
        assert (self.__parse_line("PARAMS_VAR   (selected_section: [FemurL, FemurL_01_Crs])") == 1)
        assert (self.__parse_line("PARAMS_VAR   (selection_strain_stress: [Strain, Stress])") == 1)
        assert (self.__parse_line("PARAMS_VAR   (limit: [0.2, 0.3])") == 1)
        assert (self.__parse_line("PARAMS_VAR   (selected_object: [Brain, Left_Brain, Right_Brain])") == 1)
        assert (self.__parse_line(
            "PARAMS_VAR   (limit: [0.2, 0.3]), selected_object: [Brain, Left_Brain, Right_Brain])") == 1)
        assert (self.__parse_line(
            "PARAMS_VAR (selection_strain_stress: [Strain, Stress], selected_object: [Brain, Left_Brain, Right_Brain])") == -1)

        assert (self.__parse_line("PARAMS_CLEAR"))

        assert (self.__parse_line("PARAMS_DEF   {(selected_section: FemurL), (selected_sect: FemurL_01_Crs)}") == 1)
        assert (self.__parse_line("PARAMS_VAR   (limit: [0.2, 0.3])") == 1)
        assert (self.__parse_line(
            "PARAMS_MERGE   (selection_strain_stress: [Strain, Stress], limit: [0.2, 0.3], selected_object: [Brain, Left_Brain, Right_Brain])") == -1)
        assert (self.__parse_line("PARAMS_MERGE   (limit: [0.2, 0.3])") == 1)

        assert (self.__parse_line("PLUGIN_UNIVERSAL    [TXT_REPORT]") == 1)
        assert (self.__parse_line("PARAMS_CLEAR"))
        print("*********************************************************************************")
