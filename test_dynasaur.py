from unittest import TestCase
from dynasaur_definitions import DynasaurDefinitions
from logger import ConsoleLogger
from dynasaur import Controller
from Data.elout import Elout
from Data.secforc import Secforc
from Data.volume import Volume
from lasso.dyna import Binout
from constants import DefFileConstants, ScriptCommands
from Plugins.Analysis.universal_controller import UniversalController
import io
from Data.DataContainer import DataContainer
from Plugins.Injury.csdm_controller import CSDMController
from Plugins.Injury.rib_controller import RibController
from Plugins.Injury.crosssection_controller import CrosssectionController

import os, glob, sys, pickle

path_to_simulation = "M:\\VSI-P\\120_Codes\\Python_Dynasaur\\08_Input_Examples\\HBM_Postprocessor_Injury-Tool\\100_Unit_Test_Examples"

class TestDynasaurAFilesToCompare(TestCase):
    def test_run_A_main(self):
        femur_wd = os.path.join(path_to_simulation, "binout_femur\\D3PLOT\\")
        controler = Controller(os.path.join(femur_wd, "script.dss"))
        controler.run()

    def test_run_B_main(self):
        hbm_wd = os.path.join(path_to_simulation, "binout_HBM_example_03_Output_1ms\\")
        controler = Controller(os.path.join(hbm_wd, "script.dss"))
        controler.run()

class TestOutputFiles(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestOutputFiles, self).__init__(*args, **kwargs)
        self.femur_wd = os.path.join(path_to_simulation, "binout_femur\\D3PLOT\\")

    def test_plugin_data_visualisation(self):
        ref_data_vis = os.path.join(self.femur_wd, "PLUGIN_DATA_VISUALISATION_ref.csv")

        list_of_files_data_vis = glob.glob(self.femur_wd + "PLUGIN_DATA_VISUALISATION_*.csv")
        latest_file_data_vis = max(list_of_files_data_vis, key=os.path.getctime)

        self.maxDiff = None
        HelpClass.check_diff_files(ref_data_vis, latest_file_data_vis, self)

    def test_plugin_criteria(self):
        ref_kinematic_crit = os.path.join(self.femur_wd, "PLUGIN_CRITERIA_ref.csv")

        list_of_files_kinematic_crit = glob.glob(self.femur_wd + "PLUGIN_CRITERIA_*.csv")
        latest_file_kinematic_crit = max(list_of_files_kinematic_crit, key=os.path.getctime)

        self.maxDiff = None
        HelpClass.check_diff_files(ref_kinematic_crit, latest_file_kinematic_crit, self)

    def test_crosssecton_as_kinematic_criteria(self):
        ref_kinematic_crit = os.path.join(path_to_simulation, "binout_HBM_example_03_Output_1ms\\PLUGIN_KINEMATIC_CRITERIA_ref.csv")

        list_of_files_kinematic_crit = glob.glob(os.path.join(path_to_simulation, "binout_HBM_example_03_Output_1ms/PLUGIN_KINEMATIC_CRITERIA_*.csv"))
        latest_file_kinematic_crit = max(list_of_files_kinematic_crit, key=os.path.getctime)

        self.maxDiff = None
        with open(ref_kinematic_crit) as reference:
            with open(latest_file_kinematic_crit) as output:
                self.assertEqual(reference.read(), output.read())

    def test_madymo(self):
        ref_data_vis = os.path.join("M:\\VSI-P\\120_Codes\\Python_Dynasaur\\08_Input_Examples\\MADYMO\\DatenBosch\\v2", "PLUGIN_DATA_VISUALISATION_ref.csv")

        list_of_files_data_vis = glob.glob("M:\\VSI-P\\120_Codes\\Python_Dynasaur\\08_Input_Examples\\MADYMO\\DatenBosch\\v2\\" + "PLUGIN_DATA_VISUALISATION_*.csv")
        latest_file_data_vis = max(list_of_files_data_vis, key=os.path.getctime)

        self.maxDiff = None
        HelpClass.check_diff_files(ref_data_vis, latest_file_data_vis, self)

        ref_kinematic_crit = os.path.join("M:\\VSI-P\\120_Codes\\Python_Dynasaur\\08_Input_Examples\\MADYMO\\DatenBosch\\v2", "PLUGIN_CRITERIA_ref.csv")

        list_of_files_kinematic_crit = glob.glob("M:\\VSI-P\\120_Codes\\Python_Dynasaur\\08_Input_Examples\\MADYMO\\DatenBosch\\v2\\" + "PLUGIN_CRITERIA_*.csv")
        latest_file_kinematic_crit = max(list_of_files_kinematic_crit, key=os.path.getctime)

        self.maxDiff = None
        HelpClass.check_diff_files(ref_kinematic_crit, latest_file_kinematic_crit, self)

class TestSingleFunctions(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestSingleFunctions, self).__init__(*args, **kwargs)
        self.femur_wd = os.path.join(path_to_simulation, "binout_femur\\D3PLOT\\")

    def test_read_def(self):
        dynasaur_def = DynasaurDefinitions(ConsoleLogger())
        dynasaur_def.read_def(os.path.join(self.femur_wd, "dynasaur.def"))

        with open(os.path.join(self.femur_wd, "output_ref.pkl"), "rb") as pkl_file:
            ref_dict = pickle.load(pkl_file)

        self.maxDiff = None
        for key in dynasaur_def.__dict__:
            if key != "_logger":
                if key == "_title":
                    self.assertEqual(dynasaur_def.__dict__[key], ref_dict.__dict__[key])
                elif key == "_units":
                    self.assertEqual(dynasaur_def.__dict__[key].__dict__, ref_dict.__dict__[key].__dict__)
                elif key == "_objects":
                    pass
                elif key == "_code" or key == "_energy_part" or key == "_energy_global":
                    continue
                else:
                    self.assertDictEqual(dynasaur_def.__dict__[key], ref_dict.__dict__[key])


class TestDynasaurErrorHandling(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestDynasaurErrorHandling, self).__init__(*args, **kwargs)
        self.femur_wd = os.path.join(path_to_simulation, "binout_femur\\D3PLOT\\")

    def test_definition_name_incorrect(self):
        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput
        dynasaur_def = DynasaurDefinitions(ConsoleLogger())
        try:
            dynasaur_def.read_def(os.path.join(self.femur_wd, "dynasaur_exit.def"))
        except AssertionError:
            pass

    def test_invalid_json_file(self):
        dynasaur_def = DynasaurDefinitions(ConsoleLogger())
        try:
            dynasaur_def.read_def(os.path.join(self.femur_wd, "dynasaur_invalid_json.def"))
        except SystemExit:
            pass


class TestPlugins(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestPlugins, self).__init__(*args, **kwargs)
        self.femur_wd = os.path.join(path_to_simulation, "binout_femur\\D3PLOT\\")

    def test_universal_controller(self):
        def_file_path = os.path.join(self.femur_wd, "dynasaur_object.def")
        binout_file_path = os.path.join(self.femur_wd, "binout*")
        logger, risk_functions, units, dyn_def = HelpClass.init_class_plugin(path_to_def=def_file_path,
                                                                             path_to_binout=binout_file_path,
                                                                             plugin_name=DefFileConstants.FUNC_UNIVERSAL)
        code_type = dyn_def.get_code()
        universal_controler = UniversalController(code_type=code_type, root=None, logger=logger, risk_functions=risk_functions,
                                                  units=units, name=DefFileConstants.FUNC_UNIVERSAL)

        universal_controler.init_plugin_data(update=True)

        parameters_file = os.path.join(self.femur_wd, "parameters.pkl")
        with open(parameters_file, "rb") as pkl_file:
            parameter = pickle.load(pkl_file)

        param_dict = dyn_def.get_param_dict_from_script_command(script_command=[ScriptCommands.PLUGIN_UNIVERSAL, parameter])

        selected_object = param_dict['selected_object'].replace("_", " ")
        selected_time_step = param_dict["selected_time_step"]
        selection_strain_stress = param_dict["selection_strain_stress"]
        integration_point = param_dict["integration_point"]
        selection_tension_compression = param_dict['selection_tension_compression']
        limit = param_dict['limit']
        percentile = param_dict['percentile']

        percentile_limit_str, percentile, element_count, histogram_data = universal_controler.calculate(
            start_time=None, end_time=1,
            integration_point=integration_point,
            selection_tension_compression=selection_tension_compression,
            selection_strain_stress=selection_strain_stress,
            selected_object=selected_object,
            limit=limit,
            percentile=percentile,
            is_percentile_set=True)

        calculated_results_file = os.path.join(self.femur_wd, "calculate_ref.pkl")
        with open(calculated_results_file, "rb") as pkl_file:
            result_dict = pickle.load(pkl_file)

        self.assertEqual(result_dict["percentile_limit_str"], percentile_limit_str)
        self.assertEqual(result_dict["percentile"], percentile)
        self.assertEqual(result_dict["element_count"], element_count)
        self.assertEqual(result_dict["histogram_data"], histogram_data)

    def test_csdm_controller(self):
        def_file_path = os.path.join(path_to_simulation, "binout_CSDM\\dynasaur.def")
        binout_file_path = os.path.join(path_to_simulation, "binout_CSDM\\binout*")
        logger, risk_functions, units, dyn_def = HelpClass.init_class_plugin(path_to_def=def_file_path,
                                                                             path_to_binout=binout_file_path,
                                                                             plugin_name=DefFileConstants.FUNC_CSDM)
        code_type = dyn_def.get_code()
        csdm_controller = CSDMController(code_type=code_type, root=None, logger=logger,
                                         risk_functions=risk_functions, units=units, name=DefFileConstants.FUNC_CSDM)

        csdm_controller.init_plugin_data(update=True)

        list_of_input_params = glob.glob(os.path.join(path_to_simulation, "binout_CSDM\\input_*.pkl"))

        for param in list_of_input_params:
            with open(param, "rb") as pkl_file:
                dict_params = pickle.load(pkl_file)
        param_dict = dyn_def.get_param_dict_from_script_command(script_command=[ScriptCommands.PLUGIN_CSDM, dict_params])

        csdm, risk = csdm_controller.calculate(param_dict["limit"], param_dict["object"], param_dict["curve"], "", "")

        ref_result = param.split("input")[0] + "output" + param.split("input")[1]
        with open(ref_result, "rb") as pkl_res_file:
            csdm_risk_result_dict = pickle.load(pkl_res_file)

        self.assertEqual(csdm_risk_result_dict["csmd"], csdm)
        self.assertEqual(csdm_risk_result_dict["risk"], risk)

    def test_ribs(self):
        #TODO: Change ref file
        def_file_path = os.path.join(path_to_simulation, "binout_HBM_example_04_Output_2ms_2steps-Rib/test/dynasaur_new.def")
        binout_file_path = os.path.join(path_to_simulation, "binout_HBM_example_04_Output_2ms_2steps-Rib/test/binout*")
        logger, risk_functions, units, dyn_def = HelpClass.init_class_plugin(path_to_def=def_file_path,
                                                                             path_to_binout=binout_file_path,
                                                                             plugin_name=DefFileConstants.FUNC_RIB)
        code_type = dyn_def.get_code()
        rib_controller = RibController(code_type=code_type, root=None, logger=logger,
                                       risk_functions=risk_functions,
                                       units=units,
                                       name=DefFileConstants.FUNC_RIB)

        rib_controller.init_plugin_data(update=True)

        input_params_file_path = os.path.join(path_to_simulation, "binout_HBM_example_04_Output_2ms_2steps-Rib/test/input_params.pkl")
        with open(input_params_file_path, "rb") as pkl_file:
            input_params = pickle.load(pkl_file)

        param_dict = dyn_def.get_param_dict_from_script_command(script_command=[ScriptCommands.PLUGIN_RIB, input_params])

        if not rib_controller.init_rib_definitions():  # define left and right parts of the ribs
            return

        risk_matrix, detail_risk_matrices = rib_controller.calc_risk_for_rib_object("CHEST:Ribs Overall",
                                                                                    param_dict["nr_largest_elements"],
                                                                                    param_dict["age"],
                                                                                    param_dict["function_name"], None, None)
        result_file_path = os.path.join(path_to_simulation, "binout_HBM_example_04_Output_2ms_2steps-Rib/test/result.pkl")
        with open(result_file_path, "rb") as pkl_file:
            reference = pickle.load(pkl_file)

        self.maxDiff = None
        self.assertListEqual(list(reference["risk_matrix"]), list(risk_matrix))
        self.assertEqual(len(reference["detail_risk_matric"]), len(detail_risk_matrices))
        for key in reference["detail_risk_matric"].keys():
            if key not in detail_risk_matrices.keys():
                raise("Keys are not same")
            for index, cnt in enumerate(list(reference["detail_risk_matric"][key])):
                self.assertListEqual(list(reference["detail_risk_matric"][key][index]), list(detail_risk_matrices[key][index]))

    def test_cross_section(self):
        def_file_path = os.path.join(path_to_simulation, "binout_HBM_example_03_Output_1ms\\dynasaur.def")
        binout_file_path = os.path.join(path_to_simulation, "binout_HBM_example_03_Output_1ms/binout*")
        logger, risk_functions, units, dyn_def = HelpClass.init_class_plugin(path_to_def=def_file_path,
                                                                             path_to_binout=binout_file_path,
                                                                             plugin_name=DefFileConstants.FUNC_CROSSSECTION)
        code_type = dyn_def.get_code()
        crosssection_controler = CrosssectionController(code_type=code_type, root=None, logger=logger, risk_functions=risk_functions,
                                                        units=units, name=DefFileConstants.FUNC_CROSSSECTION)

        crosssection_controler.init_plugin_data(update=True)

        list_of_input_params = glob.glob(path_to_simulation + "binout_HBM_example_03_Output_1ms/input_*.pkl")

        for param in list_of_input_params:
            with open(param, "rb") as pkl_file:
                dict_params = pickle.load(pkl_file)
            param_dict = dyn_def.get_param_dict_from_script_command(script_command=[ScriptCommands.PLUGIN_CROSSSECTION, dict_params])

            max_val, time, id, name = crosssection_controler.calculate_result(section=param_dict["section"], type=param_dict["type"])
            #risk = crosssection_controler.calculate_risk(float(max_val), param_dict["curve"])

            ref_result = param.split("input")[0] + "output" + param.split("input")[1]
            with open(ref_result, "rb") as pkl_res_file:
                result_dict = pickle.load(pkl_res_file)

            self.assertEqual(result_dict["max_val"], max_val)
            self.assertEqual(result_dict["time"], time)
            self.assertEqual(result_dict["id"], id)
            self.assertEqual(result_dict["name"], name)


class HelpClass:
    @staticmethod
    def check_list(first, second, key):
        if len(first) != len(second):
            print("[ERROR]" + str(key) + " Length do not match" + str(second))
            return False
        for key_first in first.keys():
            for key_second in second.keys():
                if key_first == key_second:
                    if len(first[key_first]) != len(second[key_second]):
                        print("[ERROR]" + str(key) + " " + str(key_first) + " Length do not match" + str(second[key_first]))
                        return False
                    for value_first in first[key_first]:
                        if int(value_first) not in second[key_first]:
                            print("[ERROR] " + str(key) + " " + str(key_first) + " value do not match " + value_first + " not in " + str(second[key_first]))
                            return False
        return True

    @staticmethod
    def init_class_plugin(path_to_def, path_to_binout, plugin_name):
        logger = ConsoleLogger()
        binout = Binout(path_to_binout)

        dyn_def = DynasaurDefinitions(logger)
        dyn_def.read_def(path_to_def)

        DataContainer._elout = Elout(binout, logger, dyn_def)
        if plugin_name == DefFileConstants.FUNC_CROSSSECTION:
            DataContainer._secforc = Secforc(binout, logger, dyn_def)
        if plugin_name == DefFileConstants.FUNC_CSDM:
            binout_dir = path_to_binout.split("\\binout*")[0]
            DataContainer._volume = Volume(binout_dir, logger, dyn_def)

        risk_functions = dyn_def.get_defined_risk_functions(plugin_name=plugin_name)
        units = dyn_def.get_units()

        return logger, risk_functions, units, dyn_def

    @staticmethod
    def check_diff_files(reference_file, current_file, self):
        with open(reference_file) as reference:
            with open(current_file) as output:
              self.assertEqual(reference.read(), output.read())


