# Class describing Units
#
# Author: Robert Greimel
# Version: 09.August.2014
#
# -----------------------------------------------------------------------
import tkinter as tk


class Units(object):

    def __init__(self):
        self._time = 1.0  # in seconds
        self._length = 1.0  # in meters
        self._weight = 1.0  # in kilogram
        self.grav_const = 9.81

    def set_time(self, time):
        self._time = time

    def set_length(self, length):
        self._length = length

    def set_weight(self, weight):
        self._weight = weight

    # time
    def second(self):
        return self._time

    # length
    def meter(self):
        return self._length

    # weight
    def kilogram(self):
        return self._weight

